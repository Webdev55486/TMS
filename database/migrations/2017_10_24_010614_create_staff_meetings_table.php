<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staff_meetings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('img_name')->nullable();
            $table->string('street');
            $table->string('room')->nullable();
            $table->string('city_town');
            $table->string('state_province');
            $table->string('zip_postal');
            $table->string('country_code');
            $table->string('coor_lat');
            $table->string('coor_lng');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staff_meetings');
    }
}

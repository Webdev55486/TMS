<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('img_name')->nullable();
            $table->string('street');
            $table->string('room')->nullable();
            $table->string('city_town');
            $table->string('state_province');
            $table->string('zip_postal');
            $table->string('country_code');
            $table->boolean('isSaturday')->default(false);
            $table->string('coor_lat');
            $table->string('coor_lng');
            $table->string('manager')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}

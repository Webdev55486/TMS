<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiteSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('company_street');
            $table->string('company_city');
            $table->string('company_state');
            $table->string('company_zip');
            $table->string('country_code');
            $table->boolean('accept_volunteer')->default(true);
            $table->boolean('enable_logo')->default(true);
            $table->string('coor_lat');
            $table->string('coor_lng');
            $table->string('company_logo');
            $table->boolean('register_backstretch')->default(true);
            $table->boolean('shirt_check')->default(false);
            $table->string('register_background');
            $table->string('register_form');
            $table->string('copyright_color');
            $table->text('thankyou_text');
            $table->text('register_email_subject');
            $table->longText('register_email_contents');
            $table->text('location_subject');
            $table->longText('location_contents');
            $table->text('location_schedule_delete_user_subject');
            $table->longText('location_schedule_delete_user_contents');
            $table->text('train_subject');
            $table->longText('train_contents');
            $table->text('train_schedule_delete_user_subject');
            $table->longText('train_schedule_delete_user_contents');
            $table->text('staff_subject');
            $table->longText('staff_contents');
            $table->text('staff_schedule_delete_user_subject');
            $table->longText('staff_schedule_delete_user_contents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_settings');
    }
}

<?php

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {

         Model::unguard();
         DB::table('users')->delete();

         $users = [
             [
                 'first_name' => 'Master',
                 'last_name' => 'Admin',
                 'email' => 'admin@tms.com',
                 'password' => bcrypt('secret'),
                 'street' => '9669 Bellefontaine Rd',
                 'city' => 'St. Louis',
                 'state' => 'MO',
                 'zip' => '63137',
                 'country_code' => 'US',
                 'employer' => 'high school',
                 'phone_cell' => '123456789',
                 'phone_home' => '123456789',
                 'volunteered_year' => 4,
                 'shirt_size' => '4',
                 'best_serve' => '1,4',
                 'speak_lang' => 'English, Chinese',
                 'dietary' => 'Spaghetti',
                 'user_role' => 3,
                 'remember_token' => str_random(10),
             ]
         ];

         foreach ($users as $user) {
             User::create($user);
         }
         Model::reguard();
     }
}

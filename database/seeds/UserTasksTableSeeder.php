<?php

use App\UserTasks;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class UserTasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('user_tasks')->delete();

        $tasks = [
            [
                'title' => 'Greeter',
                'description' => 'You welcome clients and assist them by irienting them to the process. Often a greeter is the first VITA person a client see. Some greeters perform an initial screening for eligibilty and help clients start thier paperwork for free tax preparation. This role includes opportunities to assist the site coordinator and tax preparers with administrative tasks, such as filing and mailings. Knowledge of the tax law si not required, but the basic tax law certifications is required.',
            ],
            [
                'title' => 'Intake Interviewers',
                'description' => 'Assist the clients in completing the intake sheet, interview then to insure the intake sheet is accurately completed to prepare an accurate return , and review tax preparation documents then transfer the paperwork to a tax preparer. You must be tax law certified at the basic level',
            ],
            [
                'title' => 'Tax Preparer',
                'description' => 'You complete and sucessfullycertify at the advanced level in tax training,including the use of electronic filing software(Taxwise online), to provide free tax return preparation for eligible taxpayers. You work one-on-one with individuals and families using the intake form and documents provided by the taxpayer to prepare the tax return. You must be certified at least at the Advanced level',
            ],
            [
                'title' => 'Quality Reviewer',
                'description' => 'You review tax return completed by voluteer tax preparers, ensuring that every taxpayer receives top quality service and that the tax return are error-free. You must be tax law certified at least at the Advanced level',
            ],
        ];

        foreach ($tasks as $task) {
            UserTasks::create($task);
        }
        Model::reguard();
    }
}

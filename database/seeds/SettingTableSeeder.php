<?php

use App\SiteSetting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        DB::table('site_settings')->delete();

        $settings = [
            [
                'company_name' => 'Company Name',
                'company_street' => '9669 Bellefontaine Rd',
                'company_city' => 'St. Louis',
                'company_state' => 'MO',
                'company_zip' => '63137',
                'country_code' => 'US',
                'accept_volunteer' => 1,
                'enable_logo' => 1,
                'coor_lat' => '38.742582',
                'coor_lng' => '-90.22715979999998',
                'company_logo' => '111',
                'register_backstretch' => 1,
                'shirt_check' => 0,
                'register_background' => 'rgba(31,123,173,0.61)',
                'register_form' => 'rgba(255,255,255,0.3)',
                'copyright_color' => '#ffffff',
                'thankyou_text' => 'Thank you! We have received your registration.',
                'register_email_subject' => 'Thank you! We have received your registration.',
                'register_email_contents' => '<p style="text-align: center;"><span style="color: rgb(255, 0, 0); font-family: Impact; font-size: 24px;">Thank you! We have received your registration.</span></p><p style="text-align: center;"><span style="font-size: 24px; font-family: " arial="" black";"="">You were registered successfully.</span></p><p style="text-align: center;"><span style="font-weight: bold; font-style: italic;" comic="" sans="" ms";="" font-size:="" 24px;"="">From <span style="color: rgb(0, 255, 255);">TMS </span>Support</span></p>',
                'location_subject' => 'Added you to location.',
                'location_contents' => 'Hello {username} <br /> We wanted to remind you that you are scheduled to volunteer at {location}, on {date} during the ours of {time}. <br /> If you have any questions please fell free to contact us. <br /> thanks <br /> TMS',
                'location_schedule_delete_user_subject' => 'Volunteer Removed from Location schedule',
                'location_schedule_delete_user_contents' => 'Hello <br /> {username} removed at {location}, on {date} during the ours of {time}.<br /> thanks <br /> TMS',
                'train_subject' => 'Added you to Training Session.',
                'train_contents' => 'Hello {username} <br /> We wanted to remind you that you are scheduled to volunteer at {trainingSession}, on {date} during the ours of {time}. <br /> If you have any questions please fell free to contact us. <br /> thanks <br /> TMS',
                'train_schedule_delete_user_subject' => 'Volunteer Removed from Training Session schedule',
                'train_schedule_delete_user_contents' => 'Hello <br /> {username} removed at {trainingSession}, on {date} during the ours of {time}.<br /> thanks <br /> TMS',
                'staff_subject' => 'Added you to Staff Meeting.',
                'staff_contents' => 'Hello {username} <br /> We wanted to remind you that you are scheduled to volunteer at {staffMeeting}, on {date} during the ours of {time}. <br /> If you have any questions please fell free to contact us. <br /> thanks <br /> TMS',
                'staff_schedule_delete_user_subject' => 'Volunteer Removed from Staff Meeting schedule',
                'staff_schedule_delete_user_contents' => 'Hello <br /> {username} removed at {staffMeeting}, on {date} during the ours of {time}.<br /> thanks <br /> TMS',
            ],
        ];

        foreach ($settings as $setting) {
            SiteSetting::create($setting);
        }
        Model::reguard();
    }
}

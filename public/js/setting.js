$(document).ready(function() {

	$('#site_logo_change_form').submit(function(event) {
		event.preventDefault();

		$.ajaxSetup({
			headers: {
				'X-CSRF-Token': $('meta[name=_token]').attr('content')
			}
		});
		var url = $(this).attr( 'action' );

		var formData = new FormData($(this)[0]);
		var $el = $(this).find('#company_logo');
		var $els = $(this).find('.fileinput');
		var $el_imag = $(this).find('.fileinput-new.thumbnail');

		$('.page-loader-wrapper').fadeIn();
		$.ajax({
			url: url,
			type: 'POST',
			data: formData,
			success: function (data) {

				App.alert({
					container: '#bootstrap_alerts_demo', // alerts parent container(by default placed after the page breadcrumbs)
					place: 'append', // append or prepent in container
					type: 'success',  // alert's type
					message: 'Logo changed successfully',  // alert's message
					close: true, // make alert closable
					reset: true, // close all previouse alerts first
					focus: true, // auto scroll to the alert after shown
					closeInSeconds: 5, // auto close after defined seconds
					icon: '' // put icon before the message
				});

				$els.removeClass('fileinput-exists');
				$els.addClass('fileinput-new')
				$el.wrap('<form>').closest('form').get(0).reset();
				$el.unwrap();
				var img_html = '<img style="width:100%;height:100%;" src="./assets/images/company_setting/'+data+'" />';
				$el_imag.html(img_html);

				setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 1000);
			},
			processData: false,
			contentType: false,
			error: function(data)
		   {
			   console.log(data);
		   }
		});
	});

	$('body').delegate('#thankyou_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var funal_text = $(this).find('div.panel-body').html();
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'thankyou_text':funal_text},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_1', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'Register End page Text Updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#register_email_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#registeremail_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_2', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'Register Email text Updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#location_schedule_email_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#location_schedule_email_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_3', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'email template updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#location_schedule_delete_user_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#schedule_delete_user_email_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_3', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'email template updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#train_schedule_email_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#train_schedule_email_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_4', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'email template updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#train_schedule_delete_user_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#train_schedule_delete_user_email_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_4', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'email template updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#staff_schedule_email_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#staff_schedule_email_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_5', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'email template updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});

	$('body').delegate('#staff_schedule_delete_user_text_form', 'submit', function (e) {
	  e.preventDefault();
	  var form = $(this);
	  var final_content = $(this).find('div.panel-body').html();
	  var final_subject = $(this).find('#staff_schedule_delete_user_email_subject').val();
	  console.log(final_subject);
	  var formaction =  $(this).attr("action");
	  var token = $(this).find("input[name=_token]").val();

	  $.ajax({
		type: "POST",
		url: formaction,
		data: {'_token': token, 'email_subject':final_subject, 'email_content':final_content},
		cache: false,
		success: function(data){
			App.alert({
				container: '#bootstrap_alerts_demo_5', // alerts parent container(by default placed after the page breadcrumbs)
				place: 'append', // append or prepent in container
				type: 'success',  // alert's type
				message: 'email template updated',  // alert's message
				close: true, // make alert closable
				reset: true, // close all previouse alerts first
				focus: true, // auto scroll to the alert after shown
				closeInSeconds: 5, // auto close after defined seconds
				icon: '' // put icon before the message
			});
		}
	  });
	});


});

var validate = function () {
    var handleadduser = function () {
         $('.add-user-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {

	                first_name: {
	                    required: true
	                },
									last_name: {
	                    required: true
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
                    email_confirmation: {
						equalTo: "#register_email"
					},
	                password: {
	                    required: true,
											minlength: 6
	                },
	                password_confirmation: {
	                    equalTo: "#register_password"
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.add-user-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.add-user-form').validate().form()) {
	                    $('.add-user-form').submit();
	                }
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {

            handleadduser();
        }
    };
}();

jQuery(document).ready(function() {
    validate.init();
});

var Location = function () {

	var handleLocation = function () {

         $('.location-add-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                location_title: {
	                    required: true
	                },
	                street_address: {
	                    required: true
	                },
	                town_address: {
	                    required: true
	                },
	                province: {
	                    required: true
	                },
	                postal: {
	                    required: true,
                        digits: true
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-control').addClass('has-error'); // set error class to the control group
                        // console.log($(element).closest('.form-group'));
	            },

	            success: function (label) {
	                label.closest('.form-control').removeClass('has-error');
	                label.remove();
                    // console.log(label);
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element.closest('.input-icon'));
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
					$('.page-loader-wrapper').fadeIn();
	            }
	        });

			$('.location-add-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.location-add-form').validate().form()) {
	                    $('.location-add-form').submit();
						$('.page-loader-wrapper').fadeIn();
	                }
	                return false;
	            }
	        });
	}

	var handleEventAdd = function () {

         $('.eventAdd-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                title: {
	                    required: true
	                },
	                date_start: {
	                    required: true
	                },
	                time_start: {
	                    required: true
	                },
	                date_end: {
	                    required: true
	                },
	                color: {
	                    required: true
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-control').addClass('has-error'); // set error class to the control group
                        // console.log($(element).closest('.form-group'));
	            },

	            success: function (label) {
	                label.closest('.form-control').removeClass('has-error');
	                label.remove();
                    // console.log(label);
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element.closest('.input-icon'));
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.eventAdd-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.eventAdd-form').validate().form()) {
	                    $('.eventAdd-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleEventEdit = function () {

         $('.eventEdit-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                _title: {
	                    required: true
	                },
	                _date_start: {
	                    required: true
	                },
	                _time_start: {
	                    required: true
	                },
	                _date_end: {
	                    required: true
	                },
	                _color: {
	                    required: true
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-control').addClass('has-error'); // set error class to the control group
                        // console.log($(element).closest('.form-group'));
	            },

	            success: function (label) {
	                label.closest('.form-control').removeClass('has-error');
	                label.remove();
                    // console.log(label);
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element.closest('.input-icon'));
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.eventEdit-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.eventEdit-form').validate().form()) {
	                    $('.eventEdit-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleUserAvartar = function () {

         $('.user-avartar-upload-form').validate({
	            errorElement: 'div', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                user_image: {
	                    required: true
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.btn-file').addClass('has-error'); // set error class to the control group
                        // console.log($(element).closest('.form-group'));
	            },

	            success: function (label) {
	                label.closest('.btn-file').removeClass('has-error');
	                label.remove();
                    // console.log(label);
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.user-avartar-upload-form input').change(function (e) {
                $('.user-avartar-upload-form').validate().form();
	        });
	}

	var handleUserPassword = function () {

         $('.changepassword-form').validate({
	            errorElement: 'div', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: true, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                current_password: {
	                    required: true
	                },
					password: {
	                    required: true,
						minlength: 6
	                },
	                password_confirmation: {
	                    equalTo: "#change_password"
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
                        // console.log($(element).closest('.form-group'));
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
                    // console.log(label);
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.changepassword-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.changepassword-form').validate().form()) {
	                    $('.changepassword-form').submit();
	                }
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {

            handleLocation();
			handleEventAdd();
			handleEventEdit();
			handleUserAvartar();
			handleUserPassword();

        }
    };

}();

jQuery(document).ready(function() {
    Location.init();
});

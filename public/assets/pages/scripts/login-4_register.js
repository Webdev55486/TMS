var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                email: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit
	                $('.alert-danger', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	}

	var handleRegister = function () {
         $('.register-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {

	                first_name: {
	                    required: true
	                },
									last_name: {
	                    required: true
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
					email_confirmation: {
						equalTo: "#register_email"
					},
	                password: {
	                    required: true,
											minlength: 6
	                },
	                password_confirmation: {
	                    equalTo: "#register_password"
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleRegisterUserInfo = function () {
         $('.registerUserInfo-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {

	                street: {
	                    required: true
	                },
					city: {
	                    required: true
	                },
	                state: {
	                    required: true
	                },
					zip: {
	                    required: true
	                },
	                employer: {
	                    required: true
	                },
					phone_cell: {
						required: true
					},
					phone_home: {
						required: true
					}
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container
	                    error.insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.registerUserInfo-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.registerUserInfo-form').validate().form()) {
	                    $('.registerUserInfo-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleRegisterSchedule = function () {
		function format(state) {
				if (!state.id) { return state.text; }
				var $state = $(
				 '<span>' + state.text + '</span>'
				);

				return $state;
		}

		if (jQuery().select2 && $('#volunteered_year_list').size() > 0) {
				$("#volunteered_year_list").select2({
					placeholder: '&nbsp;Select Volunteered Year',
					templateResult: format,
					templateSelection: format,
					width: 'auto',
					escapeMarkup: function(m) {
							return m;
					}
			});


			$('#volunteered_year_list').change(function() {
					$('.registerlocations-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
					var check_shirt = $('#ischeck_shirt_size').val();
					if(check_shirt > 0) {
						var volunteer_year = $('#volunteered_year_list').val();
						// console.log(volunteer_year);
						if (volunteer_year > 1) {
							$('#select_shirt_size_row').css({'display': 'none'});
						}else {
							$('#select_shirt_size_row').css({'display': 'block'});
						}
					}
			});
		}

         $('.registerlocations-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
					volunteered_year: {
	                    required: true
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
					var isselected_location = check_select_location();
					var isselected_training = check_select_traning();
					if (isselected_location > 0 && isselected_training > 0) {
						// console.log("selected");
						form.submit();
					}
					else {
						alert('Please select one location date and training session date at least.')
					}
	            }
	        });

			// function check_tshirt() {
			// 	var check_shirt = $('#ischeck_shirt_size').val();
			// 	if(check_shirt > 0) {
			// 		return true;
			// 	}else {
			// 		var current_tshirt_size = $('#shirt_size_list').val();
			// 		console.log(current_tshirt_size);
			// 	}
			// }

			function check_select_location() {
				var location_check_count = 0;
				var traning_check_count = 0;
				$("input:checkbox[name='user_events[]']").each(function(){
					if ($(this).prop('checked')==true){
						location_check_count += 1;
					}
				});
				return location_check_count;
			}

			function check_select_traning() {
				var traning_check_count = 0;
				var exist_training_session = 0;
				$("input:checkbox[name='user_t_events[]']").each(function(){
					exist_training_session += 1;
					if ($(this).prop('checked')==true){
						traning_check_count += 1;
					}
				});
				// console.log(location_check_count);
				if (exist_training_session > 0) {
					return traning_check_count;
				}
				else {
					return 1;
				}
			}

			$('.registerlocations-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.registerlocations-form').validate().form()) {
						var isselected_location = check_select_location();
						var isselected_training = check_select_traning();
						if (isselected_location > 0 && isselected_training > 0) {
							// console.log("selected");
							$('.registerlocations-form').submit();
						}
						else {
							alert('Please select one location date and training session date at least.')
						}
	                }
	                return false;
	            }
	        });
	}

	var handleResetPassword = function () {

		        function format(state) {
            if (!state.id) { return state.text; }
            var $state = $(
             '<span><img src="../assets/global/img/flags/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
            );

            return $state;
        }

         $('.reset-form').validate({
	            errorElement: 'span', //default input error message container
	            errorClass: 'help-block', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                },
	                password: {
	                    required: true
	                },
	                password_confirmation: {
	                    equalTo: "#reset-password"
	                },
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.form-group').addClass('has-error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.form-group').removeClass('has-error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.closest('.input-icon').size() === 1) {
	                    error.insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.reset-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.reset-form').validate().form()) {
	                    $('.reset-form').submit();
	                }
	                return false;
	            }
	        });
	}

    return {
        //main function to initiate the module
        init: function () {

            handleLogin();
            handleForgetPassword();
            handleRegister();
			handleResetPassword();
			handleRegisterUserInfo();
			handleRegisterSchedule();

            // init background slide images

        }
    };

}();

jQuery(document).ready(function() {
    Login.init();
});

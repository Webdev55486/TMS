<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleTrainingSession extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'training_session_schedules';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'event_id', 'user_id', 'start_time', 'end_time', 'ns', 'ex', 'cm'
    ];
    public $timestamps = false;
}

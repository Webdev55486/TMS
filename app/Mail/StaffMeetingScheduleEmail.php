<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class StaffMeetingScheduleEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $user;
     public $setting;
     public $staffmeeting;
     public $event;

    public function __construct($user, $setting, $staffmeeting, $event)
    {
        $this->user = $user;
        $this->setting = $setting;
        $this->staffmeeting = $staffmeeting;
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->setting->staff_subject)->markdown('emails.orders.staff');
    }
}

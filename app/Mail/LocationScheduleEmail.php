<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LocationScheduleEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $user;
     public $setting;
     public $location;
     public $event;

    public function __construct($user, $setting, $location, $event)
    {
        $this->user = $user;
        $this->setting = $setting;
        $this->location = $location;
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->setting->location_subject)->markdown('emails.orders.shipped');
    }
}

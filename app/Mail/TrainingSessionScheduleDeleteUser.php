<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrainingSessionScheduleDeleteUser extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

     public $user;
     public $setting;
     public $training_session;
     public $event;

    public function __construct($user, $setting, $training_session, $event)
    {
        $this->user = $user;
        $this->setting = $setting;
        $this->training_session = $training_session;
        $this->event = $event;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->setting->train_schedule_delete_user_subject)->markdown('emails.orders.trainingSessionUserDelete');
    }
}

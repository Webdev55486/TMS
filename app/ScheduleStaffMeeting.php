<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleStaffMeeting extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'staff_meetings_schedules';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'event_id', 'user_id', 'start_time', 'end_time', 'ns', 'ex'
    ];
    public $timestamps = false;
}

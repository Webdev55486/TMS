<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingSession extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'training_sessions';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'title', 'street', 'room', 'city_town', 'state_province', 'zip_postal', 'country_code', 'isTBD', 'coor_lat', 'coor_lng'
    ];

    public $timestamps = false;
}

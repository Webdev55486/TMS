<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserTasks extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'user_tasks';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'title', 'description'
    ];

    public $timestamps = false;
}

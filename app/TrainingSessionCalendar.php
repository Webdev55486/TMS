<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TrainingSessionCalendar extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'cal_training_sesstion';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'date', 'start_time', 'end_time', 'training_session_id'
    ];
    public $timestamps = false;
}

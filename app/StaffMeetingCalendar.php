<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffMeetingCalendar extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'cal_staff_meeting';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'date', 'start_time', 'end_time', 'staff_meeting_id'
    ];
    public $timestamps = false;
}

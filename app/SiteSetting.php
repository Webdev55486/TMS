<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'site_settings';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'company_name', 'company_street', 'company_city', 'company_state', 'company_zip',
        'accept_volunteer', 'enable_logo', 'register_backstretch', 'register_background', 'register_form', 'copyright_color',
        'register_email_subject', 'register_email_contents'
    ];
    public $timestamps = false;
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'locations';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'title', 'street', 'room', 'city_town', 'state_province', 'zip_postal', 'country_code', 'coor_lat', 'coor_lng', 'isSaturday', 'manager'
    ];

    public $timestamps = false;
}

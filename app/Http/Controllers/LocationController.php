<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\LocationCalendar;
use App\ScheduleLocation;
use App\SiteSetting;
use App\Location;
use App\Country;
use App\User;
use Auth;

class LocationController extends Controller
{

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (Auth::user()->user_role > 1) {
            $locations = Location::join('countries', 'countries.code', '=', 'locations.country_code')
            ->select('locations.*', 'countries.name')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();
        }
        else if(Auth::user()->user_role == 1){
            $locations = Location::where('manager', Auth::user()->id)->join('countries', 'countries.code', '=', 'locations.country_code')
            ->select('locations.*', 'countries.name')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();
        }
        else {
            return redirect()->route('dashboard');
        }

        $users = User::all();
        return view('location',['locations' => $locations, 'users' => $users]);
    }

    public function showLocationAddForm()
    {
        $countries = Country::all();
        $setting = SiteSetting::get()->first();
        return view('locationAdd',['countries' => $countries, 'setting' => $setting]);
    }

    public function store(Request $request)
    {
        //dd($request);
        $location = new Location();
        $location->title = $request->location_title;
        $location->street = $request->street_address;
        $location->room = $request->room;
        $location->city_town = $request->town_address;
        $location->state_province = $request->province;
        $location->zip_postal = $request->postal;
        $location->country_code = $request->country;
        $location->coor_lat = $request->coorlat;
        $location->coor_lng = $request->coorlng;
        if($request->follow_saturday){
            $location->isSaturday = $request->follow_saturday;
        }
        else{
            $location->isSaturday = 0;
        }

        $location->save();

        if($request->location_image)
        {
            $result_validate = $this->validate($request, [
                'location_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $imageRand = rand(1000, 9999);
            $random_name = $location->id."_".$imageRand;

            $img = $request->location_image;
            // dd($img);
            // $imageName = $random_name.'.'.$img->getClientOriginalExtension();
            $location->img_name = $random_name;
            $location->save();

            $dst = public_path('assets/images/location/');

            $img->move($dst, $location->img_name.".jpg");

            $new_img = $dst.$location->img_name.".jpg";
            // dd($new_img);
            if (($img_info = getimagesize($new_img)) === FALSE)
                return back()->with('error', 'Image not found or not an image');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
              case IMAGETYPE_GIF  : $src = imagecreatefromgif($new_img);  break;
              case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($new_img); break;
              case IMAGETYPE_PNG  : $src = imagecreatefrompng($new_img);  break;
              default : return back()->with('error', 'Unknown file type');
            }
            $thumbnail = public_path('assets/images/location/') .$random_name.'_thumbnail';

            // $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(200,  200);
            // imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
            // imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
        }
        return redirect('location/edit/'.$location->id.'/#calendar');
    }

    public function view_location($id)
    {
        $location = Location::find($id);

        $countries = Country::all();

        $userlist = User::all();

        if($location)
        {
            if (Auth::user()->user_role > 1) {
                return view('locationView',['location' => $location, 'countries' => $countries, 'users' => $userlist]);
            }
            else {
                if ($location->manager == Auth::user()->id) {
                    return view('locationView',['location' => $location, 'countries' => $countries, 'users' => $userlist]);
                }
                else {
                    return redirect('/location');
                }
            }
        }
        else
        {
            return redirect('/location');
        }
    }

    public function edit_location($id)
    {
        $location = Location::find($id);

        $countries = Country::all();
        if($location)
        {
            return view('locationEdit',['location' => $location, 'countries' => $countries]);
        }
        else
        {
            return redirect('/location');
        }
    }

    public function update(Request $request)
    {
        //dd($request);
        $location = Location::find($request->location_id_edit);
        if($location->id){
            $location->title = $request->location_title;
            $location->street = $request->street_address;
            $location->room = $request->room;
            $location->city_town = $request->town_address;
            $location->state_province = $request->province;
            $location->zip_postal = $request->postal;
            $location->country_code = $request->country;
            $location->coor_lat = $request->coorlat;
            $location->coor_lng = $request->coorlng;

            $location->save();

            if($request->location_image)
            {
                $result_validate = $this->validate($request, [
                    'location_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                ]);

                $imageRand = rand(1000, 9999);
                $random_name = $location->id."_".$imageRand;

                $img = $request->location_image;
                // dd($img);
                // $imageName = $random_name.'.'.$img->getClientOriginalExtension();
                $location->img_name = $random_name;
                $location->save();

                $dst = public_path('assets/images/location/');

                $img->move($dst, $location->img_name.".jpg");

                $new_img = $dst.$location->img_name.".jpg";
                // dd($new_img);
                if (($img_info = getimagesize($new_img)) === FALSE)
                    return back()->with('error', 'Image not found or not an image');

                $width = $img_info[0];
                $height = $img_info[1];

                switch ($img_info[2]) {
                  case IMAGETYPE_GIF  : $src = imagecreatefromgif($new_img);  break;
                  case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($new_img); break;
                  case IMAGETYPE_PNG  : $src = imagecreatefrompng($new_img);  break;
                  default : return back()->with('error', 'Unknown file type');
                }
                $thumbnail = public_path('assets/images/location/') .$random_name.'_thumbnail';

                // $tmp = imagecreatetruecolor($width, $height);
                $tmp_thumbnail = imagecreatetruecolor(200,  200);
                // imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
                imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
                // imagejpeg($tmp, $dst . ".jpg");
                imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            }
            return back()->with('status', 'Location Updated');
        }
        else {
            return back()->with('error', 'Something went Wrong');
        }
    }

    public function destroy($id)
    {
        $location = Location::find($id);

        if($location == null)
        {
            return Response()->json([
                'message'   =>  'error delete.'
            ]);
        }

        if(file_exists('assets/images/location'.'/'.$location->img_name.'_thumbnail.jpg'))
        {
            unlink('assets/images/location'.'/'.$location->img_name.'_thumbnail.jpg');
            unlink('assets/images/location'.'/'.$location->img_name.'.jpg');
        }

        $datas = LocationCalendar::where('location_id', $id)->get();
        foreach ($datas as $data) {
            $this->destroyEvent($data->id);
        }

        $location->delete();

        return Response()->json([
            'message'   =>  'success.'
        ]);
    }

    public function destroyEvent($event_id) {
        $event = LocationCalendar::find($event_id);

        if($event != null){
            $schedules = ScheduleLocation::where('event_id', $event_id)->get();
            foreach ($schedules as $schedule) {
                $this->destroySchedule($schedule->id);
            }
            $event->delete();
        }
    }

    public function destroySchedule($schedule_id) {
        $schedule = ScheduleLocation::find($schedule_id);
        if($schedule != null){
            $schedule->delete();
        }
    }

    public function reset_saturday($location_id, $statue) {
        $location = Location::find($location_id);
        $location->isSaturday = $statue;
        $location->save();
        return "sucsess";
    }

    public function assign_manager(Request $request) {
        if (Auth::user()->user_role > 1)
        {
            $location = Location::find($request->location_id);
            $managers = explode(',', $location->manager);
            if (in_array($request->manager_user_id, $managers)) {
                return back()->with('error', 'User already assigned as manager');
            }
            else {
                if ($location->manager == null) {
                    $location->manager = $request->manager_user_id;
                }
                else {
                    $location->manager = $location->manager.",".$request->manager_user_id;
                }
                $location->save();
                return back()->with('status', 'Assigned Manager');
            }
        }
        else {
            return back()->with('error', 'Permission Deinied');
        }
    }

    public function destroy_manager($location_id, $user_id) {
        $location = Location::find($location_id);
        $managers = explode(',', $location->manager);
        $reset_manager = null;
        foreach ($managers as $manager) {
            if ($user_id != $manager) {
                if ($reset_manager == null) {
                    $reset_manager = $manager;
                }
                else {
                    $reset_manager = $reset_manager.",".$manager;
                }
            }
        }
        $location->manager = $reset_manager;
        $location->save();
        return "success";
    }
}

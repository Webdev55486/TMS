<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserTasks;

class UserTaskController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_task = UserTasks::get();
        return view('userTask',['user_tasks' => $user_task]);
    }

    public function store(Request $request)
    {
        $user_task = new UserTasks();
        $user_task->title = $request->task_title;
        $user_task->description = $request->task_description;
        $user_task->save();

        return back()->with('status', 'New Task Created');
    }

    public function update(Request $request)
    {
        $user_task = UserTasks::find($request->task_id);
        $user_task->title = $request->_task_title;
        $user_task->description = $request->_task_description;
        $user_task->save();

        return back()->with('status', 'Task Edited');
    }

    public function get_single($id)
    {
        $user_task = UserTasks::find($id);

        return $user_task;
    }

    public function destroy($id)
    {
        $user_task = UserTasks::find($id);

        if($user_task == null)
            return Response()->json([
                'message'   =>  'error delete.'
            ]);

        $user_task->delete();

        return Response()->json([
            'message'   =>  'success delete.'
        ]);

    }
}

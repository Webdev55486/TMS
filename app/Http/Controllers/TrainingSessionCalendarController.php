<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TrainingSessionCalendar;
use App\ScheduleTrainingSession;
use DateTime;

class TrainingSessionCalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($training_session_id)
    {
        $datas = TrainingSessionCalendar::where('training_session_id', $training_session_id)->get(['date', 'start_time', 'end_time']);

        $myArray = array();

        foreach ($datas as $data)
        {
            $time1 = date( "g:i a", strtotime($data->start_time));
            $time2 = date( "g:i a", strtotime($data->end_time));
            $myArray[] = array('start' => $data->date, 'end' => $data->date, 'color' => '#ff0000', 'rendering' => 'background', 'title' => $time1." - ".$time2);
        }
        return Response()->json($myArray);
    }

    public function event($training_session_id, $event_date)
    {
        $data = TrainingSessionCalendar::where('training_session_id', $training_session_id)->where('date', $event_date)->get(['id', 'training_session_id', 'date', 'start_time', 'end_time'])->first();

        if($data == null)
        {
            return "nodata";
        }
        else {
            $event_id = $data->id;
            $schedule_training_sessions = ScheduleTrainingSession::where('event_id', $event_id)->join('users', 'users.id', '=', 'training_session_schedules.user_id')
            ->select('training_session_schedules.*', 'users.first_name', 'users.last_name', 'users.dietary')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();


            $myArray = array();

            foreach ($schedule_training_sessions as $schedule_training_session)
            {
                $user_name = $schedule_training_session->first_name." ".$schedule_training_session->last_name;
                $start_time = date( "g:i a", strtotime( $schedule_training_session->start_time ) );
                $end_time = date( "g:i a", strtotime( $schedule_training_session->end_time ) );
                $user_dietary = $schedule_training_session->dietary;

                $myArray[] = array('schedule_id' => $schedule_training_session->id, 'start' => $start_time, 'end' => $end_time, 'end_timestamp' => $schedule_training_session->end_time, 'name' => $user_name, 'dietary' => $user_dietary,'ns' => $schedule_training_session->ns, 'ex' => $schedule_training_session->ex, 'cm' => $schedule_training_session->cm);
            }

            $default_start_time = date( "g:i a", strtotime( $data->start_time ) );
            $default_end_time = date( "g:i a", strtotime( $data->end_time ) );

            $finalevents = array('id' => $event_id, 'default_start_time' => $default_start_time, 'default_end_time' => $default_end_time, 'schedule_datas' => $myArray);

            return Response()->json($finalevents);
        }
    }

    public function event_edit($training_session_id, $event_date)
    {
        $data = TrainingSessionCalendar::where('training_session_id', $training_session_id)->where('date', $event_date)->get(['id', 'training_session_id', 'date', 'start_time', 'end_time'])->first();

        if($data == null)
        {
            return "nodata";
        }
        else {
            $start_time = date( "g:i a", strtotime( $data->start_time ) );
            $end_time = date( "g:i a", strtotime( $data->end_time ) );
            $resultdate = DateTime::createFromFormat('Y-m-d', $data->date);
            $event_date = $resultdate->format('m-d-Y');

            $myArray = array('id' => $data->id, 'date' => $event_date, 'start_time' => $start_time, 'end_time' => $end_time,);
            return Response()->json($myArray);
        }
    }

    public function delete_schedule($schedule_id){
        $schedule_training_session = ScheduleTrainingSession::find($schedule_id);
        $schedule_training_session->delete();
        return "sucess";
    }

    public function store(Request $request)
    {
        $start_time = date( "H:i:s", strtotime( $request->time_start ) );
        $end_time = date( "H:i:s", strtotime( $request->time_end ) );
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->date_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = new TrainingSessionCalendar();
        $event->training_session_id = $request->training_session_id;
        $event->date = $event_date;
        $event->start_time = $start_time;
        $event->end_time = $end_time;
        $event->save();

        return redirect('trainingSession/edit/'.$event->training_session_id.'/#calendar')->with('goto_date', $event_date);
    }

    public function update_event(Request $request)
    {
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->_date_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = TrainingSessionCalendar::find($request->event_id);
        if ($event->id) {
            $start_time = date( "H:i:s", strtotime( $request->_time_start ) );
            $end_time = date( "H:i:s", strtotime( $request->_time_end ) );

            $event->start_time = $start_time;
            $event->end_time = $end_time;
            $event->save();

            return redirect('trainingSession/edit/'.$event->training_session_id.'/#calendar')->with('goto_date', $event_date);
        }
        return back()->with('goto_date', $event_date);
    }

    public function change_event_date(Request $request)
    {
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->_date_new_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = TrainingSessionCalendar::find($request->event_id);
        if ($event->id) {

            $event->date = $event_date;
            $event->save();

            return back()->with('status', 'schedule Date changed successfully');
        }
        return back()->with('error', 'Schedule can not find');
    }

    public function destroy($id)
    {
        $event = TrainingSessionCalendar::find($id);

        if($event == null)
        {
            return Response()->json([
                'message'   =>  'error delete.'
            ]);
        }

        $datas = ScheduleTrainingSession::where('event_id', $id)->get();
        foreach ($datas as $data) {
            $this->destroySchedule($data->id);
        }

        $event->delete();

        return Response()->json([
            'message'   =>  'success delete.'
        ]);

    }

    public function destroySchedule($schedule_id) {
        $schedule = ScheduleTrainingSession::find($schedule_id);
        if($schedule != null){
            $schedule->delete();
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\LocationScheduleDeleteUser;
use Illuminate\Support\Facades\Mail;
use App\LocationCalendar;
use App\ScheduleLocation;
use App\SiteSetting;
use App\Location;
use App\User;

class LocationScheduleController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $locations = Location::join('countries', 'countries.code', '=', 'locations.country_code')
        // ->select('locations.*', 'countries.name')
        // ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        // ->get();
        // return view('location',['locations' => $locations]);
    }

    public function store($event_id, $user_id)
    {
        $exist_user = ScheduleLocation::where('event_id' , $event_id)->where('user_id' , $user_id)->count();
        if($exist_user == 0) {

            $current_event = LocationCalendar::find($event_id);
            if($current_event->id) {
                $start_time = $current_event->start_time;
                $end_time = $current_event->end_time;
                $total_time = $current_event->total_time;

                $location_shcdule = new ScheduleLocation();
                $location_shcdule->event_id = $event_id;
                $location_shcdule->user_id = $user_id;
                $location_shcdule->start_time = $start_time;
                $location_shcdule->end_time = $end_time;
                $location_shcdule->total_time = $total_time;
                $location_shcdule->save();

                $schedule_data = ScheduleLocation::where('event_id' , $event_id)->where('user_id' , $user_id)->join('users', 'users.id', '=', 'location_schedules.user_id')
                ->select('location_schedules.*', 'users.first_name', 'users.last_name')->first();

                $user_name = $schedule_data->first_name." ".$schedule_data->last_name;
                $start_time = date( "g:i a", strtotime( $schedule_data->start_time ) );
                $end_time = date( "g:i a", strtotime( $schedule_data->end_time ) );
                $mydata = array('schedule_id' => $schedule_data->id, 'start' => $start_time, 'end' => $end_time, 'total_time' => $schedule_data->total_time, 'end_timestamp' => $schedule_data->end_time, 'name' => $user_name, 'ns' => $schedule_data->ns, 'ex' => $schedule_data->ex);

                return $mydata;
            }
        }
        else if($exist_user > 0) {
            return "exist";
        }
    }

    public function update($id, Request $request)
    {
        $start_time = date( "H:i:s", strtotime( $request->startTime ) );
        $end_time = date( "H:i:s", strtotime( $request->endTime ) );
        $schedule = ScheduleLocation::find($id);
        $schedule->start_time = $start_time;
        $schedule->end_time = $end_time;
        $schedule->save();
        return "success";
    }

    public function update_total_time($id, Request $request)
    {
        $schedule = ScheduleLocation::find($id);
        $schedule->total_time = $request->totalTime;
        $schedule->save();
        return "success";
    }

    public function destroy($id, Request $request)
    {
        $schedule = ScheduleLocation::find($id);

        if ($schedule->id) {

            $cal_location = LocationCalendar::find($schedule->event_id);

            $location = Location::find($cal_location->location_id);

            $volunteer = User::find($schedule->user_id);

            $setting = SiteSetting::find(1);

            if ($request->master_admin_check) {
                $admins = User::where('user_role', 3)->get();
                foreach ($admins as $admin) {
                    Mail::to($admin->email)->send(new LocationScheduleDeleteUser($volunteer, $setting, $location, $cal_location));
                }
            }

            if ($request->staff_admin_check) {
                $staffs = User::where('user_role', 2)->get();
                if ($staffs != null) {
                    foreach ($staffs as $staff) {
                        Mail::to($staff->email)->send(new LocationScheduleDeleteUser($volunteer, $setting, $location, $cal_location));
                    }
                }
            }

            if ($request->site_manager_check) {
                if ($location->manager) {
                    $manager = User::find($location->manager);
                    Mail::to($manager->email)->send(new LocationScheduleDeleteUser($volunteer, $setting, $location, $cal_location));
                }
            }

            if ($request->volunteer_check) {
                Mail::to($volunteer->email)->send(new LocationScheduleDeleteUser($volunteer, $setting, $location, $cal_location));
            }

            $schedule->delete();

            return Response()->json([
                'message'   =>  'success delete.'
            ]);
        }

        return "error";

    }

    public function ns_change($id, $status) {
         $schedule = ScheduleLocation::find($id);
         $schedule->ns = $status;
         $schedule->save();
         return "success";
    }

    public function ex_change($id, $status) {
         $schedule = ScheduleLocation::find($id);
         $schedule->ex = $status;
         $schedule->save();
         return "success";
     }

     public function single_schedule_destroy($id)
     {
         $schedule = ScheduleLocation::find($id);

         if ($schedule->id) {

             $cal_location = LocationCalendar::find($schedule->event_id);

             $location = Location::find($cal_location->location_id);

             $volunteer = User::find($schedule->user_id);

             $setting = SiteSetting::find(1);

            Mail::to($volunteer->email)->send(new LocationScheduleDeleteUser($volunteer, $setting, $location, $cal_location));

             $schedule->delete();

             return Response()->json([
                 'message'   =>  'success delete.'
             ]);
         }

         return "error";

     }
}

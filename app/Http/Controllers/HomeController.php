<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Location;
use App\SiteSetting;
use App\LocationCalendar;
use App\Mail\LocationScheduleEmail;
use App\ScheduleLocation;
use App\TrainingSession;
use App\TrainingSessionCalendar;
use App\Mail\TrainingSessionScheduleEmail;
use App\ScheduleTrainingSession;
use App\StaffMeeting;
use App\StaffMeetingCalendar;
use App\ScheduleStaffMeeting;
use Illuminate\Http\Request;
use App\Mail\StaffMeetingScheduleEmail;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->user_role > 1) {
            $all_volunteers = User::where('user_role' , 0)->get();
            $active_volunteer_number = 0;
            foreach ($all_volunteers as $all_volunteer) {
                $check_scheduled_user = ScheduleLocation::where('user_id', $all_volunteer->id)->count();
                if ($check_scheduled_user > 0 ) {
                    $active_volunteer_number += 1;
                }
            }

            $future_events = LocationCalendar::where('date', '>=', date("Y-m-d"))->get();
            $total_scheduled_time = 0;

            foreach ($future_events as $future_event) {
                $schedules = ScheduleLocation::where('event_id', $future_event->id)->get();
                foreach ($schedules as $schedule) {
                    $total_scheduled_time += $schedule->total_time;
                }
            }

            $all_locations = Location::all();
            $locationEvents = LocationCalendar::orderBy('date', 'ASC')->get();
            $active_locations_number = 0;
            $active_locations = array();
            foreach ($all_locations as $all_location) {
                $days_of_thislocation = LocationCalendar::where('location_id', $all_location->id)->get();
                $active_days = 0;
                foreach ($days_of_thislocation as $day_of_thislocation) {
                    $check_scheduled_day = ScheduleLocation::where('event_id', $day_of_thislocation->id)->count();
                    if ($check_scheduled_day > 0) {
                        $active_days += 1;
                    }
                }

                if ($active_days > 0) {
                    $active_locations_number += 1;
                    $active_locations[] = array('location_name' => $all_location->title, 'active_user' => $active_days);
                }
            }

            $first_year_volunteers = User::where('volunteered_year', 1)->count();
            $dashboard_topbar_data = array('active_volunteer' => $active_volunteer_number, 'active_location' => $active_locations_number, 'total_scheduled_time' => $total_scheduled_time, 'first_year_volunteer' => $first_year_volunteers);

            return view('home', ['overview_data' => $dashboard_topbar_data, 'active_locations' => $active_locations, 'locations' => $all_locations, 'events' => $locationEvents]);
        }
        else{
            return redirect()->route('profile.view');
        }
    }

    public function send_schedule_email($id) {
        $schedule_user_ids = ScheduleLocation::where('event_id', $id)->get();
        $event = LocationCalendar::find($id);
        $setting = SiteSetting::find(1);
        $current_location = Location::find($event->location_id);

        foreach ($schedule_user_ids as $schedule_user_id) {
            $user = User::find($schedule_user_id->user_id);

            Mail::to($user->email)->send(new LocationScheduleEmail($user, $setting, $current_location, $event));
        }

        return "success";
    }

    public function send_train_schedule_email($id) {
        $schedule_user_ids = ScheduleTrainingSession::where('event_id', $id)->get();
        $event = TrainingSessionCalendar::find($id);
        $setting = SiteSetting::find(1);
        $current_trainingsession = TrainingSession::find($event->training_session_id);

        foreach ($schedule_user_ids as $schedule_user_id) {
            $user = User::find($schedule_user_id->user_id);

            Mail::to($user->email)->send(new TrainingSessionScheduleEmail($user, $setting, $current_trainingsession, $event));
        }

        return "success";
    }

    public function send_staff_schedule_email($id) {
        $schedule_user_ids = ScheduleStaffMeeting::where('event_id', $id)->get();
        $event = StaffMeetingCalendar::find($id);
        $setting = SiteSetting::find(1);
        $current_staffmeeting = StaffMeeting::find($event->staff_meeting_id);

        foreach ($schedule_user_ids as $schedule_user_id) {
            $user = User::find($schedule_user_id->user_id);

            Mail::to($user->email)->send(new StaffMeetingScheduleEmail($user, $setting, $current_staffmeeting, $event));
        }

        return "success";
    }
}

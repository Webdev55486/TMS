<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Location;
use App\UserTasks;
use App\SiteSetting;
use App\ScheduleLocation;
use App\LocationCalendar;
use App\TrainingSession;
use App\ScheduleTrainingSession;
use App\TrainingSessionCalendar;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use Jrean\UserVerification\Events\UserVerified;
use Jrean\UserVerification\Facades\UserVerification as UserVerificationFacade;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;
    use VerifiesUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('showInfoRegistrationForm', 'showPlaceInfoRegistrationForm');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

     public function showRegistrationForm()
     {
         $setting = SiteSetting::find(1);
         if ($setting->accept_volunteer == 1) {
             return view('auth.register', ['setting' => $setting]);
         }
         else {
             return view('auth.notNeed', ['setting' => $setting]);
         }
     }

    protected function validator(array $data)
    {

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_role' => 0,
        ]);
    }

    public function register(Request $request)
    {
        $setting = SiteSetting::find(1);
        if($setting->accept_volunteer == 1)
        {
            $data = $request->all();

            // Standard validation
            $validator = Validator::make($data, [
                'email' => 'unique:users',
            ]);

            if ($validator->fails()) {
                return back()->with('error', 'This email address has already been taken.');
            }
            else {
                event(new Registered($user = $this->create($request->all())));

                return view('auth.registerUserInfo',['user' => $user]);
            }
        }
        return redirect('login')->with('error', 'Company not need new volunteer');
    }

    public function registerInfo(Request $request) {

        $user = User::find($request->user_id);

        $user->street = $request->street;
        $user->city = $request->city;
        $user->state = $request->state;
        $user->zip = $request->zip;
        $user->country_code = 'US';
        $user->employer = $request->employer;
        $user->phone_cell = $request->phone_cell;
        $user->phone_home = $request->phone_home;
        $user->save();

        $locations = Location::orderBy('id', 'ASC')->get();
        $locationEvents = LocationCalendar::orderBy('date', 'ASC')->get();
        $trainingSessions = TrainingSession::orderBy('id', 'ASC')->get();
        $trainingSessionEvents = TrainingSessionCalendar::orderBy('date', 'ASC')->get();
        $user_tasks = UserTasks::orderBy('id', 'ASC')->get();
        $setting = SiteSetting::find(1);

        return view('auth.registerPlaces',['user' => $user, 'setting' => $setting, 'locations' => $locations, 'events' => $locationEvents, 'trainingSessions' => $trainingSessions, 't_events' => $trainingSessionEvents, 'user_tasks' => $user_tasks]);
    }

    public function registerSchedule(Request $request) {

        $setting = SiteSetting::find(1);

        $user = User::find($request->user_id);
        $user->volunteered_year = $request->volunteered_year;
        $user->shirt_size = $request->shirt_size;
        $user->speak_lang = $request->speak_lang;
        $user->dietary = $request->dietary;
        $user->save();
        $scheduleids = $request->user_events;
        $t_scheduleids = $request->user_t_events;
        $serveFields = $request->user_serve;
        if ($scheduleids != "")
        {
            foreach($scheduleids as $id){
                $event = LocationCalendar::find($id);
                // $eventsArray[] = array('calendar_id' => $id, 'start_time' => $event->start_time, 'end_time' => $event->end_time);

                $location_shcdule = new ScheduleLocation();
                $location_shcdule->event_id = $id;
                $location_shcdule->user_id = $user->id;
                $location_shcdule->start_time = $event->start_time;
                $location_shcdule->end_time = $event->end_time;
                $location_shcdule->total_time = $event->total_time;
                $location_shcdule->save();
            }
        }

        if ($t_scheduleids != "")
        {
            foreach($t_scheduleids as $id){
                $t_event = TrainingSessionCalendar::find($id);

                $training_session_shcdule = new ScheduleTrainingSession();
                $training_session_shcdule->event_id = $id;
                $training_session_shcdule->user_id = $user->id;
                $training_session_shcdule->start_time = $t_event->start_time;
                $training_session_shcdule->end_time = $t_event->end_time;
                $training_session_shcdule->save();
            }
        }

        if ($serveFields != "")
        {
            $bestServe = "";
            foreach ($serveFields as $serveField) {
                if ($bestServe == "") {
                    $bestServe = $serveField;
                }else{
                    $bestServe = $bestServe.", ".$serveField;
                }
            }
            $user->best_serve = $bestServe;
            $user->save();
        }

        // $this->guard()->login($user);

        UserVerification::generate($user);

        UserVerification::send($user, "Thank you! We have received your registration.");

        return redirect()->route('thankyou');
    }

    public function thankyou() {
        $setting = SiteSetting::find(1);

        return view('auth.thankyou',['setting' => $setting]);
    }

    public function getVerification(Request $request, $token)
    {
        if (! $this->validateRequest($request)) {
                return redirect($this->redirectIfVerificationFails());
        }

        $user = User::where('email', $request->input('email'))->first();

        $check = $this->process($request->input('email'), $token);

        if($check == "unknown") {
            return view('auth.verifyfail', ['text' => 'User can not find']);
        }

        if($check == "verified") {
            return view('auth.verifyfail', ['text' => 'Email already Verified']);
        }

        if($check == "mismatch") {
            return view('auth.verifyfail', ['text' => 'Verify Token Mismatch']);
        }

        return view('auth.verified');
    }

    public function process($email, $token)
    {
        $user = $this->getUserByEmail($email);

        unset($user->{"password"});

        // Check if the given user is already verified.
        // If he is, we stop here.
        if($user == "error") {
            return "unknown";
        }
        if($this->isVerified($user))
        {
            return "verified";
        }

        if($this->verifyToken($user->verification_token, $token))
        {
            return "mismatch";
        }

        // dd($user);
        $this->wasVerified($user);

        return $user;
    }

    protected function getUserByEmail($email)
    {
        $user = User::where('email', $email)
            ->first();

        if ($user === null) {
            // $text = "User can not find";
            return "error";
        }

        return $user;
    }

    protected function isVerified($user)
    {
        if ($user->verified == true) {
            return true;
        }

        return false;
    }

    protected function verifyToken($storedToken, $requestToken)
    {
        if ($storedToken != $requestToken) {
            return true;
        }
        return false;
    }

    protected function wasVerified($user)
    {
        $user->verification_token = null;

        $user->verified = true;

        $user->save();

        event(new UserVerified($user));
    }
}

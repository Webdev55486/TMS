<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use \App\User;
use \App\SiteSetting;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
     protected $redirectTo = '/location';

     /**
      * Create a new controller instance.
      *
      * @return void
      */
     public function __construct()
     {
         $this->middleware('guest')->except('logout');
     }

     public function showLoginForm()
     {
         $setting = SiteSetting::find(1);
         return view('auth.login', ['setting' => $setting]);
     }

     public function validateLogin($request)
     {
         $this->validate($request, [
             'email' => 'required', 'password' => 'required'
         ]);
     }

     protected function credentials(Request $request)
     {
         return array_merge($request->only('email', 'password'));
     }

     public function login(Request $request)
     {
       $this->validateLogin($request);
       $remember = $request->input('remember');
       $getuserinfo = $this->credentials($request);
       $availablecheck = Auth::guard()->attempt($getuserinfo, $remember);

       if ($availablecheck) {
        //    if(Auth::user()->iscomplete == 1)
        //    {
               $this->isRemember($request);
               if (Auth::user()->user_role  > 1){
                   return redirect('/location');
               }else {
                   return redirect()->route('profile.view');
               }
        //    }
       }
       return redirect()->back()->with('error', 'Invalid Login – Please Try Again.');
     }

     public function isRemember($request){
         if($request->input('remember')){
             //for 30 days
             $time = time() + (86400 * 30);
             $this->cookieSet("name",$request->input('name'),$time);
             $this->cookieSet("password",$request->input('password'),$time);
         }
     }

    public function cookieSet($name, $value, $time)
    {
        setcookie($name, $value, $time, "/");
    }
}

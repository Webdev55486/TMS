<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\StaffMeetingCalendar;
use App\ScheduleStaffMeeting;
use DateTime;

class StaffMeetingCalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($staff_neeting_id)
    {
        $datas = StaffMeetingCalendar::where('staff_meeting_id', $staff_neeting_id)->get(['date', 'start_time', 'end_time']);

        $myArray = array();

        foreach ($datas as $data)
        {
            $time1 = date( "g:i a", strtotime($data->start_time));
            $time2 = date( "g:i a", strtotime($data->end_time));
            $myArray[] = array('start' => $data->date, 'end' => $data->date, 'color' => '#ff0000', 'rendering' => 'background', 'title' => $time1." - ".$time2);
        }
        return Response()->json($myArray);
    }

    public function event($staff_neeting_id, $event_date)
    {
        $data = StaffMeetingCalendar::where('staff_meeting_id', $staff_neeting_id)->where('date', $event_date)->get(['id', 'staff_meeting_id', 'date', 'start_time', 'end_time'])->first();

        if($data == null)
        {
            return "nodata";
        }
        else {
            $event_id = $data->id;
            $schedule_staffmeetings = ScheduleStaffMeeting::where('event_id', $event_id)->join('users', 'users.id', '=', 'staff_meetings_schedules.user_id')
            ->select('staff_meetings_schedules.*', 'users.first_name', 'users.last_name')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();


            $myArray = array();

            foreach ($schedule_staffmeetings as $schedule_staffmeeting)
            {
                $user_name = $schedule_staffmeeting->first_name." ".$schedule_staffmeeting->last_name;
                $start_time = date( "g:i a", strtotime( $schedule_staffmeeting->start_time ) );
                $end_time = date( "g:i a", strtotime( $schedule_staffmeeting->end_time ) );

                $myArray[] = array('schedule_id' => $schedule_staffmeeting->id, 'start' => $start_time, 'end' => $end_time,  'total_time' => $schedule_staffmeeting->total_time, 'end_timestamp' => $schedule_staffmeeting->end_time, 'name' => $user_name, 'ns' => $schedule_staffmeeting->ns, 'ex' => $schedule_staffmeeting->ex);
            }

            $default_start_time = date( "g:i a", strtotime( $data->start_time ) );
            $default_end_time = date( "g:i a", strtotime( $data->end_time ) );

            $finalevents = array('id' => $event_id, 'default_start_time' => $default_start_time, 'default_end_time' => $default_end_time, 'schedule_datas' => $myArray);

            return Response()->json($finalevents);
        }
    }

    public function event_edit($staff_neeting_id, $event_date)
    {
        $data = StaffMeetingCalendar::where('staff_meeting_id', $staff_neeting_id)->where('date', $event_date)->get(['id', 'staff_meeting_id', 'date', 'start_time', 'end_time', 'total_time'])->first();

        if($data == null)
        {
            return "nodata";
        }
        else {
            $start_time = date( "g:i a", strtotime( $data->start_time ) );
            $end_time = date( "g:i a", strtotime( $data->end_time ) );
            $resultdate = DateTime::createFromFormat('Y-m-d', $data->date);
            $event_date = $resultdate->format('m-d-Y');

            $myArray = array('id' => $data->id, 'date' => $event_date, 'start_time' => $start_time, 'end_time' => $end_time,'total_time' => $data->total_time);
            return Response()->json($myArray);
        }
    }

    public function store(Request $request)
    {
        $start_time = date( "H:i:s", strtotime( $request->time_start ) );
        $end_time = date( "H:i:s", strtotime( $request->time_end ) );
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->date_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = new StaffMeetingCalendar();
        $event->staff_meeting_id = $request->staff_meeting_id;
        $event->date = $event_date;
        $event->start_time = $start_time;
        $event->end_time = $end_time;
        $event->total_time = $request->total_time;
        $event->save();

        return redirect('staffMeeting/edit/'.$event->staff_meeting_id.'/#calendar')->with('goto_date', $event_date);
    }

    public function update_event(Request $request)
    {
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->_date_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = StaffMeetingCalendar::find($request->event_id);
        if ($event->id) {
            $start_time = date( "H:i:s", strtotime( $request->_time_start ) );
            $end_time = date( "H:i:s", strtotime( $request->_time_end ) );

            $event->start_time = $start_time;
            $event->end_time = $end_time;
            $event->total_time = $request->_total_time;
            $event->save();

            return redirect('staffMeeting/edit/'.$event->staff_meeting_id.'/#calendar')->with('goto_date', $event_date);
        }
        return back()->with('goto_date', $event_date);
    }

    public function destroy($id)
    {
        $event = StaffMeetingCalendar::find($id);

        if($event == null)
        {
            return Response()->json([
                'message'   =>  'error delete.'
            ]);
        }

        $datas = ScheduleStaffMeeting::where('event_id', $id)->get();
        foreach ($datas as $data) {
            $this->destroySchedule($data->id);
        }

        $event->delete();

        return Response()->json([
            'message'   =>  'success delete.'
        ]);

    }

    public function destroySchedule($schedule_id) {
        $schedule = ScheduleStaffMeeting::find($schedule_id);
        if($schedule != null){
            $schedule->delete();
        }
    }
}

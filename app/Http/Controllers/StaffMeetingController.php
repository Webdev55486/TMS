<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\StaffMeetingCalendar;
use App\ScheduleStaffMeeting;
use App\SiteSetting;
use App\StaffMeeting;
use App\Country;
use App\User;
use Auth;

class StaffMeetingController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $staff_meetings = StaffMeeting::join('countries', 'countries.code', '=', 'staff_meetings.country_code')
        ->select('staff_meetings.*', 'countries.name')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();
        return view('staffMeeting',['staff_meetings' => $staff_meetings]);
    }

    public function showStaffMeetingAddForm()
    {
        $countries = Country::all();
        $setting = SiteSetting::get()->first();
        return view('staffMeetingAdd',['countries' => $countries, 'setting' => $setting]);
    }

    public function store(Request $request)
    {
        $staff_meeting = new StaffMeeting();
        $staff_meeting->title = $request->staff_meeting_title;
        $staff_meeting->street = $request->street_address;
        $staff_meeting->room = $request->room;
        $staff_meeting->city_town = $request->town_address;
        $staff_meeting->state_province = $request->province;
        $staff_meeting->zip_postal = $request->postal;
        $staff_meeting->country_code = $request->country;
        $staff_meeting->coor_lat = $request->coorlat;
        $staff_meeting->coor_lng = $request->coorlng;
        $staff_meeting->save();

        if($request->staff_meeting_image)
        {
            $result_validate = $this->validate($request, [
                'staff_meeting_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $imageRand = rand(1000, 9999);
            $random_name = $staff_meeting->id."_".$imageRand;

            $img = $request->staff_meeting_image;
            // dd($img);
            // $imageName = $random_name.'.'.$img->getClientOriginalExtension();
            $staff_meeting->img_name = $random_name;
            $staff_meeting->save();

            $dst = public_path('assets/images/staff_meeting/');

            $img->move($dst, $staff_meeting->img_name.".jpg");

            $new_img = $dst.$staff_meeting->img_name.".jpg";
            // dd($new_img);
            if (($img_info = getimagesize($new_img)) === FALSE)
                return back()->with('error', 'Image not found or not an image');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
              case IMAGETYPE_GIF  : $src = imagecreatefromgif($new_img);  break;
              case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($new_img); break;
              case IMAGETYPE_PNG  : $src = imagecreatefrompng($new_img);  break;
              default : return back()->with('error', 'Unknown file type');
            }
            $thumbnail = public_path('assets/images/staff_meeting/') .$random_name.'_thumbnail';

            // $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(200,  200);
            // imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
            // imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
        }
        return redirect('staffMeeting/edit/'.$staff_meeting->id.'/#calendar');
    }

    public function view_staffMeeting($id)
    {
        $staff_meeting = StaffMeeting::find($id);

        $countries = Country::all();

        $userlist = User::all();

        if($staff_meeting)
        {
            return view('staffMeetingView',['staff_meeting' => $staff_meeting, 'countries' => $countries, 'users' => $userlist]);
        }
        else
        {
            return redirect('/staffMeeting');
        }
    }

    public function edit_staffMeeting($id)
    {
        $staff_meeting = StaffMeeting::find($id);

        $countries = Country::all();
        if($staff_meeting)
        {
            return view('staffMeetingEdit',['staff_meeting' => $staff_meeting, 'countries' => $countries]);
        }
        else
        {
            return redirect('/staffMeeting');
        }
    }

    public function update(Request $request)
    {
        $staff_meeting = StaffMeeting::find($request->staff_meeting_id_edit);
        if($staff_meeting->id) {
            $staff_meeting->title = $request->staff_meeting_title;
            $staff_meeting->street = $request->street_address;
            $staff_meeting->room = $request->room;
            $staff_meeting->city_town = $request->town_address;
            $staff_meeting->state_province = $request->province;
            $staff_meeting->zip_postal = $request->postal;
            $staff_meeting->country_code = $request->country;
            $staff_meeting->coor_lat = $request->coorlat;
            $staff_meeting->coor_lng = $request->coorlng;
            $staff_meeting->save();

            if($request->staff_meeting_image)
            {
                $result_validate = $this->validate($request, [
                    'staff_meeting_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                ]);

                $imageRand = rand(1000, 9999);
                $random_name = $staff_meeting->id."_".$imageRand;

                $img = $request->staff_meeting_image;
                // dd($img);
                // $imageName = $random_name.'.'.$img->getClientOriginalExtension();
                $staff_meeting->img_name = $random_name;
                $staff_meeting->save();

                $dst = public_path('assets/images/staff_meeting/');

                $img->move($dst, $staff_meeting->img_name.".jpg");

                $new_img = $dst.$staff_meeting->img_name.".jpg";
                // dd($new_img);
                if (($img_info = getimagesize($new_img)) === FALSE)
                    return back()->with('error', 'Image not found or not an image');

                $width = $img_info[0];
                $height = $img_info[1];

                switch ($img_info[2]) {
                  case IMAGETYPE_GIF  : $src = imagecreatefromgif($new_img);  break;
                  case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($new_img); break;
                  case IMAGETYPE_PNG  : $src = imagecreatefrompng($new_img);  break;
                  default : return back()->with('error', 'Unknown file type');
                }
                $thumbnail = public_path('assets/images/staff_meeting/') .$random_name.'_thumbnail';

                // $tmp = imagecreatetruecolor($width, $height);
                $tmp_thumbnail = imagecreatetruecolor(200,  200);
                // imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
                imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
                // imagejpeg($tmp, $dst . ".jpg");
                imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            }
            return back()->with('status', 'Location Updated');
        }
        else{
            return back()->with('error', 'Something went Wrong');
        }
    }

    public function destroy($id)
    {
        $staff_meeting = StaffMeeting::find($id);

        if($staff_meeting == null)
            return Response()->json([
                'message'   =>  'error delete.'
            ]);
        $datas = StaffMeetingCalendar::where('staff_meeting_id', $id)->get();
        if(file_exists('assets/images/staff_meeting/'.$staff_meeting->img_name.'_thumbnail.jpg'))
        {
            unlink('assets/images/staff_meeting/'.$staff_meeting->img_name.'_thumbnail.jpg');
            unlink('assets/images/staff_meeting/'.$staff_meeting->img_name.'.jpg');
        }

        foreach ($datas as $data) {
            $this->destroyEvent($data->id);
        }

        $staff_meeting->delete();

        return Response()->json([
            'message'   =>  'success.'
        ]);
    }

    public function destroyEvent($event_id) {
        $event = StaffMeetingCalendar::find($event_id);
        if($event != null){
            $schedules = ScheduleStaffMeeting::where('event_id', $event_id)->get();
            foreach ($schedules as $schedule) {
                $this->destroySchedule($schedule->id);
            }
            $event->delete();
        }
    }

    public function destroySchedule($schedule_id) {
        $schedule = ScheduleStaffMeeting::find($schedule_id);
        if($schedule != null){
            $schedule->delete();
        }
    }
}

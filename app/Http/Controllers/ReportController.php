<?php

namespace App\Http\Controllers;

use App\User;
use App\Location;
use App\LocationCalendar;
use App\ScheduleLocation;
use App\TrainingSession;
use App\TrainingSessionCalendar;
use App\ScheduleTrainingSession;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function dietary()
    {
        $users = User::where('user_role', 0)->get();
        $final_users = array();
        foreach ($users as $user) {
            $training_schedule = ScheduleTrainingSession::where('user_id', $user->id)->first();
            // dd($training_schedule->id);
            if ($training_schedule) {
                $cal_trainingsession = TrainingSessionCalendar::find($training_schedule->event_id);
                $training_session = TrainingSession::find($cal_trainingsession->training_session_id);
                $final_users[] = array('name' => $user->first_name." ".$user->last_name, 'dietary' => $user->dietary, 'train_title' => $training_session->title, 'date' => $cal_trainingsession->date);
            }
        }
        return view('dietary', ['users' => $final_users]);
    }

    public function tshirt()
    {
        $users = User::where('user_role', 0)->get();
        $final_users = array();
        foreach ($users as $user) {
            $schedules = ScheduleLocation::where('user_id', $user->id)->get();
            // dd($schedules);
            $check_location_id = "start";
            $final_locations = array();
            foreach ($schedules as $schedule) {
                $calendar = LocationCalendar::find($schedule->event_id);
                // dd($calendar);
                $location = Location::find($calendar->location_id);
                $check_added_location = explode(',', $check_location_id);
                if (!in_array($location->id, $check_added_location)) {
                    $check_location_id = $check_location_id.",".$location->id;
                    $final_locations[] = array('location_name' => $location->title);
                }
            }

            $user_tshirt = null;
            if($user->shirt_size == 1) {
                $user_tshirt = "Small";
            } else if($user->shirt_size == 2) {
                $user_tshirt = "Medium";
            } else if($user->shirt_size == 3) {
                $user_tshirt = "Large";
            } else if($user->shirt_size == 4) {
                $user_tshirt = "X-Large";
            } else if($user->shirt_size == 5) {
                $user_tshirt = "2X-Large";
            } else if($user->shirt_size == 6) {
                $user_tshirt = "3X-Large";
            } else if($user->shirt_size == 7) {
                $user_tshirt = "4X-Large";
            } else if($user->shirt_size == 8) {
                $user_tshirt = "5X-Large";
            } else if($user->shirt_size == 9) {
                $user_tshirt = "6X-Large";
            }

            $final_users[] = array('user_name' => $user->first_name." ".$user->last_name, 'user_locations' => $final_locations, 'user_tshirt' => $user_tshirt);

        }
        return view('tshirt', ['users' => $final_users]);
    }

    public function tshirt_first()
    {
        $users = User::where('user_role', 0)->where('volunteered_year', 1)->get();
        $final_users = array();
        foreach ($users as $user) {
            $schedules = ScheduleLocation::where('user_id', $user->id)->get();
            // dd($schedules);
            $check_location_id = "start";
            $final_locations = array();
            foreach ($schedules as $schedule) {
                $calendar = LocationCalendar::find($schedule->event_id);
                // dd($calendar);
                $location = Location::find($calendar->location_id);
                $check_added_location = explode(',', $check_location_id);
                if (!in_array($location->id, $check_added_location)) {
                    $check_location_id = $check_location_id.",".$location->id;
                    $final_locations[] = array('location_name' => $location->title);
                }
            }

            $user_tshirt = null;
            if($user->shirt_size == 1) {
                $user_tshirt = "Small";
            } else if($user->shirt_size == 2) {
                $user_tshirt = "Medium";
            } else if($user->shirt_size == 3) {
                $user_tshirt = "Large";
            } else if($user->shirt_size == 4) {
                $user_tshirt = "X-Large";
            } else if($user->shirt_size == 5) {
                $user_tshirt = "2X-Large";
            } else if($user->shirt_size == 6) {
                $user_tshirt = "3X-Large";
            } else if($user->shirt_size == 7) {
                $user_tshirt = "4X-Large";
            } else if($user->shirt_size == 8) {
                $user_tshirt = "5X-Large";
            } else if($user->shirt_size == 9) {
                $user_tshirt = "6X-Large";
            }

            $final_users[] = array('user_name' => $user->first_name." ".$user->last_name, 'user_locations' => $final_locations, 'user_tshirt' => $user_tshirt);

        }
        return view('tshirt', ['users' => $final_users]);
    }

    public function location_date_user($id) {

        $current_calendar = LocationCalendar::find($id);
        $users = ScheduleLocation::where('event_id', $id)
        ->join('users', 'users.id', '=', 'location_schedules.user_id')
        ->select('location_schedules.total_time', 'users.*')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        return view('scheduleUser', ['users' => $users]);
    }

    public function training_user() {

        $complete_users = ScheduleTrainingSession::where('cm', 1)
        ->join('users', 'users.id', '=', 'training_session_schedules.user_id')
        ->join('cal_training_sesstion', 'cal_training_sesstion.id', '=', 'training_session_schedules.event_id')
        ->join('training_sessions', 'training_sessions.id', '=', 'cal_training_sesstion.training_session_id')
        ->select('training_session_schedules.cm', 'users.first_name', 'users.last_name', 'training_sessions.title', 'cal_training_sesstion.date')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();
        return view('completeTrain', ['users' => $complete_users]);
    }

    public function volunteer() {

        $volunteers = User::where('user_role', 0)->get();
        $locations = Location::all();

        $all_volunteer_infos = array();
        foreach ($volunteers as $volunteer) {
            foreach ($locations as $location) {
                $events = LocationCalendar::where('location_id', $location->id)->where('date', '<=', date("Y-m-d"))->get();
                $total_work_time = 0;
                if ($events != null) {
                    foreach ($events as $event) {
                        $schedules = ScheduleLocation::where('event_id', $event->id)->where('user_id', $volunteer->id)->first();
                        if ($schedules != null) {
                            $total_work_time += $schedules->total_time;
                        }
                    }
                }

                if ($total_work_time != 0) {
                    $all_volunteer_infos[] = array('name' => $volunteer->first_name." ".$volunteer->last_name, 'site' => $location->title, 'total_work' => $total_work_time, 'all_years' => $volunteer->volunteered_year, 'task' => $volunteer->best_serve);
                }
            }
        }

        return view('volunteer', ['users' => $all_volunteer_infos]);
    }
}

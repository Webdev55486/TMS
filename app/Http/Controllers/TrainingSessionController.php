<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\TrainingSessionCalendar;
use App\ScheduleTrainingSession;
use App\TrainingSession;
use App\SiteSetting;
use App\Country;
use App\User;
use Auth;

class TrainingSessionController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $train_sessions = TrainingSession::join('countries', 'countries.code', '=', 'training_sessions.country_code')
        ->select('training_sessions.*', 'countries.name')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();
        return view('trainingSession',['training_sessions' => $train_sessions]);
    }

    public function showTrainingSessionAddForm()
    {
        $countries = Country::all();
        $setting = SiteSetting::get()->first();
        return view('trainingSessionAdd',['countries' => $countries, 'setting' => $setting]);
    }

    public function store(Request $request)
    {
        //dd($request);
        $train_session = new TrainingSession();
        $train_session->title = $request->training_session_title;
        $train_session->street = $request->street_address;
        $train_session->room = $request->room;
        $train_session->city_town = $request->town_address;
        $train_session->state_province = $request->province;
        $train_session->zip_postal = $request->postal;
        $train_session->country_code = $request->country;
        $train_session->coor_lat = $request->coorlat;
        $train_session->coor_lng = $request->coorlng;
        if($request->follow_tbd){
            $train_session->isTBD = $request->follow_tbd;
        }
        else{
            $train_session->isTBD = 0;
        }
        $train_session->save();

        if($request->training_session_image)
        {
            $result_validate = $this->validate($request, [
                'training_session_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);

            $imageRand = rand(1000, 9999);
            $random_name = $train_session->id."_".$imageRand;

            $img = $request->training_session_image;
            // dd($img);
            // $imageName = $random_name.'.'.$img->getClientOriginalExtension();
            $train_session->img_name = $random_name;
            $train_session->save();

            $dst = public_path('assets/images/training_session/');

            $img->move($dst, $train_session->img_name.".jpg");

            $new_img = $dst.$train_session->img_name.".jpg";
            // dd($new_img);
            if (($img_info = getimagesize($new_img)) === FALSE)
                return back()->with('error', 'Image not found or not an image');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
              case IMAGETYPE_GIF  : $src = imagecreatefromgif($new_img);  break;
              case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($new_img); break;
              case IMAGETYPE_PNG  : $src = imagecreatefrompng($new_img);  break;
              default : return back()->with('error', 'Unknown file type');
            }
            $thumbnail = public_path('assets/images/training_session/') .$random_name.'_thumbnail';

            // $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(200,  200);
            // imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
            // imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
        }
        return redirect('trainingSession/edit/'.$train_session->id.'/#calendar');
    }

    public function view_trainingSession($id)
    {
        $train_session = TrainingSession::find($id);

        $countries = Country::all();

        $userlist = User::all();

        if($train_session)
        {
            return view('trainingSessionView',['training_session' => $train_session, 'countries' => $countries, 'users' => $userlist]);
        }
        else
        {
            return redirect('/trainingSession');
        }
    }

    public function edit_trainingSession($id)
    {
        $train_session = TrainingSession::find($id);

        $countries = Country::all();
        if($train_session)
        {
            return view('trainingSessionEdit',['training_session' => $train_session, 'countries' => $countries]);
        }
        else
        {
            return redirect('/trainingSession');
        }
    }

    public function update(Request $request)
    {
        //dd($request);
        $train_session = TrainingSession::find($request->trainingSession_id_edit);
        if($train_session->id){
            $train_session->title = $request->training_session_title;
            $train_session->street = $request->street_address;
            $train_session->room = $request->room;
            $train_session->city_town = $request->town_address;
            $train_session->state_province = $request->province;
            $train_session->zip_postal = $request->postal;
            $train_session->country_code = $request->country;
            $train_session->coor_lat = $request->coorlat;
            $train_session->coor_lng = $request->coorlng;
            $train_session->save();

            if($request->training_session_image)
            {
                $result_validate = $this->validate($request, [
                    'training_session_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
                ]);

                $imageRand = rand(1000, 9999);
                $random_name = $train_session->id."_".$imageRand;

                $img = $request->training_session_image;
                // dd($img);
                // $imageName = $random_name.'.'.$img->getClientOriginalExtension();
                $train_session->img_name = $random_name;
                $train_session->save();

                $dst = public_path('assets/images/training_session/');

                $img->move($dst, $train_session->img_name.".jpg");

                $new_img = $dst.$train_session->img_name.".jpg";
                // dd($new_img);
                if (($img_info = getimagesize($new_img)) === FALSE)
                    return back()->with('error', 'Image not found or not an image');

                $width = $img_info[0];
                $height = $img_info[1];

                switch ($img_info[2]) {
                  case IMAGETYPE_GIF  : $src = imagecreatefromgif($new_img);  break;
                  case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($new_img); break;
                  case IMAGETYPE_PNG  : $src = imagecreatefrompng($new_img);  break;
                  default : return back()->with('error', 'Unknown file type');
                }
                $thumbnail = public_path('assets/images/training_session/') .$random_name.'_thumbnail';

                // $tmp = imagecreatetruecolor($width, $height);
                $tmp_thumbnail = imagecreatetruecolor(200,  200);
                // imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
                imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 200, 200, $width, $height);
                // imagejpeg($tmp, $dst . ".jpg");
                imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            }
            return back()->with('status', 'Training Session Updated');
        }
        else {
            return back()->with('error', 'Something went Wrong');
        }
    }

    public function destroy($id)
    {
        $train_session = TrainingSession::find($id);

        if($train_session == null)
            return Response()->json([
                'message'   =>  'error delete.'
            ]);
        $datas = TrainingSessionCalendar::where('training_session_id', $id)->get();
        if(file_exists('assets/images/training_session/'.$train_session->img_name.'_thumbnail.jpg'))
        {
            unlink('assets/images/training_session/'.$train_session->img_name.'_thumbnail.jpg');
            unlink('assets/images/training_session/'.$train_session->img_name.'.jpg');
        }

        foreach ($datas as $data) {
            $this->destroyEvent($data->id);
        }

        $train_session->delete();

        return Response()->json([
            'message'   =>  'success.'
        ]);
    }

    public function destroyEvent($event_id) {
        $event = TrainingSessionCalendar::find($event_id);

        if($event != null){
            $schedules = ScheduleTrainingSession::where('event_id', $event_id)->get();
            foreach ($schedules as $schedule) {
                $this->destroySchedule($schedule->id);
            }
            $event->delete();
        }
    }

    public function destroySchedule($schedule_id) {
        $schedule = ScheduleTrainingSession::find($schedule_id);
        if($schedule != null){
            $schedule->delete();
        }
    }

    public function reset_tbd($trainingsession_id, $statue) {
        $train_session = TrainingSession::find($trainingsession_id);
        $train_session->isTBD = $statue;
        $train_session->save();
        return "sucsess";
    }
}

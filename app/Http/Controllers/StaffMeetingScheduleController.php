<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\StaffMeetingCalendar;
use App\ScheduleStaffMeeting;
use App\User;

class StaffMeetingScheduleController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $locations = Location::join('countries', 'countries.code', '=', 'locations.country_code')
        // ->select('locations.*', 'countries.name')
        // ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        // ->get();
        // return view('location',['locations' => $locations]);
    }

    public function store($event_id, $user_id)
    {
        $exist_user = ScheduleStaffMeeting::where('event_id' , $event_id)->where('user_id' , $user_id)->count();
        if($exist_user == 0) {

            $current_event = StaffMeetingCalendar::find($event_id);
            if($current_event->id) {
                $start_time = $current_event->start_time;
                $end_time = $current_event->end_time;
                $total_time = $current_event->total_time;

                $location_shcdule = new ScheduleStaffMeeting();
                $location_shcdule->event_id = $event_id;
                $location_shcdule->user_id = $user_id;
                $location_shcdule->start_time = $start_time;
                $location_shcdule->end_time = $end_time;
                $location_shcdule->total_time = $total_time;
                $location_shcdule->save();

                $schedule_data = ScheduleStaffMeeting::where('event_id' , $event_id)->where('user_id' , $user_id)->join('users', 'users.id', '=', 'staff_meetings_schedules.user_id')
                ->select('staff_meetings_schedules.*', 'users.first_name', 'users.last_name')->first();

                $user_name = $schedule_data->first_name." ".$schedule_data->last_name;
                $start_time = date( "g:i a", strtotime( $schedule_data->start_time ) );
                $end_time = date( "g:i a", strtotime( $schedule_data->end_time ) );
                $mydata = array('schedule_id' => $schedule_data->id, 'start' => $start_time, 'end' => $end_time, 'total_time' => $schedule_data->total_time, 'end_timestamp' => $schedule_data->end_time, 'name' => $user_name, 'ns' => $schedule_data->ns, 'ex' => $schedule_data->ex);

                return $mydata;
            }
        }
        else if($exist_user > 0) {
            return "exist";
        }
    }

    public function update($id, Request $request)
    {
        $start_time = date( "H:i:s", strtotime( $request->startTime ) );
        $end_time = date( "H:i:s", strtotime( $request->endTime ) );
        $schedule = ScheduleStaffMeeting::find($id);
        $schedule->start_time = $start_time;
        $schedule->end_time = $end_time;
        $schedule->save();
        return "success";
    }

    public function update_total_time($id, Request $request)
    {
        $schedule = ScheduleStaffMeeting::find($id);
        $schedule->total_time = $request->totalTime;
        $schedule->save();
        return "success";
    }

    public function destroy($id)
    {
        $schedule = ScheduleStaffMeeting::find($id);

        $schedule->delete();

        return Response()->json([
            'message'   =>  'success delete.'
        ]);

    }

    public function ns_change($id, $status) {
         $schedule = ScheduleStaffMeeting::find($id);
         $schedule->ns = $status;
         $schedule->save();
         return "success";
    }

    public function ex_change($id, $status) {
         $schedule = ScheduleStaffMeeting::find($id);
         $schedule->ex = $status;
         $schedule->save();
         return "success";
     }
}

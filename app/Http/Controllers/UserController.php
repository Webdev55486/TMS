<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use DateTime;
use App\Country;
use App\Location;
use App\SiteSetting;
use App\LocationCalendar;
use App\ScheduleLocation;
use App\ScheduleTrainingSession;
use App\ScheduleStaffMeeting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile() {
        $user = Auth::user();

        $schedule_days = ScheduleLocation::where('user_id', $user->id)
        ->join('cal_location', 'cal_location.id', '=', 'location_schedules.event_id')
        ->select('location_schedules.*', 'cal_location.date', 'cal_location.location_id')
        ->orderBy('date', 'ASC')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        $schedule_days_train_session = ScheduleTrainingSession::where('user_id', $user->id)
        ->join('cal_training_sesstion', 'cal_training_sesstion.id', '=', 'training_session_schedules.event_id')
        ->select('training_session_schedules.*', 'cal_training_sesstion.date', 'cal_training_sesstion.training_session_id')
        ->orderBy('date', 'ASC')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        $schedule_days_staff_meeting = ScheduleStaffMeeting::where('user_id', $user->id)
        ->join('cal_staff_meeting', 'cal_staff_meeting.id', '=', 'staff_meetings_schedules.event_id')
        ->select('staff_meetings_schedules.*', 'cal_staff_meeting.date', 'cal_staff_meeting.staff_meeting_id')
        ->orderBy('date', 'ASC')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        // print_r($schedule_days);;
        // exit;

        return view('profile', ['schedule_days' => $schedule_days, 'train_schedule_days' => $schedule_days_train_session, 'staff_meeting_schedule_days' => $schedule_days_staff_meeting]);
    }

    public function profile_edit() {
        return view('profileEdit');
    }

    public function site_setting() {
        if (Auth::user()->user_role == 3) {
            $countries = Country::all();
            $setting = SiteSetting::find(1);
            return view('siteSetting',['countries' => $countries, 'setting' => $setting]);
        }
        else {
            return redirect()->route('dashboard');
        }
    }

    public function change_tshirt($status) {
        $setting = SiteSetting::find(1);
        $setting->shirt_check = $status;
        $setting->save();
        return 'success';
    }

    public function change_newuser($status) {
        $setting = SiteSetting::find(1);
        $setting->accept_volunteer = $status;
        $setting->save();
        return 'success';
    }

    public function reset_logo($status) {
        $setting = SiteSetting::find(1);
        $setting->enable_logo = $status;
        $setting->save();
        return 'success';
    }

    public function reset_backstretch($status) {
        $setting = SiteSetting::find(1);
        $setting->register_backstretch = $status;
        $setting->save();
        return 'success';
    }

    public function change_setting(Request $request) {

        $setting = SiteSetting::find(1);
        $setting->company_name = $request->company_name;
        $setting->company_street = $request->company_street;
        $setting->company_city = $request->company_city;
        $setting->company_state = $request->company_state;
        $setting->company_zip = $request->company_zip;
        $setting->country_code = $request->country;
        $setting->coor_lat = $request->coorlat;
        $setting->coor_lng = $request->coorlng;
        $setting->save();

        return back()->with('status', 'Company Address updated');
    }

    public function change_logo(Request $request) {

        // dd($request->company_logo);
        $setting = SiteSetting::find(1);

        $this->validate($request, [
            'company_logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        $imageRand = rand(1000, 9999);
        $random_name = time()."_".$imageRand;

        $img = $request->company_logo;

        $imageName = $random_name.'.'.$img->getClientOriginalExtension();
        $dst = public_path('assets/images/company_setting/');
        $setting->company_logo = $imageName;
        $setting->save();
        $img->move($dst, $imageName);

        // return back()->with('status', 'Logo Changed successfully');
        return Response()->json($imageName);
    }

    public function change_register_form(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->register_background = $request->background_color;
        $setting->register_form = $request->register_form_color;
        $setting->copyright_color = $request->copyright_color;
        $setting->save();
        return back()->with('status', 'Register Form Theme Updated');
    }

    public function change_thankyou_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->thankyou_text = $request->thankyou_text;
        $setting->save();
        return "success";
    }

    public function change_register_email_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->register_email_subject = $request->email_subject;
        $setting->register_email_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function change_location_schedule_email_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->location_subject = $request->email_subject;
        $setting->location_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function change_location_schedule_delete_user_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->location_schedule_delete_user_subject = $request->email_subject;
        $setting->location_schedule_delete_user_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function change_train_schedule_email_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->train_subject = $request->email_subject;
        $setting->train_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function change_train_schedule_delete_user_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->train_schedule_delete_user_subject = $request->email_subject;
        $setting->train_schedule_delete_user_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function change_staff_schedule_email_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->staff_subject = $request->email_subject;
        $setting->staff_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function change_staff_schedule_delete_user_text(Request $request) {
        $setting = SiteSetting::find(1);
        $setting->staff_schedule_delete_user_subject = $request->email_subject;
        $setting->staff_schedule_delete_user_contents = $request->email_content;
        $setting->save();
        return "success";
    }

    public function preview_register_form()
    {
        $setting = SiteSetting::find(1);

        return view('auth.preview_register', ['setting' => $setting]);

    }

    public function user_lists() {
        $users = User::all();
        return view('userManagement', ['users' => $users]);
    }

    public function view_single_user($id) {
        $user = User::find($id);
        $schedule_days = ScheduleLocation::where('user_id', $user->id)
        ->join('cal_location', 'cal_location.id', '=', 'location_schedules.event_id')
        ->select('location_schedules.*', 'cal_location.date', 'cal_location.location_id')
        ->orderBy('date', 'ASC')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        $schedule_days_train_session = ScheduleTrainingSession::where('user_id', $user->id)
        ->join('cal_training_sesstion', 'cal_training_sesstion.id', '=', 'training_session_schedules.event_id')
        ->select('training_session_schedules.*', 'cal_training_sesstion.date', 'cal_training_sesstion.training_session_id')
        ->orderBy('date', 'ASC')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        $schedule_days_staff_meeting = ScheduleStaffMeeting::where('user_id', $user->id)
        ->join('cal_staff_meeting', 'cal_staff_meeting.id', '=', 'staff_meetings_schedules.event_id')
        ->select('staff_meetings_schedules.*', 'cal_staff_meeting.date', 'cal_staff_meeting.staff_meeting_id')
        ->orderBy('date', 'ASC')
        ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        ->get();

        return view('userProfile',['user' => $user, 'schedule_days' => $schedule_days, 'train_schedule_days' => $schedule_days_train_session, 'staff_meeting_schedule_days' => $schedule_days_staff_meeting]);
    }

    public function edit_single_user($id) {
        $user = User::find($id);

        return view('userProfileEdit', ['user' => $user]);
    }

    protected function validator(array $data) {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
        ]);
    }

    protected function create(array $data) {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'user_role' => $data['user_role'],
        ]);
    }

    public function user_add(Request $request) {
        $data = $request->all();

        // // Standard validation
        if ($this->isExistEmail($request->input('email'))) {
            return back()->with('error', 'This email address has already been taken.');
        }
        else {
            event(new Registered($user = $this->create($request->all())));

            return back()->with('status', 'You have created a new user.');
        }
    }

    public function get_user($user_id) {
        $user = User::find($user_id);
        return $user;
    }

    public function user_role_change(Request $request){
        $user = User::find($request->user_id);
        if( $user->user_role == $request->user_role ){
            return back()->with('error', 'User already have that level');
        }
        else {
            $user->user_role = $request->user_role;
            $user->save();
            return back()->with('status', 'User role Updated');
        }
    }

    public function profile_edit_bio(Request $request) {
        $user = Auth::user();
        if ($user->id) {

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->dietary = $request->dietary;
            $user->street = $request->street;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->zip = $request->zip;
            $user->employer = $request->employer;
            $user->volunteered_year = $request->volunteered_year;
            $user->shirt_size = $request->shirt_size;
            $user->phone_cell = $request->phone_cell;
            $user->phone_home = $request->phone_home;
            $user->save();

            return back()->with('status', 'Profile Bio Edited successfully')->with('tap', 'bio');
        }
        else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'bio');
        }
    }

    public function profile_edit_task(Request $request) {
        $user = Auth::user();
        if ($user->id) {

            $serveFields = $request->user_task;

            if ($serveFields != "")
            {
                $bestServe = "";
                foreach ($serveFields as $serveField) {
                    if ($bestServe == "") {
                        $bestServe = $serveField;
                    }else{
                        $bestServe = $bestServe.", ".$serveField;
                    }
                }
                $user->best_serve = $bestServe;
                $user->save();
                return back()->with('status', 'Profile task Edited successfully')->with('tap', 'bio');
            }else {
                return back()->with('error', 'Please Select task')->with('tap', 'bio');
            }
        }
        else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'bio');
        }
    }

    public function profile_edit_email(Request $request) {
        $obj_user = Auth::user();
        if ($obj_user->id) {
            //if Verified email
            if($obj_user->email == $request->input('email'))
            {
                return back()->with('error', 'You have already this email address.')->with('tap', 'bio');
            }
            //if Email already exist
            if($this->isExistEmail($request->input('email')) == true)
            {
                return back()->with('error', 'This email address has already been taken.')->with('tap', 'bio');
            }

            $obj_user->email = $request->input('email');
            $obj_user->save();
            return back()->with('status','Email changed successfully.')->with('tap', 'bio');
        } else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'bio');
        }
    }

    public function isExistEmail($email){
        $user = User::where(array('email' => $email))->count();
        if($user != 0){
            return true;
        }else{
            return false;
        }
    }

    public function profile_edit_avartar(Request $request) {
        $user = Auth::user();
        if ($user->id) {
            $this->validate($request, [
                'user_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $imageRand = rand(1000, 9999);
            $user->photo = $user->id."_".$imageRand;
            $user->save();

            $img = $request->user_image;


            $dst = public_path('assets/images/avatar/') . $user->photo;
            $thumbnail = public_path('assets/images/avatar/') . $user->photo.'_thumbnail';
            if (($img_info = getimagesize($img)) === FALSE)
                return back()->with('error', 'Image not found or not an image')->with('tap', 'avartar');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
              case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
              case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
              case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
              default : return back()->with('error', 'Unknown file type');
            }

            $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(500,  500);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 500, 500, $width, $height);
            imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            // $imageName = time().'.'.$request->image->getClientOriginalExtension();
            // $request->image->move(public_path('images/avatar'), $imageName);

            return back()->with('status', 'Your image changed successfully')->with('tap', 'avartar');
        } else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'avartar');
        }
    }

    public function profile_edit_password(Request $request) {
        if (Auth::check()) {
            $requestData = $request->All();
            $this->validateChangePassword($requestData);
            $current_password = Auth::user()->password;
            if (Hash::Check($requestData['current_password'], $current_password)) {
                $obj_user = Auth::user();
                $obj_user->password = bcrypt($requestData['password']);
                $obj_user->save();
                return back()->with('status', 'Password changed successfully')->with('tap', 'password');
            } else {
                return back()->with('error', 'Please enter correct current password')->with('tap', 'password');
            }
        } else {
            return redirect()->to('/');
        }
    }

    private function validateChangePassword(array $data)
    {
        $messages = [
            'current_password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',
            'password.regex' => 'Must contain at least one number and one uppercase and lowercase letter, and at least 6 or more characters'
        ];

        $validator = Validator::make($data, [
            'current_password' => 'required',
        ], $messages);

        return $validator->validate();
    }

    public function edit_bio_single_user(Request $request) {
        $user = User::find($request->user_id);
        if ($user->id) {

            $user->first_name = $request->first_name;
            $user->last_name = $request->last_name;
            $user->dietary = $request->dietary;
            $user->street = $request->street;
            $user->city = $request->city;
            $user->state = $request->state;
            $user->zip = $request->zip;
            $user->employer = $request->employer;
            $user->volunteered_year = $request->volunteered_year;
            $user->shirt_size = $request->shirt_size;
            $user->phone_cell = $request->phone_cell;
            $user->phone_home = $request->phone_home;
            $user->save();

            return back()->with('status', 'Profile Bio Edited successfully')->with('tap', 'bio');
        }
        else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'bio');
        }
    }

    public function edit_task_single_user(Request $request) {
        $user = User::find($request->user_id);
        if ($user->id) {

            $serveFields = $request->user_task;

            if ($serveFields != "")
            {
                $bestServe = "";
                foreach ($serveFields as $serveField) {
                    if ($bestServe == "") {
                        $bestServe = $serveField;
                    }else{
                        $bestServe = $bestServe.", ".$serveField;
                    }
                }
                $user->best_serve = $bestServe;
                $user->save();
                return back()->with('status', 'Profile task Edited successfully')->with('tap', 'bio');
            }else {
                return back()->with('error', 'Please Select task')->with('tap', 'bio');
            }
        }
        else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'bio');
        }
    }

    public function edit_email_single_user(Request $request) {
        $user = User::find($request->user_id);
        // dd($user);
        if ($user->id) {
            //if Verified email
            if($user->email == $request->input('email'))
            {
                return back()->with('error', 'You have already this email address.')->with('tap', 'bio');
            }
            //if Email already exist
            if($this->isExistEmail($request->input('email')) == true)
            {
                return back()->with('error', 'This email address has already been taken.')->with('tap', 'bio');
            }

            $user->email = $request->input('email');
            $user->save();
            return back()->with('status','Email changed successfully.')->with('tap', 'bio');
        } else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'bio');
        }
    }

    public function edit_avartar_single_user(Request $request) {
        $user = User::find($request->user_id);
        if ($user->id) {
            $this->validate($request, [
                'user_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            ]);
            $imageRand = rand(1000, 9999);
            $user->photo = $user->id."_".$imageRand;
            $user->save();

            $img = $request->user_image;


            $dst = public_path('assets/images/avatar/') . $user->photo;
            $thumbnail = public_path('assets/images/avatar/') . $user->photo.'_thumbnail';
            if (($img_info = getimagesize($img)) === FALSE)
                return back()->with('error', 'Image not found or not an image')->with('tap', 'avartar');

            $width = $img_info[0];
            $height = $img_info[1];

            switch ($img_info[2]) {
              case IMAGETYPE_GIF  : $src = imagecreatefromgif($img);  break;
              case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($img); break;
              case IMAGETYPE_PNG  : $src = imagecreatefrompng($img);  break;
              default : return back()->with('error', 'Unknown file type');
            }

            $tmp = imagecreatetruecolor($width, $height);
            $tmp_thumbnail = imagecreatetruecolor(500,  500);
            imagecopyresampled($tmp, $src, 0, 0, 0, 0, $width, $height, $width, $height);
            imagecopyresized($tmp_thumbnail, $src, 0, 0, 0, 0, 500, 500, $width, $height);
            imagejpeg($tmp, $dst . ".jpg");
            imagejpeg($tmp_thumbnail, $thumbnail . ".jpg");
            // $imageName = time().'.'.$request->image->getClientOriginalExtension();
            // $request->image->move(public_path('images/avatar'), $imageName);

            return back()->with('status', 'Your image changed successfully')->with('tap', 'avartar');
        } else {
            return back()->with('error', 'You have no access for this action')->with('tap', 'avartar');
        }
    }

    public function edit_password_single_user(Request $request) {
        $obj_user = User::find($request->user_id);

        $requestData = $request->All();

        $obj_user->password = bcrypt($requestData['password']);
        $obj_user->save();
        return back()->with('status', 'Password changed successfully')->with('tap', 'password');
    }

    public function delete_single_user($id)
    {
        $obj_user = User::find($id);

        if($obj_user == null)
            return Response()->json([
                'message'   =>  'error delete.'
            ]);

        $obj_user->delete();

        return Response()->json([
            'message'   =>  'success delete.'
        ]);

    }

    public function change_env_maildetail(Request $request) {
        $env = file_get_contents(base_path() . '/.env');
        $env = preg_split('/\s+/', $env);

        foreach($env as $env_key => $env_value){

            // Turn the value into an array and stop after the first split
            // So it's not possible to split e.g. the App-Key by accident
            $entry = explode("=", $env_value, 2);

            // Check, if new key fits the actual .env-key
            if($entry[0] == "APP_NAME"){
                // If yes, overwrite it with the new one
                $env[$env_key] = "APP_NAME" . "=" . $request->app_name;

            }else if ($entry[0] == "MAIL_FROM_ADDRESS") {

                $env[$env_key] = "MAIL_FROM_ADDRESS" . "=" . $request->mail_address;

            }else if ($entry[0] == "MAIL_FROM_NAME") {

                $env[$env_key] = "MAIL_FROM_NAME" . "=" . $request->mail_name;

            }else {
                // If not, keep the old one
                $env[$env_key] = $env_value;
            }
        }

        $env = implode("\n", $env);

        file_put_contents(base_path() . '/.env', $env);

        return back()->with('status', 'Email Detail Updated');
        // dd($env);
    }
}

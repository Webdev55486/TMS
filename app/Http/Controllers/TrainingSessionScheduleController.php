<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\TrainingSessionScheduleDeleteUser;
use Illuminate\Support\Facades\Mail;
use App\TrainingSessionCalendar;
use App\ScheduleTrainingSession;
use App\TrainingSession;
use App\SiteSetting;
use App\User;

class TrainingSessionScheduleController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $locations = Location::join('countries', 'countries.code', '=', 'locations.country_code')
        // ->select('locations.*', 'countries.name')
        // ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
        // ->get();
        // return view('location',['locations' => $locations]);
    }

    public function store($event_id, $user_id)
    {
        $exist_user = ScheduleTrainingSession::where('event_id' , $event_id)->where('user_id' , $user_id)->count();
        if($exist_user == 0) {

            $current_event = TrainingSessionCalendar::find($event_id);
            if($current_event->id) {
                $start_time = $current_event->start_time;
                $end_time = $current_event->end_time;

                $location_shcdule = new ScheduleTrainingSession();
                $location_shcdule->event_id = $event_id;
                $location_shcdule->user_id = $user_id;
                $location_shcdule->start_time = $start_time;
                $location_shcdule->end_time = $end_time;
                $location_shcdule->save();

                $schedule_data = ScheduleTrainingSession::where('event_id' , $event_id)->where('user_id' , $user_id)->join('users', 'users.id', '=', 'training_session_schedules.user_id')
                ->select('training_session_schedules.*', 'users.first_name', 'users.last_name', 'users.dietary')->first();

                $user_name = $schedule_data->first_name." ".$schedule_data->last_name;
                $start_time = date( "g:i a", strtotime( $schedule_data->start_time ) );
                $end_time = date( "g:i a", strtotime( $schedule_data->end_time ) );
                $user_dietary = $schedule_data->dietary;

                $mydata = array('schedule_id' => $schedule_data->id, 'start' => $start_time, 'end' => $end_time, 'end_timestamp' => $schedule_data->end_time, 'name' => $user_name, 'dietary' => $user_dietary, 'ns' => $schedule_data->ns, 'ex' => $schedule_data->ex);

                return $mydata;
            }
        }
        else if($exist_user > 0) {
            return "exist";
        }
    }

    public function ns_change($id, $status) {
         $schedule = ScheduleTrainingSession::find($id);
         $schedule->ns = $status;
         $schedule->save();
         return "success";
    }

    public function ex_change($id, $status) {
         $schedule = ScheduleTrainingSession::find($id);
         $schedule->ex = $status;
         $schedule->save();
         return "success";
    }

    public function cm_change($id, $status) {
         $schedule = ScheduleTrainingSession::find($id);
         $schedule->cm = $status;
         $schedule->save();
         return "success";
    }

     public function destroy($id, Request $request)
     {
         $schedule = ScheduleTrainingSession::find($id);

         if ($schedule->id) {

             $cal_training_session = TrainingSessionCalendar::find($schedule->event_id);

             $training_session = TrainingSession::find($cal_training_session->training_session_id);

             $volunteer = User::find($schedule->user_id);

             $setting = SiteSetting::find(1);

             if ($request->master_admin_check) {
                 $admins = User::where('user_role', 3)->get();
                 foreach ($admins as $admin) {
                     Mail::to($admin->email)->send(new TrainingSessionScheduleDeleteUser($volunteer, $setting, $training_session, $cal_training_session));
                 }
             }

             if ($request->staff_admin_check) {
                 $staffs = User::where('user_role', 2)->get();
                 if ($staffs != null) {
                     foreach ($staffs as $staff) {
                         Mail::to($staff->email)->send(new TrainingSessionScheduleDeleteUser($volunteer, $setting, $training_session, $cal_training_session));
                     }
                 }
             }

             if ($request->volunteer_check) {
                 Mail::to($volunteer->email)->send(new TrainingSessionScheduleDeleteUser($volunteer, $setting, $training_session, $cal_training_session));
             }

             $schedule->delete();

             return Response()->json([
                 'message'   =>  'success delete.'
             ]);
         }

         return "error";

     }

     public function single_schedule_destroy($id) {
         $schedule = ScheduleTrainingSession::find($id);

         if ($schedule->id) {

             $cal_training_session = TrainingSessionCalendar::find($schedule->event_id);
             $training_session = TrainingSession::find($cal_training_session->training_session_id);
             $volunteer = User::find($schedule->user_id);
             $setting = SiteSetting::find(1);

             Mail::to($volunteer->email)->send(new TrainingSessionScheduleDeleteUser($volunteer, $setting, $training_session, $cal_training_session));

             $schedule->delete();

             return Response()->json([
                 'message'   =>  'success delete.'
             ]);
         }

         return "error";
     }
}

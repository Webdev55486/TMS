<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LocationCalendar;
use App\ScheduleLocation;
use DateTime;

class LocationCalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($location_id)
    {
        $datas = LocationCalendar::where('location_id', $location_id)->get();

        $myArray = array();

        foreach ($datas as $data)
        {
            $time1 = date( "g:i a", strtotime($data->start_time));
            $time2 = date( "g:i a", strtotime($data->end_time));
            $break_time1 = date( "g:i a", strtotime($data->break_start_time));
            $break_time2 = date( "g:i a", strtotime($data->break_end_time));
            $myArray[] = array('start' => $data->date, 'end' => $data->date, 'color' => '#ff0000', 'rendering' => 'background', 'title' => $time1." - ".$time2, 'title1' => $break_time1."-".$break_time2);
        }
        return Response()->json($myArray);
    }

    public function event($location_id, $event_date)
    {
        $data = LocationCalendar::where('location_id', $location_id)->where('date', $event_date)->get(['id', 'location_id', 'date', 'start_time', 'end_time'])->first();

        if($data == null)
        {
            return "nodata";
        }
        else {
            $event_id = $data->id;
            $schedule_locations = ScheduleLocation::where('event_id', $event_id)->join('users', 'users.id', '=', 'location_schedules.user_id')
            ->select('location_schedules.*', 'users.first_name', 'users.last_name')
            ->getQuery() // Optional: downgrade to non-eloquent builder so we don't build invalid User objects.
            ->get();


            $myArray = array();

            foreach ($schedule_locations as $schedule_location)
            {
                $user_name = $schedule_location->first_name." ".$schedule_location->last_name;
                $start_time = date( "g:i a", strtotime( $schedule_location->start_time ) );
                $end_time = date( "g:i a", strtotime( $schedule_location->end_time ) );

                $myArray[] = array('schedule_id' => $schedule_location->id, 'start' => $start_time, 'end' => $end_time, 'total_time' => $schedule_location->total_time, 'end_timestamp' => $schedule_location->end_time, 'name' => $user_name, 'ns' => $schedule_location->ns, 'ex' => $schedule_location->ex);
            }

            $default_start_time = date( "g:i a", strtotime( $data->start_time ) );
            $default_end_time = date( "g:i a", strtotime( $data->end_time ) );

            $finalevents = array('id' => $event_id, 'default_start_time' => $default_start_time, 'default_end_time' => $default_end_time, 'schedule_datas' => $myArray);

            return Response()->json($finalevents);
        }
    }

    public function edit_event($location_id, $event_date)
    {
        $data = LocationCalendar::where('location_id', $location_id)->where('date', $event_date)->get()->first();

        if($data == null)
        {
            return "nodata";
        }
        else {
            $start_time = date( "g:i a", strtotime( $data->start_time ) );
            $end_time = date( "g:i a", strtotime( $data->end_time ) );
            $break_start_time = date( "g:i a", strtotime( $data->breadk_start_time ) );
            $break_end_time = date( "g:i a", strtotime( $data->breadk_end_time ) );
            $resultdate = DateTime::createFromFormat('Y-m-d', $data->date);
            $event_date = $resultdate->format('m-d-Y');

            $myArray = array('id' => $data->id, 'date' => $event_date, 'start_time' => $start_time, 'end_time' => $end_time, 'break_start_time' => $break_start_time, 'break_end_time' => $break_end_time, 'total_time' => $data->total_time);
            return Response()->json($myArray);
        }
    }

    public function delete_schedule($schedule_id){
        $schedule_locations = ScheduleLocation::find($schedule_id);
        $schedule_locations->delete();
        return "sucess";
    }

    public function store(Request $request)
    {
        $start_time = date( "H:i:s", strtotime( $request->time_start ) );
        $break_start_time = date( "H:i:s", strtotime( $request->break_time_start ) );
        $end_time = date( "H:i:s", strtotime( $request->time_end ) );
        $break_end_time = date( "H:i:s", strtotime( $request->break_time_end ) );
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->date_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = new LocationCalendar();
        $event->location_id = $request->location_id;
        $event->date = $event_date;
        $event->start_time = $start_time;
        $event->end_time = $end_time;
        $event->break_start_time = $break_start_time;
        $event->break_end_time = $break_end_time;
        $event->total_time = $request->total_time;
        $event->save();

        return redirect('location/edit/'.$event->location_id.'/#calendar')->with('goto_date', $event_date);
    }

    public function update_event(Request $request)
    {
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->_date_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = LocationCalendar::find($request->event_id);
        if ($event->id) {
            $start_time = date( "H:i:s", strtotime( $request->_time_start ) );
            $end_time = date( "H:i:s", strtotime( $request->_time_end ) );
            $break_start_time = date( "H:i:s", strtotime( $request->_break_time_start ) );
            $break_end_time = date( "H:i:s", strtotime( $request->_break_time_end ) );

            $event->start_time = $start_time;
            $event->end_time = $end_time;
            $event->break_start_time = $break_start_time;
            $event->break_end_time = $break_end_time;
            $event->total_time = $request->_total_time;
            $event->save();

            return redirect('location/edit/'.$event->location_id.'/#calendar')->with('goto_date', $event_date);
        }
        return back()->with('goto_date', $event_date);
    }

    public function change_event_date(Request $request)
    {
        $resultdate = DateTime::createFromFormat('m-d-Y', $request->_date_new_select);
        $event_date = $resultdate->format('Y-m-d');

        $event = LocationCalendar::find($request->event_id);
        if ($event->id) {

            $event->date = $event_date;
            $event->save();

            return back()->with('status', 'schedule Date changed successfully');
        }
        return back()->with('error', 'Schedule can not find');
    }

    public function destroy($id)
    {
        $event = LocationCalendar::find($id);

        if($event == null)
        {
            return Response()->json([
                'message'   =>  'error delete.'
            ]);
        }

        $datas = ScheduleLocation::where('event_id', $id)->get();
        foreach ($datas as $data) {
            $this->destroySchedule($data->id);
        }

        $event->delete();

        return Response()->json([
            'message'   =>  'success delete.'
        ]);

    }

    public function destroySchedule($schedule_id) {
        $schedule = ScheduleLocation::find($schedule_id);
        if($schedule != null){
            $schedule->delete();
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LocationCalendar extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'cal_location';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'date', 'start_time', 'end_time', 'breadk_start_time', 'breadk_end_time', 'total_time', 'location_id'
    ];
    public $timestamps = false;
}

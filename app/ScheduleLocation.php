<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ScheduleLocation extends Model
{
    /**
     * [$table description]
     * @var string
     */
    protected $table = 'location_schedules';
    /**
     * [$fillable description]
     * @var [type]
     */
    protected $fillable = [
        'event_id', 'user_id', 'start_time', 'end_time', 'total_time', 'ns', 'ex'
    ];
    public $timestamps = false;
}

@extends('master')
@section('title')
Profile
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('pagelevel_style')
    <link href="{{cdn('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/profile.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- BEGIN PAGE TITLE-->
    <!-- END PAGE TITLE-->
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{route('profile.edit')}}" class="btn green btn-sm"> Edit Profile </a>
            </div>
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="profile custom-profile-class">
        <div class="row">
            <div class="col-md-3">
                <div style="padding-bottom:30px;text-align:-webkit-center;">
                    <h3 class="bold uppercase font-green">
                        @if (Auth::user()->user_role == 3)
                            Master Admin
                        @elseif (Auth::user()->user_role == 2)
                            Staff Admin
                        @elseif (Auth::user()->user_role == 1)
                            Site manager
                        @elseif (Auth::user()->user_role == 0)
                            Volunteer
                        @endif
                    </h3>
                    @if(file_exists('assets/images/avatar/'.'/'.Auth::user()->photo.'_thumbnail.jpg'))
                      <img alt="" style="max-width: 300px;width:100%;" class="img-responsive pic-bordered" src="{{ cdn('assets/images/avatar/').'/'.Auth::user()->photo.'_thumbnail.jpg'}}" />
                    @else
                      <img alt="" style="max-width: 300px;width:100%;" class="img-responsive pic-bordered" src="{{ cdn('assets/images/avatar/nophoto.jpg') }}" />
                    @endif
                </div>
            </div>
            <div class="col-md-9">
                <div class="tabbable-line tabbable-full-width">
                   <ul class="nav nav-tabs">
                       <li class="active" style="width:80px;text-align:center;">
                           <a href="#tab_1_1" data-toggle="tab"> Bio </a>
                       </li>
                       <li>
                           <a href="#tab_1_3" data-toggle="tab"> Work History </a>
                       </li>
                       <li>
                           <a href="#tab_1_6" data-toggle="tab"> Schedule </a>
                       </li>
                   </ul>
                   <div class="tab-content">
                       <div class="tab-pane active" id="tab_1_1">
                           <div class="row">
                               <div class="col-md-12">
                                   <div class="row">
                                       <div class="col-md-9 profile-info">
                                           <h1 class="font-green sbold uppercase">
                                               {{Auth::user()->first_name}} &nbsp; {{Auth::user()->last_name}} &nbsp;&nbsp;
                                               <label style="font-size:12pt;font-weight:bold;color:#a7a7a7;">
                                                   <i class="fa fa-map-marker"></i>
                                                   United States
                                               </label>
                                           </h1>
                                           <h4 class="font-blue bold">
                                               <?php
                                                    $user_tasks = explode(',', Auth::user()->best_serve);
                                                    $tasks = \App\UserTasks::all();
                                                    foreach ($user_tasks as $user_task) {
                                                        foreach ($tasks as $task) {
                                                            if ($task->id == $user_task) {
                                                                echo $task->title.",";
                                                            }
                                                        }
                                                    }
                                               ?>
                                           </h4>
                                           <div class="row">
                                               <div class="col-md-10 col-md-offset-1">
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Cell Phone:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-blue bold profileinfo_p">{{Auth::user()->phone_cell}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Home/Work Phone:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-blue bold profileinfo_p">{{Auth::user()->phone_home}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Address:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{Auth::user()->street}},{{Auth::user()->city}},{{Auth::user()->state}} {{Auth::user()->zip}},United States</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">E-mail:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-blue bold profileinfo_p">{{Auth::user()->email}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Tshirt Size:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">
                                                               @if (Auth::user()->shirt_size == 1)
                                                                   Small
                                                               @elseif (Auth::user()->shirt_size == 2)
                                                                   Medium
                                                               @elseif (Auth::user()->shirt_size == 3)
                                                                   Large
                                                               @elseif (Auth::user()->shirt_size == 4)
                                                                   X-Large
                                                               @elseif (Auth::user()->shirt_size == 5)
                                                                   2X-Large
                                                               @elseif (Auth::user()->shirt_size == 6)
                                                                   3X-Large
                                                               @elseif (Auth::user()->shirt_size == 7)
                                                                   4X-Large
                                                               @elseif (Auth::user()->shirt_size == 8)
                                                                   5X-Large
                                                               @elseif (Auth::user()->shirt_size == 9)
                                                                   6X-Large
                                                               @endif
                                                           </p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Dietary Needs:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{Auth::user()->dietary}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Employer / School:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{Auth::user()->employer}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Length of Service:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{Auth::user()->volunteered_year}} Years</p>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="tab-pane" id="tab_1_3">
                           this is work history field.<br />
                           current null.
                       </div>
                       <div class="tab-pane" id="tab_1_6">
                           <div class="row">
                               <div class="col-md-12">
                                   <p class="bold uppercase" style="margin:0;">Location Schedule</p>
                               </div>
                               <?php
                                    $locations = \App\Location::all();
                                    foreach ($locations as $location) {
                                        $islocation = 0;
                                        foreach ($schedule_days as $schedule_day) {
                                            if ($location->id == $schedule_day->location_id){
                                                $islocation += 1;
                                            }
                                        }

                                        if($islocation >0 ){
                                            ?>
                                            <div class="col-md-12" style="padding-top:10px;padding-left:25px;">
                                                <p class="bold" style="margin:0;color:#0097cb;">{{$location->title}} | {{$location->street}} | {{$location->city_town}}, {{$location->state_province}} {{$location->zip_postal}}</p>
                                            </div>
                                            @foreach ($schedule_days as $schedule_day)
                                                @if ($schedule_day->location_id == $location->id)
                                                    <?php
                                                        $resultdate = DateTime::createFromFormat('Y-m-d', $schedule_day->date);
                                                        $schedule_date = $resultdate->format('m-d-Y');
                                                    ?>
                                                    <div class="col-md-12" style="padding-top:10px;padding-left:25px;" id="user_schedule_{{$schedule_day->id}}">
                                                        <label style="cursor:pointer;" class="bold">{{$schedule_date}} | {{date( "g:i a", strtotime($schedule_day->start_time))}} - {{date( "g:i a", strtotime($schedule_day->end_time))}}</label>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <?php
                                        }
                                    }
                               ?>
                           </div>
                           <div class="row">
                               <div class="col-md-12">
                                   <p class="bold uppercase" style="margin:0;margin-top:10px;">Training Session Schedule</p>
                               </div>
                               <?php
                                    $training_sessions = \App\TrainingSession::all();
                                    foreach ($training_sessions as $training_session) {
                                        $istraining_session = 0;
                                        foreach ($train_schedule_days as $train_schedule_day) {
                                            if ($training_session->id == $train_schedule_day->training_session_id){
                                                $istraining_session += 1;
                                            }
                                        }

                                        if($istraining_session >0 ){
                                            ?>
                                            <div class="col-md-12" style="padding-top:10px;padding-left:25px;">
                                                <p class="bold" style="margin:0;color:#0097cb;">{{$training_session->title}} | {{$training_session->street}} | {{$training_session->city_town}}, {{$training_session->state_province}} {{$training_session->zip_postal}}</p>
                                            </div>
                                            @foreach ($train_schedule_days as $train_schedule_day)
                                                @if ($train_schedule_day->training_session_id == $training_session->id)
                                                    <?php
                                                        $resultdate = DateTime::createFromFormat('Y-m-d', $train_schedule_day->date);
                                                        $schedule_date = $resultdate->format('m-d-Y');
                                                    ?>
                                                    <div class="col-md-12" style="padding-top:10px;padding-left:25px;" id="user_schedule_training_session_{{$train_schedule_day->id}}">
                                                        <label style="cursor:pointer;" class="bold">{{$schedule_date}} | {{date( "g:i a", strtotime($train_schedule_day->start_time))}} - {{date( "g:i a", strtotime($train_schedule_day->end_time))}}</label>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <?php
                                        }
                                    }
                               ?>
                           </div>
                           <div class="row">
                               <div class="col-md-12">
                                   <p class="bold uppercase" style="margin:0;margin-top:10px;">Staff Meeting Schedule</p>
                               </div>
                               <?php
                                    $staff_meetings = \App\StaffMeeting::all();
                                    foreach ($staff_meetings as $staff_meeting) {
                                        $isstaff_meeting = 0;
                                        foreach ($staff_meeting_schedule_days as $staff_meeting_schedule_day) {
                                            if ($staff_meeting->id == $staff_meeting_schedule_day->staff_meeting_id){
                                                $isstaff_meeting += 1;
                                            }
                                        }

                                        if($isstaff_meeting >0 ){
                                            ?>
                                            <div class="col-md-12" style="padding-top:10px;padding-left:25px;">
                                                <p class="bold" style="margin:0;color:#0097cb;">{{$staff_meeting->title}} | {{$staff_meeting->street}} | {{$staff_meeting->city_town}}, {{$staff_meeting->state_province}} {{$staff_meeting->zip_postal}}</p>
                                            </div>
                                            @foreach ($staff_meeting_schedule_days as $staff_meeting_schedule_day)
                                                @if ($staff_meeting_schedule_day->staff_meeting_id == $staff_meeting->id)
                                                    <?php
                                                        $resultdate = DateTime::createFromFormat('Y-m-d', $staff_meeting_schedule_day->date);
                                                        $schedule_date = $resultdate->format('m-d-Y');
                                                    ?>
                                                    <div class="col-md-12" style="padding-top:10px;padding-left:25px;" id="user_schedule_staff_meeting_{{$staff_meeting_schedule_day->id}}">
                                                        <label style="cursor:pointer;" class="bold">{{$schedule_date}} | {{date( "g:i a", strtotime($staff_meeting_schedule_day->start_time))}} - {{date( "g:i a", strtotime($staff_meeting_schedule_day->end_time))}}</label>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <?php
                                        }
                                    }
                               ?>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </div>
    <!-- start add new user -->
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection

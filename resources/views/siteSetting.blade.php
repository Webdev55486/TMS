@extends('master')
@section('title')
Setting
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{ cdn('assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.cs') }}s" rel="stylesheet" type="text/css" />
    <link href="{{ cdn('assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ cdn('assets/global/plugins/bootstrap-summernote/summernote.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('pagelevel_style')
    <link href="{{cdn('css/locationPage.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{{ session('status') }}</span>
        </div>
    @endif
    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-body">
                    <div id="gmap" class="gmaps"> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="alert alert-success logo-success" style="display:none;">
        <button class="close" data-close="alert"></button>
        <span></span>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-cogs font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Company Setting</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="bootstrap_alerts_demo"> </div>
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" data-toggle="tab">Address</a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab">Logo</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <form method="post" class="location-add-form" id="company_address_change_form" action="{{route('change.setting')}}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="inputlocationTitle">Company Name*</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="inputlocationTitle" value="{{$setting->company_name}}" name="company_name" placeholder="Title">
                                            <span class="input-group-addon">
                                                <i class="fa fa-user font-red"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="location-body-form">
                                        <input type="hidden" id="coorlat" name="coorlat" value="{{$setting->coor_lat}}">
                                        <input type="hidden" id="coorlng" name="coorlng" value="{{$setting->coor_lng}}">
                                        <div class="address-title">
                                            <div class="address-caption font-red-sunglo">
                                                <span class="address-caption-subject bold uppercase"> Company Address* </span>
                                            </div>
                                            <div class="actions-button-div">
                                                <div class="btn-group">
                                                    <a class="btn btn-sm green" id="gmap_geocoding_btn">Update Map</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="address-body-div">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputstreet">Street</label>
                                                        <input type="text" id="inputstreet" name="company_street" value="{{$setting->company_street}}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputRoom">Room</label>
                                                        <input type="text" id="inputRoom" name="room" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputcitytown">City / Town</label>
                                                        <input type="text" id="inputcitytown" name="company_city" value="{{$setting->company_city}}" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputstateprovinceregion">State / province / Region</label>
                                                        <input type="text" id="inputstateprovinceregion" value="{{$setting->company_state}}" name="company_state" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="inputzippostcode">Zip / Postal Code</label>
                                                        <input type="text" id="inputzippostcode" name="company_zip" value="{{$setting->company_zip}}" onchange="" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Country</label>
                                                        <select class="form-control" id="country" name="country">
                                                            @foreach($countries as $country)
                                                                <option value="{{$country['code']}}" <?php if($country['code'] == "US"){echo "selected";}  ?> >{{$country['name']}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab_1_2">
                                <form class="logo_change_form" action="{{route('change.logo')}}" id="site_logo_change_form" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}

                                    <div class="fileinput fileinput-new" data-provides="fileinput" style="width:100%;position:relative;text-align:center;">
                                        <div class="fileinput-new thumbnail" style="background-color: #cccccc;">
                                            @if(file_exists('assets/images/company_setting'.'/'.$setting->company_logo))
                                              <img style="width:100%;height:100%;" src="{{cdn('assets/images/company_setting').'/'.$setting->company_logo}}" />
                                            @else
                                              <img src="{{cdn('assets/images/company_setting/default_logo.png')}}" />
                                            @endif
                                        </div>
                                        <div class="fileinput-preview location-page fileinput-exists thumbnail" style="max-width: 100%;max-height: 100%;background-color: #cccccc;"> </div>
                                        <div class="">
                                            <span class="btn default btn-file">
                                                <span class="fileinput-new"> Select image </span>
                                                <span class="fileinput-exists"> Change </span>
                                                <input type="file" name="company_logo" id="company_logo" required>
                                            </span>
                                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-cogs font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Register page</span>
                    </div>
                    <div class="actions-button-div">
                        <div class="btn-group">
                            <a class="btn btn-sm green" href="{{route('preview.register')}}" target="_blank">Preview</a>
                        </div>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="bootstrap_alerts_demo_1"> </div>
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_2_2" data-toggle="tab">Page Setting</a>
                            </li>
                            <li>
                                <a href="#tab_2_1" data-toggle="tab">Theme Change</a>
                            </li>
                            <li>
                                <a href="#tab_2_3" data-toggle="tab">End page Text of Register</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_2_1">
                                <form class="logo_change_form" action="{{route('change.register_form')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group">
                                                {{ Form::label('_color', 'Background Color') }}
                                                <div class="input-group select-color">
                                                    <input class="form-control" type="text"  name="background_color" value="{{$setting->register_background}}">
                                                    <span class="input-group-addon">
                                                        <i style="background-color:{{$setting->register_background}}; height:20px; width:30px; border: 1px solid #ccc;"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group">
                                                {{ Form::label('_color', 'Register Form Color') }}
                                                <div class="input-group select-color">
                                                    <input class="form-control" type="text"  name="register_form_color" value="{{$setting->register_form}}">
                                                    <span class="input-group-addon">
                                                        <i style="background-color:{{$setting->register_form}}; height:20px; width:30px; border: 1px solid #ccc;"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-3">
                                            <div class="form-group">
                                                {{ Form::label('_color', 'CopyRight Text Color') }}
                                                <div class="input-group select-color">
                                                    <input class="form-control" type="text"  name="copyright_color" value="{{$setting->copyright_color}}">
                                                    <span class="input-group-addon">
                                                        <i style="background-color:{{$setting->copyright_color}}; height:20px; width:30px; border: 1px solid #ccc;"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane active" id="tab_2_2">
                                <div class="form-group" style="padding-top:20px;">
                                    <span class="uppercase" style="font-size:10pt;color:#333;">
                                        <div class="row">
                                            <div class="col-md-6 setting_register_page_text">
                                                <span>Register page:</span>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="checkbox" @if($setting->accept_volunteer == 1) checked @endif class="make-switch" data-on-text="Enabled" data-off-text="Disabled" data-size="small" name="accept_volunteer" value="1">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="form-group" style="padding-top:20px;">
                                    <span class="uppercase" style="font-size:10pt;color:#333;">
                                        <div class="row">
                                            <div class="col-md-6 setting_register_page_text">
                                                <span>Register page logo:</span>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="checkbox" @if($setting->enable_logo == 1) checked @endif class="make-switch" data-on-text="Enabled" data-off-text="Disabled" data-size="small" name="enable_logo" value="1">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="form-group" style="padding-top:20px;">
                                    <span class="uppercase" style="font-size:10pt;color:#333;">
                                        <div class="row">
                                            <div class="col-md-6 setting_register_page_text">
                                                <span>T-Shirts for First Years Only:</span>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="checkbox" @if($setting->shirt_check == 1) checked @endif class="make-switch" data-on-text="Enabled" data-off-text="Disabled" data-size="small" name="shirt_check" value="1">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                                <div class="form-group" style="padding-top:20px;">
                                    <span class="uppercase" style="font-size:10pt;color:#333;">
                                        <div class="row">
                                            <div class="col-md-6 setting_register_page_text">
                                                <span>Register page background image:</span>
                                            </div>
                                            <div class="col-md-6">
                                                <input type="checkbox" @if($setting->register_backstretch == 1) checked @endif class="make-switch" data-on-text="Enabled" data-off-text="Disabled" data-size="small" name="enable_backstretch" value="1">
                                            </div>
                                        </div>
                                    </span>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_2_3">
                                <form class="logo_change_form" action="{{route('change.thankyou_text')}}" id="thankyou_text_form" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <div name="thankyou_text" id="summernote_1"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-cogs font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Mail Text Setting</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="bootstrap_alerts_demo_2"> </div>
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_3_1" data-toggle="tab">Register Mail</a>
                            </li>
                            <li>
                                <a href="#tab_3_2" data-toggle="tab">Mail Detail</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_3_1">
                                <form class="register_email_text_form" id="register_email_text_form" action="{{route('change.register_email_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="registeremail_subject" name="registeremail_subject" class="form-control" value="{{$setting->register_email_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="registeremail_contents" id="registeremail_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab_3_2">
                                <form class="logo_change_form" action="{{route('change.env.maildetail')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="form-group">
                                                <label for="inputRoom">App Name</label>
                                                <input type="text" id="app_name" name="app_name" class="form-control" value="{{config('app.name')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="form-group">
                                                <label for="inputRoom">Mail From Address</label>
                                                <input type="text" id="mail_address" name="mail_address" class="form-control" value="{{config('mail.from.address')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="form-group">
                                                <label for="inputRoom">Mail From Name</label>
                                                <input type="text" id="mail_name" name="mail_name" class="form-control" value="{{config('mail.from.name')}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-cogs font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Location Schedule</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="bootstrap_alerts_demo_3"> </div>
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_4_1" data-toggle="tab">Send Schedule</a>
                            </li>
                            <li>
                                <a href="#tab_4_2" data-toggle="tab">Remove Volunteer</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_4_1">
                                <form class="location_schedule_email_text_form" id="location_schedule_email_text_form" action="{{route('change.location_schedule_email_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="location_schedule_email_subject" name="location_schedule_email_subject" class="form-control" value="{{$setting->location_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="location_schedule_email_contents" id="location_schedule_email_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab_4_2">
                                <form class="location_schedule_delete_user_text_form" id="location_schedule_delete_user_text_form" action="{{route('change.location_schedule_delete_user_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="schedule_delete_user_email_subject" name="schedule_delete_user_email_subject" class="form-control" value="{{$setting->location_schedule_delete_user_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="schedule_delete_user_email_contents" id="schedule_delete_user_email_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-cogs font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Training Session Schedule</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="bootstrap_alerts_demo_4"> </div>
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_5_1" data-toggle="tab">Send Schedule</a>
                            </li>
                            <li>
                                <a href="#tab_5_2" data-toggle="tab">Remove Volunteer</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_5_1">
                                <form class="train_schedule_email_text_form" id="train_schedule_email_text_form" action="{{route('change.change_train_schedule_email_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="train_schedule_email_subject" name="train_schedule_email_subject" class="form-control" value="{{$setting->train_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="train_schedule_email_contents" id="train_schedule_email_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab_5_2">
                                <form class="train_schedule_delete_user_text_form" id="train_schedule_delete_user_text_form" action="{{route('change.train_schedule_delete_user_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="train_schedule_delete_user_email_subject" name="train_schedule_delete_user_email_subject" class="form-control" value="{{$setting->train_schedule_delete_user_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="train_schedule_delete_user_email_contents" id="train_schedule_delete_user_email_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 ">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-cogs font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase">Staff Meeting Schedule</span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div id="bootstrap_alerts_demo_5"> </div>
                    <div class="tabbable-line tabbable-full-width">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_6_1" data-toggle="tab">Send Schedule</a>
                            </li>
                            <li>
                                <a href="#tab_6_2" data-toggle="tab">Delete User</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_6_1">
                                <form class="staff_schedule_email_text_form" id="staff_schedule_email_text_form" action="{{route('change.change_staff_schedule_email_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="staff_schedule_email_subject" name="staff_schedule_email_subject" class="form-control" value="{{$setting->staff_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="staff_schedule_email_contents" id="staff_schedule_email_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="tab_6_2">
                                <form class="staff_schedule_delete_user_text_form" id="staff_schedule_delete_user_text_form" action="{{route('change.staff_schedule_delete_user_text')}}" method="post" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <div class="form-group">
                                                <label for="inputRoom">Email Subject</label>
                                                <input type="text" id="staff_schedule_delete_user_email_subject" name="staff_schedule_delete_user_email_subject" class="form-control" value="{{$setting->staff_schedule_delete_user_subject}}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <label for="inputRoom">Email Contents</label>
                                            <div class="form-group">
                                                <div name="staff_schedule_delete_user_email_contents" id="staff_schedule_delete_user_email_contents"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group text-center" style="padding-top:20px;">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-sm green">Save Change</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    {{-- <script src="{{cdn('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script> --}}
    <script src="{{cdn('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js')}}" type="text/javascript"></script>
    <script src="{{ cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
    <script src="{{ cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
    <script src="{{ cdn('assets/global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-toastr/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{ cdn('assets/global/plugins/bootstrap-summernote/summernote.min.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script src="{{cdn('assets/pages/scripts/components-bootstrap-switch.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/location.js')}}" type="text/javascript"></script>
    <script src="{{cdn('js/setting.js')}}" type="text/javascript"></script>
    <script>
        $('#summernote_1').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var thankyou_text = '<?php echo html_entity_decode($setting->thankyou_text); ?>';
        $('#summernote_1').parent().find('div.panel-body').html(thankyou_text);

        $('#registeremail_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var register_email_contents = '<?php echo html_entity_decode($setting->register_email_contents); ?>';
        $('#registeremail_contents').parent().find('div.panel-body').html(register_email_contents);

        $('#location_schedule_email_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var location_schedule_email_contents = '<?php echo html_entity_decode($setting->location_contents); ?>';
        $('#location_schedule_email_contents').parent().find('div.panel-body').html(location_schedule_email_contents);

        $('#schedule_delete_user_email_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var schedule_delete_user_email_contents = '<?php echo html_entity_decode($setting->location_schedule_delete_user_contents); ?>';
        $('#schedule_delete_user_email_contents').parent().find('div.panel-body').html(schedule_delete_user_email_contents);

        $('#train_schedule_email_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var train_schedule_email_contents = '<?php echo html_entity_decode($setting->train_contents); ?>';
        $('#train_schedule_email_contents').parent().find('div.panel-body').html(train_schedule_email_contents);

        $('#train_schedule_delete_user_email_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var train_schedule_delete_user_email_contents = '<?php echo html_entity_decode($setting->train_schedule_delete_user_contents); ?>';
        $('#train_schedule_delete_user_email_contents').parent().find('div.panel-body').html(train_schedule_delete_user_email_contents);

        $('#staff_schedule_email_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var staff_schedule_email_contents = '<?php echo html_entity_decode($setting->staff_contents); ?>';
        $('#staff_schedule_email_contents').parent().find('div.panel-body').html(staff_schedule_email_contents);

        $('#staff_schedule_delete_user_email_contents').summernote({
            height: 200,
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link']],
                ['view', ['fullscreen']]
              ]
        });
        var staff_schedule_delete_user_email_contents = '<?php echo html_entity_decode($setting->staff_schedule_delete_user_contents); ?>';
        $('#staff_schedule_delete_user_email_contents').parent().find('div.panel-body').html(staff_schedule_delete_user_email_contents);

        $('.select-color').colorpicker();
        var str_coordinatelat = <?php echo $setting->coor_lat; ?>;
        var str_coordinatelng = <?php echo $setting->coor_lng; ?>;
        var coordinatelat = parseFloat(str_coordinatelat);
        var coordinatelng = parseFloat(str_coordinatelng);

        var map;
        var marker;
        function initMap() {
           var mapOptions = {
               center: new google.maps.LatLng(coordinatelat,coordinatelng),
               zoom: 17,
               mapTypeId: google.maps.MapTypeId.ROADMAP
           };
           map = new google.maps.Map(document.getElementById('gmap'),mapOptions);

           var myLatLng = {lat: coordinatelat, lng: coordinatelng};

           marker = new google.maps.Marker({
             map: map,
             position: myLatLng
           });
        }

        function resetmap () {
            var street = document.getElementById('inputstreet').value;
            var city = document.getElementById('inputcitytown').value;
            var State = document.getElementById('inputstateprovinceregion').value;
            var postal = document.getElementById('inputzippostcode').value;
            var countryId = document.getElementById('country');
            var countryName = countryId.options[countryId.selectedIndex].text;

            var addressInput = "";

            if (street != "")
            {
                addressInput = addressInput + street + ",";
            }

            if (city != "")
            {
                addressInput = addressInput + city + ",";
            }

            if (State != "")
            {
                addressInput = addressInput + State + postal + ",";
            }

            if (countryName != "")
            {
                addressInput = addressInput + countryName;
            }

            console.log(addressInput);

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({address: addressInput}, function(results, status) {

                if (status == google.maps.GeocoderStatus.OK) {

                var myResult = results[0].geometry.location;

                console.log(myResult.lat());
                console.log(myResult.lng());

                $('#coorlat').val(myResult.lat());
                $('#coorlng').val(myResult.lng());

                createMarker(myResult);

                map.setCenter(myResult);

                map.setZoom(17);
                }
            });
        }

        function createMarker(latlng) {

          if(marker != undefined && marker != ''){
            marker.setMap(null);
            marker = '';
          }

          marker = new google.maps.Marker({
            map: map,
            position: latlng
          });
        }

        $('#gmap_geocoding_btn').click(function() {
            resetmap();
        });

        $('#inputstreet, #inputcitytown, #inputstateprovinceregion, #country').on('change',function(e) {
            resetmap();
        });

        $('input[name="shirt_check"]').on('switchChange.bootstrapSwitch', function(event, state) {
          reset_tshirt_status(state);
        });

        $('input[name="accept_volunteer"]').on('switchChange.bootstrapSwitch', function(event, state) {
          reset_accept_newUser(state);
        });

        $('input[name="enable_logo"]').on('switchChange.bootstrapSwitch', function(event, state) {
          reset_logo(state);
        });

        $('input[name="enable_backstretch"]').on('switchChange.bootstrapSwitch', function(event, state) {
          reset_backstretch(state);
        });

        function reset_tshirt_status(state){
            var status = "";
            if(state) {
                status = 1;
            }
            else {
                status = 0;
            }
            var url = "{{ url('/change_tshirt') }}";
            var final_url = url+"/"+status;

            $.ajax({
                url: final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }

        function reset_accept_newUser(state){
            var status = "";
            if(state) {
                status = 1;
            }
            else {
                status = 0;
            }
            var url = "{{ url('/change_newuser') }}";
            var final_url = url+"/"+status;

            $.ajax({
                url: final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }

        function reset_logo(state){
            var status = "";
            if(state) {
                status = 1;
            }
            else {
                status = 0;
            }
            var url = "{{ url('/reset_logo') }}";
            var final_url = url+"/"+status;

            $.ajax({
                url: final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }

        function reset_backstretch(state){
            var status = "";
            if(state) {
                status = 1;
            }
            else {
                status = 0;
            }
            var url = "{{ url('/reset_backstretch') }}";
            var final_url = url+"/"+status;

            $.ajax({
                url: final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtCsu8LKseS4s2ahkBvYcL_yKKmKWX82g&callback=initMap"></script>
@endsection

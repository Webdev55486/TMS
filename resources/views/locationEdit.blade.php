@extends('master')
@section('title')
Edit location
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/locationPage.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <input type="hidden" name="" id="goto_date_val" value="@if(session('goto_date')){{session('goto_date')}}@endif">
    <form method="post" class="location-update-form" action="{{route('location.update')}}" enctype="multipart/form-data">
        <input type="hidden" name="location_id_edit" id="location_id_edit" value="{{$location->id}}">
        <div class="page-bar">
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn green btn-sm"> Update Location </button>
                </div>
            </div>
        </div>
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @if (session('status'))
            <div class="alert alert-success">
                <button class="close" data-close="alert"></button>
                <span>{{ session('status') }}</span>
            </div>
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 ">
                <div class="portlet location-image-form light bordered">
                    <div class="fileinput fileinput-new" data-provides="fileinput" style="width:100%;position:relative;text-align:center;">
                        <div class="fileinput-new thumbnail" style="height:298px;width:100%;">
                            @if(file_exists('assets/images/location'.'/'.$location->img_name.'_thumbnail.jpg'))
                              <img style="width:100%;height:100%;" src="{{cdn('assets/images/location').'/'.$location->img_name.'.jpg'}}" />
                            @else
                              <img style="width:100%;height:100%;" src="{{cdn('assets/images/location/not_found.jpg')}}" />
                            @endif
                        </div>
                        <div class="image-select-button">
                            <span class="btn default btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists"> Change </span>
                                <input type="file" name="location_image">
                            </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                        <div class="fileinput-preview location-page fileinput-exists thumbnail" style="max-width: 100%;width:100%; max-height: 100%;height:298px;"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div id="gmap" class="gmaps"> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo">
                            <i class="fa fa-map-marker font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Location </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-group">
                            <label style="cursor:pointer;" class="bold"><input type="checkbox" class="icheck" @if($location->isSaturday == 1) checked @endif id="saturdaycheck" data-checkbox="icheckbox_minimal-red" name="follow_saturday">Check If This Location is Open Saturdays Only.</label>
                        </div>
                        <div class="form-group">
                            <label for="inputlocationTitle">Title*</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputlocationTitle" name="location_title" value="{{$location->title}}" placeholder="Title">
                                <span class="input-group-addon">
                                    <i class="fa fa-user font-red"></i>
                                </span>
                            </div>
                        </div>
                        <div class="location-body-form">
                            <input type="hidden" id="coorlat" name="coorlat" value="{{$location->coor_lat}}">
                            <input type="hidden" id="coorlng" name="coorlng" value="{{$location->coor_lng}}">
                            <div class="address-title">
                                <div class="address-caption font-red-sunglo">
                                    <span class="address-caption-subject bold uppercase"> Address* </span>
                                </div>
                                <div class="actions-button-div">
                                    <div class="btn-group">
                                        <a class="btn btn-sm green" id="gmap_geocoding_btn">Update Map</a>
                                    </div>
                                </div>
                            </div>
                            <div class="address-body-div">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputstreet">Street Address</label>
                                            <input type="text" id="inputstreet" name="street_address" value="{{$location->street}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputRoom">Room</label>
                                            <input type="text" id="inputRoom" name="room" value="{{$location->room}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputcitytown">City / Town</label>
                                            <input type="text" id="inputcitytown" name="town_address" value="{{$location->city_town}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputstateprovinceregion">State / province / Region</label>
                                            <input type="text" id="inputstateprovinceregion" name="province" value="{{$location->state_province}}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputzippostcode">Zip / Postal Code</label>
                                            <input type="text" id="inputzippostcode" name="postal" value="{{$location->zip_postal}}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control" id="country" name="country">
                                                @foreach($countries as $country)
                                                    <option <?php if($location->country_code == $country['code']) echo 'selected' ?> value="{{$country['code']}}" >{{$country['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="row">
        <div class="col-md-12 ">
            <div class="btn-group pull-right" style="margin-bottom:20px;">
                <a href="{{route('location')}}" class="btn green btn-sm"> Back to List </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            {{ Form::open(['route' => 'location.event.store', 'class' => 'eventAdd-form', 'method' => 'post', 'role' => 'form']) }}
                <input type="hidden" name="location_id" value="{{$location->id}}" />
                <div id="responsive-modal" class="modal fade" tabindex="-1" data-backdrop="static">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4>Add New Schedule</h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-group">
                                {{ Form::label('date_select', 'Date') }}
                                {{ Form::text('date_select', old('date_select'), ['class' => 'form-control', 'readonly' => 'true']) }}
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        {{ Form::label('time_start', 'Start Time') }}
                                        {{ Form::text('time_start', old('time_start'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A', 'required']) }}
                                    </div>

                                    <div class="form-group">
                                        {{ Form::label('time_end', 'End Time') }}
                                        {{ Form::text('time_end', old('time_end'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A', 'required']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="break-time-container">
                                        <div class="form-group">
                                            {{ Form::label('break_time_start', 'Break Start Time') }}
                                            {{ Form::text('break_time_start', old('break_time_start'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A' ]) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('break_time_end', 'Break End Time') }}
                                            {{ Form::text('break_time_end', old('break_time_end'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                {{ Form::label('total_time', 'Total hours worked') }}
                                {{ Form::text('total_time', old('total_time'), ['class' => 'form-control', 'required']) }}
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dafault" data-dismiss="modal">Cancel</button>
                            {!! Form::submit('Add', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
            <div class="portlet light bordered">
                <div id='calendar'></div>
            </div>
            {{ Form::open(['route' => 'location.event.update', 'class' => 'eventEdit-form', 'method' => 'post']) }}
                <input type="hidden" id="event_id" name="event_id" value="" />
                <div id="modal-event" class="modal fade" tabindex="-1" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4>Schedule Edit or Delete</h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    {{ Form::label('_date_select', 'Date') }}
                                    {{ Form::text('_date_select', old('_date_select'), ['class' => 'form-control', 'readonly' => 'true']) }}
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            {{ Form::label('_time_start', 'Start Time') }}
                                            {{ Form::text('_time_start', old('_time_start'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A', 'required']) }}
                                        </div>

                                        <div class="form-group">
                                            {{ Form::label('_time_end', 'End Time') }}
                                            {{ Form::text('_time_end', old('_time_end'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A', 'required']) }}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="break-time-container">
                                            <div class="form-group">
                                                {{ Form::label('_break_time_start', 'Break Start Time') }}
                                                {{ Form::text('_break_time_start', old('_break_time_start'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A']) }}
                                            </div>

                                            <div class="form-group">
                                                {{ Form::label('_break_time_end', 'Break End Time') }}
                                                {{ Form::text('_break_time_end', old('_break_time_end'), ['class' => 'form-control clockface_1', 'data-format' => 'h:mm A']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    {{ Form::label('_total_time', 'Total hours worked') }}
                                    {{ Form::text('_total_time', old('_total_time'), ['class' => 'form-control', 'required']) }}
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a id="delete" data-href="{{ url('locationCalendar/delete') }}" data-id="" class="btn btn-danger">Delete</a>
                                <button type="button" class="btn btn-dafault" data-dismiss="modal">Close</button>
                                {!! Form::submit('Edit', ['class' => 'btn btn-success']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/pages/scripts/components-date-time-pickers.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/location.js')}}" type="text/javascript"></script>
    <script>
        var locationId = <?php echo $location->id ;?>;

        var BASEURL = "{{ url('/locationCalendar') }}";

        $(document).ready(function() {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth()+1; //January is 0!
            var yyyy = today.getFullYear();

            if(dd<10) {
                dd = '0'+dd
            }

            if(mm<10) {
                mm = '0'+mm
            }

            today = yyyy + '-' + mm + '-' + dd;

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                selectable: false,
                selectHelper: true,

                events: BASEURL + '/events/' + locationId,

                eventRender: function(event, element, view) {
                    /* Only muck about if it is a background rendered event */
                    if (event.rendering == 'background') {
                        var bgEventTitle = document.createElement('div');
                        bgEventTitle.style.position = 'absolute';
                        bgEventTitle.style.bottom = '0';
                        bgEventTitle.classList.add('fc-event');
                        bgEventTitle.classList.add('custom-title-div');
                        bgEventTitle.innerHTML = '<h3 class="fc-title custom-h3">' + event.title + '</h3><h3 class="fc-title custom-h3"> (break ' + event.title1 + ')</h3>' ;
                        element.css('position', 'relative').html(bgEventTitle);
                    }
                },

                dayClick: function(date, jsEvent, view) {
                    var eventDate =  date.format();
                    var currentDay = today;
                    var getData = BASEURL +'/event_edit/'+locationId+'/'+eventDate;
                    $.ajax({
                        url: getData,
                        type: 'get',
                        success: function(result){
                            if (result != "nodata" || jsEvent.target.classList.contains('fc-bgevent')) {
                                $('#modal-event #delete').attr('data-id', result['id']);
                                $('#event_id').val(result['id']);
                                $('#modal-event #_date_select').val(result['date']);
                                $('#modal-event #_time_start').val(result['start_time']);
                                $('#modal-event #_time_end').val(result['end_time']);
                                $('#modal-event #_break_time_start').val(result['break_start_time']);
                                $('#modal-event #_break_time_end').val(result['break_end_time']);
                                $('#modal-event #_total_time').val(result['total_time']);
                                $('#modal-event').modal('show');
                            }
                            else{
                                if (eventDate > currentDay)
                                {
                                    $('#date_select').val(date.format('MM-DD-YYYY'));
                                    $('#responsive-modal').modal('show');
                                }
                            }
                        },
                        error: function(result){
                            console.log(result);
                        }
                    });
                },
            });

            var goto_date = today;
            if ($('#goto_date_val').val() != "") {
                goto_date = $('#goto_date_val').val();
            }
            $('#calendar').fullCalendar('gotoDate', new Date(goto_date));
        });

        $('#delete').on('click', function(){
            var x = $(this);
            var delete_url = x.attr('data-href')+'/'+x.attr('data-id');

            $.ajax({
                url: delete_url,
                type: 'get',
                success: function(result){
                    $('#modal-event').modal('hide');
                    // alert(result.message);
                    $('#calendar').fullCalendar( 'refetchEvents' );
                },
                error: function(result){
                    $('#modal-event').modal('hide');
                    alert(result.message);
                }
            });
        });

    </script>

    <script>
        var str_coordinatelat = <?php echo $location->coor_lat; ?>;
        var str_coordinatelng = <?php echo $location->coor_lng; ?>;
        var coordinatelat = parseFloat(str_coordinatelat);
        var coordinatelng = parseFloat(str_coordinatelng);

        var map;
        var marker;
        function initMap() {
            var mapOptions = {
        		center: new google.maps.LatLng(coordinatelat,coordinatelng),
        		zoom: 17,
        		mapTypeId: google.maps.MapTypeId.ROADMAP
        	};
            map = new google.maps.Map(document.getElementById('gmap'),mapOptions);

            var myLatLng = {lat: coordinatelat, lng: coordinatelng};

            marker = new google.maps.Marker({
              map: map,
              position: myLatLng
            });
        }

        function resetmap () {
            var street = document.getElementById('inputstreet').value;
            var city = document.getElementById('inputcitytown').value;
            var State = document.getElementById('inputstateprovinceregion').value;
            var postal = document.getElementById('inputzippostcode').value;
            var countryId = document.getElementById('country');
            var countryName = countryId.options[countryId.selectedIndex].text;

            var addressInput = "";

            if (street != "")
            {
                addressInput = addressInput + street + ",";
            }

            if (city != "")
            {
                addressInput = addressInput + city + ",";
            }

            if (State != "")
            {
                addressInput = addressInput + State + postal + ",";
            }

            if (countryName != "")
            {
                addressInput = addressInput + countryName;
            }

        	var geocoder = new google.maps.Geocoder();

        	geocoder.geocode({address: addressInput}, function(results, status) {

        		if (status == google.maps.GeocoderStatus.OK) {

                var myResult = results[0].geometry.location;

                console.log(myResult.lat());
                console.log(myResult.lng());

                $('#coorlat').val(myResult.lat());
                $('#coorlng').val(myResult.lng());

                createMarker(myResult);

                map.setCenter(myResult);

                map.setZoom(17);
                }
            });
        }

        function createMarker(latlng) {

          if(marker != undefined && marker != ''){
            marker.setMap(null);
            marker = '';
          }

          marker = new google.maps.Marker({
            map: map,
            position: latlng
          });
        }

        $('#gmap_geocoding_btn').click(function() {
            resetmap();
        });

        $('#inputstreet, #inputcitytown, #inputstateprovinceregion, #country').on('change',function(e) {
            resetmap();
        });

        $('#saturdaycheck').on('ifChecked', function(event){
            reset_locationDay(1);
        });

        $('#saturdaycheck').on('ifUnchecked', function(event){
            // console.log("uncheck");
            reset_locationDay(0);
        });

        function reset_locationDay(status){
            var url = "{{ url('/location/changeSat') }}";
            var sat_location_id = $('#location_id_edit').val();
            var final_url = url+"/"+sat_location_id+"/"+status;

            $.ajax({
                url: final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(result);
                }
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtCsu8LKseS4s2ahkBvYcL_yKKmKWX82g&callback=initMap"></script>
@endsection

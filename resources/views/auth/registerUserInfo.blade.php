@extends('auth.auth_master_register')
@section('title')
Register Info
@endsection
@section('body_type')
class="login"
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="registerUserInfo-form" action="{{ url('registerInfo') }}" method="post">
        <h3 class="form-title text-center bold uppercase">Register User Info</h3>
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{$user->id}}">
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Street</label>
            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Street" name="street" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">City/Town</label>
            <div class="input-icon">
                <i class="fa fa-location-arrow"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="City/Town" name="city" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">State</label>
            <div class="input-icon">
                <i class="fa fa-location-arrow"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="State" name="state" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Zip/Post Code</label>
            <div class="input-icon">
                <i class="fa fa-check"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Zip/Post Code" name="zip" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Country</label>
            <select name="country" id="country_list" class="select2 form-control" disabled>
                <option value="US" selected>United States</option>
            </select>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Employer/School</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Employer/School" name="employer" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Cell Phone</label>
            <div class="input-icon">
                <i class="fa fa-phone"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Cell Phone" id="phone_cell" name="phone_cell" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Home/Work Phone</label>
            <div class="input-icon">
                <i class="fa fa-phone"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Home/Work Phone" id="phone_home" name="phone_home" /> </div>
        </div>
        <div class="form-actions text-center">
            {{-- <button id="register-back-btn" type="button" class="btn red btn-outline"> Back </button> --}}
            <button type="submit" id="register-submit-btn" class="btn green"> Save and Next </button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection
@section('pagelevel_script')
    <script type="text/javascript">
        $("#phone_cell").mask("(000) 000-0000");
        $("#phone_home").mask("(000) 000-0000");
    </script>
@endsection

@extends('auth.auth_master')
@section('title')
login
@endsection
@section('body_type')
class="login"
@endsection
@section('content')
    <!-- BEGIN LOGIN FORM -->
    <form class="login-form" action="{{ route('login')}}" method="post">
        <h3 class="form-title text-center">Login to your account</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter email and password. </span>
        </div>
        @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="input-icon">
                <i class="fa fa-user"></i>
                <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
            </div>
        </div>
        <div class="form-actions">
            <label class="checkbox" style="cursor:pointer;">
                <input type="checkbox" name="remember" value="1" /> Remember me </label>
            <button type="submit" class="btn green pull-right"> Login </button>
        </div>
        <div class="forget-password">
            <h4>Forgot your password ?</h4>
            <p> No worries, click
                <a> here </a> to reset your password. </p>
        </div>
    </form>
    <!-- END LOGIN FORM -->
@endsection

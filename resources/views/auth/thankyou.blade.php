@extends('auth.auth_master_register')
@section('title')
Thank thankyou_text
@endsection
@section('body_type')
class="login"
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="notneed-form">
        <h3 class="form-title text-center bold"><?php echo html_entity_decode($setting->thankyou_text); ?></h3>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection

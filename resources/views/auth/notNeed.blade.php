@extends('auth.auth_master_register')
@section('title')
Register Disabled
@endsection
@section('body_type')
class="login"
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="notneed-form">
        <h3 class="form-title text-center bold">Thank you for your interest! At this time we are in-between Tax Seasons.
            We typically open this form near the end of November; please come back to see us then!</h3>
        <div class="form-actions text-center">
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection

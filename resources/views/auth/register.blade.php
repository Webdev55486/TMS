@extends('auth.auth_master_register')
@section('title')
Register Basic Info
@endsection
@section('body_type')
class="login"
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="register-form" action="{{ route('register')}}" method="post">
        <h3 class="form-title text-center bold">Sign Up</h3>
        @if (session('error'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">First Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="first_name" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Last Name</label>
            <div class="input-icon">
                <i class="fa fa-font"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="last_name" />
            </div>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" id="register_email" placeholder="Email" name="email" /> </div>
        </div>
        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Re-type Your Email</label>
            <div class="input-icon">
                <i class="fa fa-envelope"></i>
                <input class="form-control placeholder-no-fix" type="text" placeholder="Re-type Your Email" name="email_confirmation" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="input-icon">
                <i class="fa fa-lock"></i>
                <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> </div>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
            <div class="controls">
                <div class="input-icon">
                    <i class="fa fa-check"></i>
                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Your Password" name="password_confirmation" /> </div>
            </div>
        </div>
        <div class="form-actions text-center">
            <button type="submit" id="register-submit-btn" class="btn green"> Sign Up </button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection

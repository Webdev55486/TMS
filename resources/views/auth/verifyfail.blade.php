@extends('auth.auth_master_register')
@section('title')
Email Not Verified
@endsection
@section('body_type')
class="login"
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="notneed-form">
        <h3 class="form-title text-center bold">{{$text}}</h3>
        <div class="form-actions text-center">
            <a href="{{ route('login')}}" id="register-submit-btn" class="btn green"> Back To Login </a>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection

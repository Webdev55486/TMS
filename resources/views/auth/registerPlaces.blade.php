@extends('auth.auth_master_register')
@section('title')
Register Schedule
@endsection
@section('body_type')
class="place"
@endsection
@section('content')
    <!-- BEGIN REGISTRATION FORM -->
    <form class="registerlocations-form" action="{{url('registerschedule')}}" method="post">
        <h3 class="form-title text-center bold uppercase">Register Schedule</h3>
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{$user->id}}">
        <input type="hidden" name="ischeck_shirt_size" id="ischeck_shirt_size" value="{{$setting->shirt_check}}">
        <div class="row">
            <div class="col-md-8">
                <p class="bold">Number of years you have volunteered in VITA (include this year)</p>
            </div>
            <div class="col-md-4">
              <div class="form-group">
                  <label class="control-label visible-ie8 visible-ie9">Volunteered Year</label>
                  <select name="volunteered_year" id="volunteered_year_list" class="select2 form-control">
                      <option value="1" >This is My First Year!</option>
                      @for ($i=2; $i < 41; $i++)
                          <option value="{{$i}}">{{$i}}</option>
                      @endfor
                  </select>
              </div>
            </div>
        </div>
        <div class="row" id="select_shirt_size_row">
            <div class="col-md-8">
                <p class="bold">What shirt size whould you prefer? (Note to female Volunteers, Shirt sizes should be given in the context of men's shirt size)</p>
            </div>
            <div class="col-md-4">
              <div class="form-group shirt-size">
                  <label class="control-label visible-ie8 visible-ie9">Shirt Size</label>
                  <select name="shirt_size" id="shirt_size_list" class="select2 form-control">
                    <option value="1" >Small</option>
                    <option value="2" >Medium</option>
                    <option value="3" selected >Large</option>
                    <option value="4" >X-Large</option>
                    <option value="5" >2X-Large</option>
                    <option value="6" >3X-Large</option>
                    <option value="7" >4X-Large</option>
                    <option value="8" >5X-Large</option>
                    <option value="9" >6X-Large</option>
                  </select>
              </div>
            </div>
        </div>
        <hr class="registerplease-hr"/>
        <div class="row">
            <div class="col-md-12">
                <p class="bold">Please Check any and all locations where you would like to volunteer.<br /><br />
                When you Click location, a list of dates will open to allow you to select specific date(s)</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-group accordion" id="locations">
                    <?php
                        $have_event_check_sat = 0;
                        $php_timestamp = time();
                        foreach ($locations as $location) {
                            if($location->isSaturday == 1)
                            {
                                foreach ($events as $event) {

                                    if($event->location_id == $location->id) {
                                        $have_event_check_sat += 1;
                                    }
                                }
                            }
                        }
                    ?>
                    @if ($have_event_check_sat > 0)
                        <p class="bold" style="font-size:13pt;" ><span style="border-bottom: 2px solid #fff;">The following are Saturday Locations...</span></p>
                    @endif
                    @foreach ($locations as $location)
                        <?php
                            $have_event_check1 = 0;
                            foreach ($events as $event) {
                                if($event->location_id == $location->id) {
                                    $have_event_check1 += 1;
                                }
                            }
                        ?>
                        @if($location->isSaturday == 1 && $have_event_check1 > 0 )
                            <div class="panel panel-default registerplase-panel">
                                <div class="panel-heading registerplase-panelheading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle bold" data-toggle="collapse" data-parent="#locations" href="#location{{$location->id}}"> {{$location->title}} | {{$location->street}} | {{$location->city_town}}, {{$location->state_province}} {{$location->zip_postal}}, <span class="room-number location-room-number"> {{$location->room}} </span> </a>
                                    </h4>
                                </div>
                                <div id="location{{$location->id}}" class="panel-collapse collapse">
                                    <div class="panel-body" style="border-top: 0;">
                                        <p class="bold" style="font-size:13pt;color:#fff700;margin-top:0;margin-bottom:10px;"> Check all dates you are available to volunteer at {{$location->title}}</p>
                                        <div class="row">
                                            @foreach ($events as $event)
                                                <?php
                                                    $resultdate = DateTime::createFromFormat('Y-m-d', $event->date);
                                                    $event_date = $resultdate->format('m-d-Y');
                                                    $event_day = str_replace('-', '/', $event_date);
                                                    $current_event_timestamp = strtotime($event_day." ".$event->end_time);
                                                ?>
                                                @if ($event->location_id == $location->id && $php_timestamp < $current_event_timestamp)
                                                    <div class="col-md-12" style="padding-top:10px;">
                                                        <label style="cursor:pointer;" class="bold tms-register-schedule-text"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="user_events[]" value="{{$event->id}}">Sat; {{$event_date}} | {{date( "g:i a", strtotime($event->start_time))}} - {{date( "g:i a", strtotime($event->end_time))}} ( break {{date( "g:i a", strtotime($event->break_start_time))}} - {{date( "g:i a", strtotime($event->break_end_time))}} ) </label>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <?php
                        $have_event_check_week = 0;
                        foreach ($locations as $location) {
                            if($location->isSaturday == 0)
                            {
                                foreach ($events as $event) {
                                    if($event->location_id == $location->id) {
                                        $have_event_check_week += 1;
                                    }
                                }
                            }
                        }
                    ?>
                    @if ($have_event_check_week > 0)
                        <p class="bold" style="font-size:13pt;" ><span style="border-bottom: 2px solid #fff;">The following are Weekday Locations...</span></p>
                    @endif
                    @foreach ($locations as $location)
                        <?php
                            $have_event_check2 = 0;
                            foreach ($events as $event) {
                                if($event->location_id == $location->id) {
                                    $have_event_check2 += 1;
                                }
                            }
                        ?>
                        @if($location->isSaturday == 0 && $have_event_check2 > 0 )
                            <div class="panel panel-default registerplase-panel">
                                <div class="panel-heading registerplase-panelheading">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle bold" data-toggle="collapse" data-parent="#locations" href="#location{{$location->id}}"> {{$location->title}} | {{$location->street}} | {{$location->city_town}}, {{$location->state_province}} {{$location->zip_postal}},  <span class="room-number location-room-number"> {{$location->room}} </span>  </a>
                                    </h4>
                                </div>
                                <div id="location{{$location->id}}" class="panel-collapse collapse">
                                    <div class="panel-body" style="border-top: 0;">
                                        <p class="bold" style="font-size:13pt;color:#fff700;margin-top:0;margin-bottom:10px;"> Check all dates you are available to volunteer at {{$location->title}}</p>
                                        <div class="row">
                                            @foreach ($events as $event)
                                                <?php
                                                    $resultdate = DateTime::createFromFormat('Y-m-d', $event->date);
                                                    $event_date = $resultdate->format('m-d-Y');
                                                    $event_day = str_replace('-', '/', $event_date);
                                                    $current_event_timestamp = strtotime($event_day." ".$event->end_time);
                                                    // echo $current_event_timestamp;
                                                ?>
                                                @if ($event->location_id == $location->id && $php_timestamp < $current_event_timestamp)
                                                    <div class="col-sm-12" style="padding-top:10px;">
                                                        <label style="cursor:pointer;" class="bold tms-register-schedule-text"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="user_events[]" value="{{$event->id}}"> {{$event_date}} | {{date( "g:i a", strtotime($event->start_time))}}-{{date( "g:i a", strtotime($event->end_time))}} ( break {{date( "g:i a", strtotime($event->break_start_time))}} - {{date( "g:i a", strtotime($event->break_end_time))}} ) </label>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="bold">Please Check the task(s) where you would best serve:<br /></p>
                <p>&nbsp;Choose one or more:</p>
                @foreach ($user_tasks as $user_task)
                    <label style="cursor:pointer;margin-left:5px;" class="bold">
                        <input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="user_serve[]" value="{{$user_task->id}}">
                        {{$user_task->title}}
                    </label>
                    <p style="margin-top:0;">&nbsp;{{$user_task->description}}</p>
                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="bold" style="margin:0;">Would you like to participate as an interpreter? Please list the languages you speak</p>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Language</label>
                    <input class="form-control placeholder-no-fix" type="text" placeholder="Language" name="speak_lang" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel-group accordion" id="trainingSessions">
                    <?php
                        $istraining_session = \App\TrainingSessionCalendar::count();
                    ?>
                    @if ($istraining_session > 0)
                        <p class="bold" style="font-size:13pt;" ><span style="border-bottom: 2px solid #fff;">Training Dates for Reviewer & Interviewer/Data entry</span></p>
                    @endif
                    @foreach ($trainingSessions as $trainingSession)
                        <?php
                            $have_t_event_check = 0;
                            foreach ($t_events as $t_event) {
                                if($t_event->training_session_id == $trainingSession->id) {
                                    $have_t_event_check += 1;
                                }
                            }
                        ?>
                        @if($have_t_event_check > 0 )
                            @if ($trainingSession->isTBD == 1)
                                <div class="panel panel-default registerplase-panel">
                                    <div class="panel-heading registerplase-panelheading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle bold" data-toggle="collapse" data-parent="#trainingSessions" href="#trainingSession{{$trainingSession->id}}"> {{$trainingSession->title}} | {{$trainingSession->street}} | {{$trainingSession->city_town}}, {{$trainingSession->state_province}} {{$trainingSession->zip_postal}} <span class="room-number trainging-room-number"> Room number: {{$trainingSession->room}} </span> </a>
                                        </h4>
                                    </div>
                                    <div id="trainingSession{{$trainingSession->id}}" class="panel-collapse collapse">
                                        <div class="panel-body" style="border-top: 0;">
                                            <div class="row">
                                                @foreach ($t_events as $t_event)
                                                    @if ($t_event->training_session_id == $trainingSession->id)
                                                        <div class="col-sm-6" style="padding-top:10px;">
                                                            <label style="cursor:pointer;" class="bold tms-register-schedule-text"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="user_t_events[]" value="{{$t_event->id}}"> TBD Training Session </label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="panel panel-default registerplase-panel">
                                    <div class="panel-heading registerplase-panelheading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle bold" data-toggle="collapse" data-parent="#trainingSessions" href="#trainingSession{{$trainingSession->id}}"> {{$trainingSession->title}} | {{$trainingSession->street}} | {{$trainingSession->city_town}}, {{$trainingSession->state_province}} {{$trainingSession->zip_postal}} <span class="room-number trainging-room-number"> Room number: {{$trainingSession->room}} </span> </a>
                                        </h4>
                                    </div>
                                    <div id="trainingSession{{$trainingSession->id}}" class="panel-collapse collapse">
                                        <div class="panel-body" style="border-top: 0;">
                                            <div class="row">
                                                @foreach ($t_events as $t_event)
                                                    @if ($t_event->training_session_id == $trainingSession->id)
                                                        <div class="col-sm-6" style="padding-top:10px;">
                                                            <label style="cursor:pointer;" class="bold tms-register-schedule-text"><input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" name="user_t_events[]" value="{{$t_event->id}}">{{$t_event->date}} | {{date( "g:i a", strtotime($t_event->start_time))}}-{{date( "g:i a", strtotime($t_event->end_time))}}</label>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="input-icon">
                        <i class="fa fa-cutlery"></i>
                        <input class="form-control" type="text" placeholder="Dietary Needs" name="dietary">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions text-center">
            <button type="submit" id="register-submit-btn" class="btn green"> Save and Next </button>
        </div>
    </form>
    <!-- END REGISTRATION FORM -->
@endsection
@section('pagelevel_script')
<script src="{{cdn('assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js')}}" type="text/javascript"></script>
@endsection

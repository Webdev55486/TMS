@extends('master')
@section('title')
User Task
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ cdn('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{{ session('status') }}</span>
        </div>
    @endif
    <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> User Task management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" data-toggle="modal" href="#responsive" class="btn green"> Add Task
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="usertask_table">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#usertask_table .checkboxes" />
                              </th>
                              <th> Title </th>
                              <th> Description </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($user_tasks as $user_task)
                            <tr class="odd gradeX" id="user_task_{{$user_task->id}}">
                                <td style="cursor:pointer;vertical-align:middle;">
                                    <input type="checkbox" class="checkboxes" value="1" />
                                </td>
                                <td style="vertical-align:middle;"> {{$user_task->title}}</td>
                                <td style="vertical-align:middle;"> {{$user_task->description}} </td>
                                <td align='center' style="vertical-align:middle;">
                                    <a type="button" onclick="edit_task({{$user_task->id}})" class="btn btn-sm btn-default green user-managebutton-size" style="width:75px;margin-top:5px;">Edit</a>
                                    <a type="button" onclick="delete_task({{$user_task->id}})" class="btn btn-sm btn-default red-flamingo user-managebutton-size" style="width:75px;margin-top:5px;">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <div id="responsive" class="modal fade" data-backdrop="static" tabindex="-1" data-width="560">
        <form action="{{ route('user_task.store') }}" class="add-user-task-form" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h2 class="modal-title text-center">New Task</h2>
                <br />
                <h4 class="modal-title text-center">Enter detail below to create new Task</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Title</label>
                                <input class="form-control placeholder-no-fix" type="text" placeholder="Title" name="task_title" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Description</label>
                                <textarea name="task_description" class="form-control placeholder-no-fix" rows="8" placeholder="Description" required></textarea>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center" style="text-align:center;">
                <button type="button" data-dismiss="modal" class="btn dark">Cancel</button>
                <button type="submit" class="btn green">Proceed</button>
            </div>
        </form>
    </div>

    <div id="responsive1" class="modal fade" data-backdrop="static" tabindex="-1" data-width="560">
        <form action="{{ route('user_task.update') }}" class="add-user-task-form" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h2 class="modal-title text-center">Edit Task</h2>
                <br />
                <h4 class="modal-title text-center">Enter detail below to Edit Task</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    {{ csrf_field() }}
                    <input type="hidden" name="task_id" id="task_id" value="">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Title</label>
                                <input class="form-control placeholder-no-fix" type="text" placeholder="Title" id="_task_title" name="_task_title" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Description</label>
                                <textarea name="_task_description" class="form-control placeholder-no-fix" rows="8" id="_task_description" placeholder="Description" required></textarea>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center" style="text-align:center;">
                <button type="button" data-dismiss="modal" class="btn dark">Cancel</button>
                <button type="submit" class="btn green">Save</button>
            </div>
        </form>
    </div>
@endsection
@section('pagelevel_script')
    <script src="{{ cdn('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ cdn('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script type="text/javascript">

        var BASEURL = "{{ url('/task') }}";

        function delete_task(id){
          var delete_url = BASEURL +'/delete/'+id;
          swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
          }, function (isConfirm) {
              if (isConfirm) {
                  $.ajax({
                      url: delete_url,
                      type: 'get',
                      success: function(result){
                          $("#user_task_"+ id).remove();
                          swal("Deleted!", "Task has been deleted.", "success");
                      },
                      error: function(result){
                          console.log(error);
                      }
                  });
              } else {
                  swal("Cancelled", "Task is safe :)", "error");
              }
          });
        };

        function edit_task(id) {
            var get_url = BASEURL +'/get_single/'+id;

            $.ajax({
                url: get_url,
                type: 'get',
                success: function(result){
                    var html_div = $('#responsive1');
                    console.log(html_div);
                    console.log(result['id']);
                    html_div.find('#task_id').val(result['id']);
                    html_div.find('#_task_title').val(result['title']);
                    html_div.find('#_task_description').val(result['description']);
                    html_div.modal('show');
                },
                error: function(result){
                    console.log(error);
                }
            });
        }
    </script>
@endsection

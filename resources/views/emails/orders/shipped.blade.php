<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div style="width: 100%;height: 100%; background-color: #fff;padding: 20px;">
            <?php
                $email_html = html_entity_decode($setting->location_contents);

                $email_html = str_replace("{username}", $user->first_name." ".$user->last_name, $email_html);

                $email_html = str_replace("{location}", $location->title, $email_html);

                $resultdate = DateTime::createFromFormat('Y-m-d', $event->date);
                $event_date = $resultdate->format('m-d-Y');

                $email_html = str_replace("{date}", $event_date, $email_html);

                $time = date( "g:i a", strtotime($event->start_time))." - ".date( "g:i a", strtotime($event->end_time));

                $email_html = str_replace("{time}", $time, $email_html);

                echo $email_html;
            ?>
        </div>
    </body>
</html>

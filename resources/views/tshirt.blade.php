@extends('master')
@section('title')
Tshirt Report
@endsection
@section('pagelevel_cssplugin')
    <link href="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/dashboard.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <!-- BEGIN REGIONAL STATS PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-qq font-red-sunglo"></i>
                        <span class="caption-subject font-red-sunglo bold uppercase">T - Shirt Report</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="report_table">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th> Site Location Name </th>
                                <th> Tshirt Size </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td style="text-align: left;padding: 8px 10px;vertical-align: middle;">{{$user['user_name']}}</td>
                                    <td>
                                        @if ($user['user_locations'] != null)
                                            @foreach ($user['user_locations'] as $location)
                                                <p style="margin: 2px 0;">{{$location['location_name']}}</p>
                                            @endforeach
                                        @else
                                            <p style="margin: 2px 0;">no location</p>
                                        @endif
                                    </td>
                                    <td style="vertical-align: middle;">{{$user['user_tshirt']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/dashboard.js')}}" type="text/javascript"></script>
@endsection

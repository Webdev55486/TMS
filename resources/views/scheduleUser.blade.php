@extends('master')
@section('title')
Report By Date for locations.
@endsection
@section('pagelevel_cssplugin')
    <link href="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/dashboard.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <!-- BEGIN REGIONAL STATS PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-calendar font-red-sunglo"></i>
                        <span class="caption-subject font-red-sunglo bold uppercase">Report By Date for locations.</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" style="width: 100%;" id="report_table">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th> Email </th>
                                <th> Phone </th>
                                <th> Task </th>
                                <th> Employer </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td style="text-align: left;padding: 8px 10px; vertical-align:middle;">{{$user->first_name}} {{$user->last_name}}</td>
                                    <td style="vertical-align:middle;">{{$user->email}}</td>
                                    <td style="vertical-align:middle;">
                                        @if ($user->phone_cell)
                                            <p style="margin: 2px 0;">{{$user->phone_cell}}</p>
                                        @endif
                                        @if ($user->phone_home)
                                            <p style="margin: 2px 0;">{{$user->phone_home}}</p>
                                        @endif
                                    </td>
                                    <td style="vertical-align:middle;">
                                        <?php
                                            $single_tasks = explode(',', $user->best_serve);
                                            $tasks = \App\UserTasks::all();
                                        ?>
                                        @foreach ($single_tasks as $single_task)
                                            @foreach ($tasks as $task)
                                                @if ($single_task == $task->id)
                                                    <p style="margin: 2px 0;">{{$task->title}}</p>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    </td>
                                    <td style="vertical-align:middle;">{{$user->employer}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/dashboard.js')}}" type="text/javascript"></script>
@endsection

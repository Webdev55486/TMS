@extends('master')
@section('title')
Dietary Report
@endsection
@section('pagelevel_cssplugin')
    <link href="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/dashboard.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <!-- BEGIN REGIONAL STATS PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cutlery font-red-sunglo"></i>
                        <span class="caption-subject font-red-sunglo bold uppercase">Dietary Report</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" style="width: 100%;" id="report_table">
                        <thead>
                            <tr>
                                <th> Name </th>
                                <th> Dietary Need </th>
                                <th> Training Location </th>
                                <th> Date </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                                @if ($user['dietary'] != "")
                                    <tr>
                                        <?php
                                            $resultdate = DateTime::createFromFormat('Y-m-d', $user['date']);
                                            $event_date = $resultdate->format('m-d-Y');
                                        ?>
                                        <td style="text-align: left;padding: 8px 10px">{{$user['name']}}</td>
                                        <td>{{$user['dietary']}}</td>
                                        <td>{{$user['train_title']}}</td>
                                        <td>{{$event_date}}</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/dashboard.js')}}" type="text/javascript"></script>
@endsection

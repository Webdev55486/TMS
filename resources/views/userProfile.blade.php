@extends('master')
@section('title')
Profile
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('pagelevel_style')
    <link href="{{cdn('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/profile.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- BEGIN PAGE TITLE-->
    <!-- END PAGE TITLE-->
    @if (Auth::user()->user_role == 3)
        <div class="page-bar">
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <a data-toggle="modal" href="#responsive" class="btn green btn-sm"> Change User Role </a>
                </div>
            </div>
        </div>
    @endif
    @if (session('status'))
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{{ session('status') }}</span>
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            {{ session('error') }}
        </div>
    @endif
    <h3 class="page-title bold uppercase">
        @if($user->user_role == 0) volunteer @elseif ($user->user_role == 1) Site manager @elseif ($user->user_role == 2) Staff admin @else Master Admin @endif
    </h3>
    <!-- END PAGE HEADER-->
    <div class="profile custom-profile-class">
        <div class="row">
            <div class="col-md-3">
                <div style="padding-bottom:30px;text-align:-webkit-center;">
                    @if(file_exists('assets/images/avatar/'.'/'.$user->photo.'_thumbnail.jpg'))
                      <img alt="" style="max-width: 300px;width:100%;" class="img-responsive pic-bordered" src="{{ cdn('assets/images/avatar/').'/'.$user->photo.'_thumbnail.jpg'}}" />
                    @else
                      <img alt="" style="max-width: 300px;width:100%;" class="img-responsive pic-bordered" src="{{ cdn('assets/images/avatar/nophoto.jpg') }}" />
                    @endif
                </div>
            </div>
            <div class="col-md-9">
                <div class="tabbable-line tabbable-full-width">
                   <ul class="nav nav-tabs">
                       <li class="active" style="width:80px;text-align:center;">
                           <a href="#tab_1_1" data-toggle="tab"> Bio </a>
                       </li>
                       <li>
                           <a href="#tab_1_3" data-toggle="tab"> Work History </a>
                       </li>
                       <li>
                           <a href="#tab_1_6" data-toggle="tab"> Schedule </a>
                       </li>
                   </ul>
                   <div class="tab-content">
                       <div class="tab-pane active" id="tab_1_1">
                           <div class="row">
                               <div class="col-md-12">
                                   <div class="row">
                                       <div class="col-md-9 profile-info">
                                           <h1 class="font-green sbold uppercase">
                                               {{$user->first_name}} &nbsp; {{$user->last_name}} &nbsp;&nbsp;
                                               <label style="font-size:12pt;font-weight:bold;color:#a7a7a7;">
                                                   <i class="fa fa-map-marker"></i>
                                                   United States
                                               </label>
                                           </h1>
                                           <h4 class="font-blue bold">
                                               <?php
                                                    $user_tasks = explode(',', $user->best_serve);
                                                    $tasks = \App\UserTasks::all();
                                                    foreach ($user_tasks as $user_task) {
                                                        foreach ($tasks as $task) {
                                                            if ($task->id == $user_task) {
                                                                echo $task->title.",";
                                                            }
                                                        }
                                                    }
                                               ?>
                                           </h4>
                                           <div class="row">
                                               <div class="col-md-10 col-md-offset-1">
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Cell Phone:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-blue bold profileinfo_p">{{$user->phone_cell}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Home/Work Phone:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-blue bold profileinfo_p">{{$user->phone_home}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Address:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{$user->street}},{{$user->city}},{{$user->state}} {{$user->zip}},United States</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">E-mail:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-blue bold profileinfo_p">{{$user->email}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Tshirt Size:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">
                                                               @if ($user->shirt_size == 1)
                                                                   Small
                                                               @elseif ($user->shirt_size == 2)
                                                                   Medium
                                                               @elseif ($user->shirt_size == 3)
                                                                   Large
                                                               @elseif ($user->shirt_size == 4)
                                                                   X-Large
                                                               @elseif ($user->shirt_size == 5)
                                                                   2X-Large
                                                               @elseif ($user->shirt_size == 6)
                                                                   3X-Large
                                                               @elseif ($user->shirt_size == 7)
                                                                   4X-Large
                                                               @elseif ($user->shirt_size == 8)
                                                                   5X-Large
                                                               @elseif ($user->shirt_size == 9)
                                                                   6X-Large
                                                               @endif
                                                           </p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Dietary Needs:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{$user->dietary}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Employer / School:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{$user->employer}}</p>
                                                       </div>
                                                   </div>
                                                   <div class="row">
                                                       <div class="col-xs-3">
                                                           <p class="font-grey-gallery bold profileinfo_p">Length of Service:</p>
                                                       </div>
                                                       <div class="col-xs-9">
                                                           <p class="font-grey-gallery bold profileinfo_p">{{$user->volunteered_year}} Years</p>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                       <div class="tab-pane" id="tab_1_3">
                           this is work history field.<br />
                           current null.
                       </div>
                       <div class="tab-pane" id="tab_1_6">
                           <div class="row">
                               <div class="col-md-12">
                                   <p class="bold uppercase" style="margin:0;">Location Schedule</p>
                               </div>
                               <?php
                                    $locations = \App\Location::all();
                                    foreach ($locations as $location) {
                                        $islocation = 0;
                                        foreach ($schedule_days as $schedule_day) {
                                            if ($location->id == $schedule_day->location_id){
                                                $islocation += 1;
                                            }
                                        }

                                        if($islocation >0 ){
                                            ?>
                                            <div class="col-md-12" style="padding-top:10px;padding-left:25px;">
                                                <p class="bold" style="margin:0;color:#0097cb;">{{$location->title}} | {{$location->street}} | {{$location->city_town}}, {{$location->state_province}} {{$location->zip_postal}}</p>
                                            </div>
                                            @foreach ($schedule_days as $schedule_day)
                                                @if ($schedule_day->location_id == $location->id)
                                                    <?php
                                                        $resultdate = DateTime::createFromFormat('Y-m-d', $schedule_day->date);
                                                        $schedule_date = $resultdate->format('m-d-Y');
                                                    ?>
                                                    <div class="col-md-12" style="padding-top:10px;padding-left:25px;" id="user_schedule_{{$schedule_day->id}}">
                                                        <label style="cursor:pointer;" class="bold">{{$schedule_date}} | {{date( "g:i a", strtotime($schedule_day->start_time))}} - {{date( "g:i a", strtotime($schedule_day->end_time))}}</label>
                                                        <a href="#" onclick="delete_schedule({{$schedule_day->id}})"><i class="fa fa-close font-red-sunglo"></i></a>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <?php
                                        }
                                    }
                               ?>
                           </div>
                           <div class="row">
                               <div class="col-md-12">
                                   <p class="bold uppercase" style="margin:0;margin-top:10px;">Training Session Schedule</p>
                               </div>
                               <?php
                                    $training_sessions = \App\TrainingSession::all();
                                    foreach ($training_sessions as $training_session) {
                                        $istraining_session = 0;
                                        foreach ($train_schedule_days as $train_schedule_day) {
                                            if ($training_session->id == $train_schedule_day->training_session_id){
                                                $istraining_session += 1;
                                            }
                                        }

                                        if($istraining_session >0 ){
                                            ?>
                                            <div class="col-md-12" style="padding-top:10px;padding-left:25px;">
                                                <p class="bold" style="margin:0;color:#0097cb;">{{$training_session->title}} | {{$training_session->street}} | {{$training_session->city_town}}, {{$training_session->state_province}} {{$training_session->zip_postal}}</p>
                                            </div>
                                            @foreach ($train_schedule_days as $train_schedule_day)
                                                @if ($train_schedule_day->training_session_id == $training_session->id)
                                                    <?php
                                                        $resultdate = DateTime::createFromFormat('Y-m-d', $train_schedule_day->date);
                                                        $schedule_date = $resultdate->format('m-d-Y');
                                                    ?>
                                                    <div class="col-md-12" style="padding-top:10px;padding-left:25px;" id="user_schedule_training_session_{{$train_schedule_day->id}}">
                                                        <label style="cursor:pointer;" class="bold">{{$schedule_date}} | {{date( "g:i a", strtotime($train_schedule_day->start_time))}} - {{date( "g:i a", strtotime($train_schedule_day->end_time))}}</label>
                                                        <a href="#" onclick="delete_schedule_train({{$train_schedule_day->id}})"><i class="fa fa-close font-red-sunglo"></i></a>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <?php
                                        }
                                    }
                               ?>
                           </div>
                           @if ($user->user_role > 0)
                               <div class="row">
                                   <div class="col-md-12">
                                       <p class="bold uppercase" style="margin:0;margin-top:10px;">Staff Meeting Schedule</p>
                                   </div>
                                   <?php
                                        $staff_meetings = \App\StaffMeeting::all();
                                        foreach ($staff_meetings as $staff_meeting) {
                                            $isstaff_meeting = 0;
                                            foreach ($staff_meeting_schedule_days as $staff_meeting_schedule_day) {
                                                if ($staff_meeting->id == $staff_meeting_schedule_day->staff_meeting_id){
                                                    $isstaff_meeting += 1;
                                                }
                                            }

                                            if($isstaff_meeting >0 ){
                                                ?>
                                                <div class="col-md-12" style="padding-top:10px;padding-left:25px;">
                                                    <p class="bold" style="margin:0;color:#0097cb;">{{$staff_meeting->title}} | {{$staff_meeting->street}} | {{$staff_meeting->city_town}}, {{$staff_meeting->state_province}} {{$staff_meeting->zip_postal}}</p>
                                                </div>
                                                @foreach ($staff_meeting_schedule_days as $staff_meeting_schedule_day)
                                                    @if ($staff_meeting_schedule_day->staff_meeting_id == $staff_meeting->id)
                                                        <?php
                                                            $resultdate = DateTime::createFromFormat('Y-m-d', $staff_meeting_schedule_day->date);
                                                            $schedule_date = $resultdate->format('m-d-Y');
                                                        ?>
                                                        <div class="col-md-12" style="padding-top:10px;padding-left:25px;" id="user_schedule_staff_meeting_{{$staff_meeting_schedule_day->id}}">
                                                            <label style="cursor:pointer;" class="bold">{{$schedule_date}} | {{date( "g:i a", strtotime($staff_meeting_schedule_day->start_time))}} - {{date( "g:i a", strtotime($staff_meeting_schedule_day->end_time))}}</label>
                                                            <a href="#" onclick="delete_schedule_staff_meeting({{$staff_meeting_schedule_day->id}})"><i class="fa fa-close font-red-sunglo"></i></a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                                <?php
                                            }
                                        }
                                   ?>
                               </div>
                           @endif
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </div>
    <!-- start add new user -->
    <div id="responsive" class="modal fade" data-backdrop="static" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="{{ route('singleUser.role.change') }}" class="add-user-form" method="post">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h2 class="modal-title text-center">{{$user->first_name}} &nbsp; {{$user->last_name}}</h2>
                        <br />
                        <h4 class="modal-title text-center">User role change</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                            {{ csrf_field() }}
                            <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label visible-ie8 visible-ie9">Role</label>
                                        <select name="user_role" class="select2 form-control">
                                            <option value="0" @if($user->user_role == 0) selected @endif>Volunteer</option>
                                            <option value="1" @if($user->user_role == 1) selected @endif>Site Manager</option>
                                            <option value="2" @if($user->user_role == 2) selected @endif>Staff Admin</option>
                                            <option value="3" @if($user->user_role == 3) selected @endif>Master Admin</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer text-center" style="text-align:center;">
                        <button type="button" data-dismiss="modal" class="btn dark">Cancel</button>
                        <button type="submit" class="btn green">Change</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script type="text/javascript">
        function delete_schedule(id) {

            var url = "{{ url('/single-user/delete_schedule') }}";
            var delete_url = url+"/"+id;
            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Delete !",
              cancelButtonText: "No, Cancel !",
              showLoaderOnConfirm: true,
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){
                            $('#user_schedule_'+id).remove();
                            swal("Deleted!", "Location Schedule has been deleted.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "Location Schedule is safe :)", "error");
                }
            });
        }

        function delete_schedule_train(id) {
            var url = "{{ url('/single-user/delete_schedule_training_session') }}";
            var delete_url = url+"/"+id;

            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Delete !",
              cancelButtonText: "No, Cancel !",
              closeOnConfirm: false,
              closeOnCancel: false,
              showLoaderOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){
                            $('#user_schedule_training_session_'+id).remove();
                            swal("Deleted!", "Training Session Schedule has been deleted.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "Training Session Schedule is safe :)", "error");
                }
            });
        }

        function delete_schedule_staff_meeting(id) {
            var url = "{{ url('/single-user/delete_schedule_staff_meeting') }}";
            var delete_url = url+"/"+id;

            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, Delete !",
              cancelButtonText: "No, Cancel !",
              closeOnConfirm: false,
              closeOnCancel: false,
              showLoaderOnConfirm: true
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){
                            $('#user_schedule_staff_meeting_'+id).remove();
                            swal("Deleted!", "Staff Meeting Schedule has been deleted.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "Training Session Schedule is safe :)", "error");
                }
            });
        }
    </script>
@endsection

@extends('master')
@section('title')
Training Session
@endsection
@section('content')
  <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> Training Session management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" href="{{route('trainingSession.add')}}" class="btn green"> Add Training Session
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="training_session_table">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#training_session_table .checkboxes" />
                              </th>
                              <th> Image </th>
                              <th> Title </th>
                              <th> Address </th>
                              <th> Zip, Postal </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($training_sessions as $training_session)
                            <tr class="odd gradeX" id="location_{{$training_session->id}}">
                                <td style="cursor:pointer;vertical-align:middle;">
                                    <input type="checkbox" class="checkboxes" value="1" />
                                </td>
                                <td style="cursor:pointer;text-align:center;">
                                    @if(file_exists('assets/images/training_session'.'/'.$training_session->img_name.'_thumbnail.jpg'))
                                      <a href="{{url('trainingSession/view/'.$training_session->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/training_session').'/'.$training_session->img_name.'_thumbnail.jpg'}}" /></a>
                                    @else
                                      <a href="{{url('trainingSession/view/'.$training_session->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/training_session/not_found.jpg')}}" /></a>
                                    @endif
                                </td>
                                <td style="vertical-align:middle;"> {{$training_session->title}} </td>
                                <td style="vertical-align:middle;"> {{$training_session->street}}, {{$training_session->city_town}}, {{$training_session->state_province}} &nbsp;{{$training_session->zip_postal}}, {{$training_session->name}}, <label class="font-red-flamingo"> {{$training_session->room}} </label> </td>
                                <td style="vertical-align:middle;"> {{$training_session->zip_postal}} </td>
                                <td align='center' style="vertical-align:middle;">
                                    <a href="{{url('trainingSession/edit/'.$training_session->id)}}" class="btn btn-sm btn-default green user-managebutton-size" style="width:75px;margin-bottom:5px;">Edit</a>
                                    <a type="button" onclick="delete_trainingSession({{$training_session->id}})" class="btn btn-sm btn-default red-flamingo user-managebutton-size" style="width:75px;margin-top:5px;">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
  </div>
@endsection
@section('custom_script')
<script>

  function delete_trainingSession(id){
    var BASEURL = "{{ url('/') }}";
    var delete_url = BASEURL +'/trainingSession/delete/'+id;
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Delete !",
      cancelButtonText: "No, Cancel !",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: delete_url,
                type: 'get',
                success: function(result){
                    $("#location_"+ id).remove();
                    swal("Deleted!", "Training Session has been deleted.", "success");
                },
                error: function(result){
                    console.log(error);
                }
            });
        } else {
            swal("Cancelled", "Training Session is safe :)", "error");
        }
    });
  };
</script>
@endsection

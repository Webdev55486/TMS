@extends('master')
@section('title')
Add Staff Meeting
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('pagelevel_style')
    <link href="{{cdn('css/locationPage.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <form method="post" class="location-add-form" action="{{route('staffMeeting.store')}}" enctype="multipart/form-data">
        <div class="page-bar">
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <button type="submit" class="btn green btn-sm save-staffmeeting-button"> Save Staff Meeting </button>
                </div>
            </div>
        </div>
        @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 ">
                <div class="portlet location-image-form light bordered">
                    <div class="fileinput fileinput-new" data-provides="fileinput" style="width:100%;position:relative;text-align:center;">
                        <div class="fileinput-new thumbnail" style="height:295px;width:100%;">
                            <p style="font-size: 12pt;font-weight: bold;text-transform: uppercase;margin-top:110px;">Staff Meeting Image</p>
                        </div>
                        <div class="image-select-button">
                            <span class="btn default btn-file">
                                <span class="fileinput-new"> Select image </span>
                                <span class="fileinput-exists"> Change </span>
                                <input type="file" name="staff_meeting_image" required>
                            </span>
                            <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                        </div>
                        <div class="fileinput-preview location-page fileinput-exists thumbnail" style="max-width: 100%;width:100%; max-height: 100%;height:298px;"> </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 ">
                <div class="portlet light portlet-fit bordered">
                    <div class="portlet-body">
                        <div id="gmap" class="gmaps"> </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 ">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-red-sunglo">
                            <i class="fa fa-map-marker font-red-sunglo"></i>
                            <span class="caption-subject bold uppercase"> Staff Meeting </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <div class="form-group">
                            <label for="inputlocationTitle">Title*</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="inputlocationTitle" name="staff_meeting_title" placeholder="Title" required>
                                <span class="input-group-addon">
                                    <i class="fa fa-user font-red"></i>
                                </span>
                            </div>
                        </div>
                        <div class="location-body-form">
                            <input type="hidden" id="coorlat" name="coorlat" value="{{$setting->coor_lat}}">
                            <input type="hidden" id="coorlng" name="coorlng" value="{{$setting->coor_lng}}">
                            <div class="address-title">
                                <div class="address-caption font-red-sunglo">
                                    <span class="address-caption-subject bold uppercase"> Address* </span>
                                </div>
                                <div class="actions-button-div">
                                    <div class="btn-group">
                                        <a class="btn btn-sm green" id="gmap_geocoding_btn">Update Map</a>
                                    </div>
                                </div>
                            </div>
                            <div class="address-body-div">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputstreet">Street Address</label>
                                            <input type="text" id="inputstreet" value="{{$setting->company_street}}" name="street_address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputRoom">Room</label>
                                            <input type="text" id="inputRoom" name="room" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputcitytown">City / Town</label>
                                            <input type="text" id="inputcitytown" value="{{$setting->company_city}}" name="town_address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputstateprovinceregion">State / province / Region</label>
                                            <input type="text" id="inputstateprovinceregion" value="{{$setting->company_state}}" name="province" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="inputzippostcode">Zip / Postal Code</label>
                                            <input type="text" id="inputzippostcode" name="postal" value="{{$setting->company_zip}}" onchange="" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label>Country</label>
                                            <select class="form-control" id="country" name="country">
                                                @foreach($countries as $country)
                                                    <option value="{{$country['code']}}" <?php if($country['code'] == "US"){echo "selected";}  ?> >{{$country['name']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="submit" class="btn green save-staffmeeting-button"> Save Staff Meeting </button>
            </div>
        </div>
    </form>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    {{-- <script src="{{cdn('assets/pages/scripts/maps-google.js')}}" type="text/javascript"></script> --}}
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/location.js')}}" type="text/javascript"></script>
    <script>

        var str_coordinatelat = <?php echo $setting->coor_lat; ?>;
        var str_coordinatelng = <?php echo $setting->coor_lng; ?>;
        var coordinatelat = parseFloat(str_coordinatelat);
        var coordinatelng = parseFloat(str_coordinatelng);

        var map;
        var marker;
        function initMap() {
            var mapOptions = {
                center: new google.maps.LatLng(coordinatelat,coordinatelng),
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById('gmap'),mapOptions);

            var myLatLng = {lat: coordinatelat, lng: coordinatelng};

            marker = new google.maps.Marker({
              map: map,
              position: myLatLng
            });
        }

        function resetmap () {
            var street = document.getElementById('inputstreet').value;
            var city = document.getElementById('inputcitytown').value;
            var State = document.getElementById('inputstateprovinceregion').value;
            var postal = document.getElementById('inputzippostcode').value;
            var countryId = document.getElementById('country');
            var countryName = countryId.options[countryId.selectedIndex].text;

            var addressInput = "";

            if (street != "")
            {
                addressInput = addressInput + street + ",";
            }

            if (city != "")
            {
                addressInput = addressInput + city + ",";
            }

            if (State != "")
            {
                addressInput = addressInput + State + postal + ",";
            }

            if (countryName != "")
            {
                addressInput = addressInput + countryName;
            }

        	var geocoder = new google.maps.Geocoder();

        	geocoder.geocode({address: addressInput}, function(results, status) {

        		if (status == google.maps.GeocoderStatus.OK) {

                var myResult = results[0].geometry.location;

                $('#coorlat').val(myResult.lat());
                $('#coorlng').val(myResult.lng());

                createMarker(myResult);

                map.setCenter(myResult);

                map.setZoom(17);
                }
            });
        }

        function createMarker(latlng) {

          if(marker != undefined && marker != ''){
            marker.setMap(null);
            marker = '';
          }

          marker = new google.maps.Marker({
            map: map,
            position: latlng
          });
        }

        $('#gmap_geocoding_btn').click(function() {
            resetmap();
        });

        $('#inputstreet, #inputcitytown, #inputstateprovinceregion, #country').on('change',function(e) {
            resetmap();
        });
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtCsu8LKseS4s2ahkBvYcL_yKKmKWX82g&callback=initMap"></script>
@endsection

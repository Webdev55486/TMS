@extends('master')
@section('title')
Profile Edit
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('pagelevel_style')
    <link href="{{cdn('assets/pages/css/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/profile.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <!-- END PAGE TITLE-->
    @if (session('status'))
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{{ session('status') }}</span>
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            {{ session('error') }}
        </div>
    @endif
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{url('single-user/view/'.$user->id)}}" class="btn green btn-sm"> View Profile </a>
            </div>
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <div class="profile custom-profile-class">
        <div class="row">
            <div class="col-md-3">
                <div style="padding-bottom:30px;text-align:-webkit-center;">
                    @if(file_exists('assets/images/avatar/'.'/'.$user->photo.'_thumbnail.jpg'))
                      <img alt="" style="max-width: 300px;width:100%;" class="img-responsive pic-bordered" src="{{ cdn('assets/images/avatar/').'/'.$user->photo.'_thumbnail.jpg'}}" />
                    @else
                      <img alt="" style="max-width: 300px;width:100%;" class="img-responsive pic-bordered" src="{{ cdn('assets/images/avatar/nophoto.jpg') }}" />
                    @endif
                </div>
            </div>
            <div class="col-md-9">
                <div class="tabbable-line tabbable-full-width">
                   <ul class="nav nav-tabs">
                       <li @if (!session('tap')) class="active" @elseif(session('tap') && session('tap') == 'bio') class="active" @endif style="width:80px;text-align:center;">
                           <a href="#tab_1_1" data-toggle="tab"> Bio </a>
                       </li>
                       <li @if(session('tap') && session('tap') == 'avartar') class="active" @endif>
                           <a href="#tab_1_2" data-toggle="tab"> User Image </a>
                       </li>
                       <li @if(session('tap') && session('tap') == 'password') class="active" @endif>
                           <a href="#tab_1_3" data-toggle="tab"> Password </a>
                       </li>
                       <li>
                           <a href="#tab_1_5" data-toggle="tab"> Email </a>
                       </li>
                       <li >
                           <a href="#tab_1_7" data-toggle="tab"> Task </a>
                       </li>
                   </ul>
                   <div class="tab-content">
                       <div class="tab-pane @if (!session('tap')) active @elseif(session('tap') && session('tap') == 'bio') active @endif" id="tab_1_1">
                           <div class="row">
                               <div class="col-md-10 profile-info">
                                   <form role="form" action="{{route('singleUser.edit.bio')}}" style="margin-bottom:20px;"  method="post" enctype="multipart/form-data">
                                       {{ csrf_field() }}
                                       <input type="hidden" name="user_id" value="{{$user->id}}">
                                       <div class="row">
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">First Name</label>
                                                   <input type="text" placeholder="John" class="form-control" name="first_name" value="{{$user->first_name}}" />
                                               </div>
                                           </div>
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">Last Name</label>
                                                   <input type="text" placeholder="Dae" class="form-control" name="last_name" value="{{$user->last_name}}" />
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">Dietary Needs</label>
                                                   <input type="text" placeholder="Dietary Needs" class="form-control" name="dietary" value="{{$user->dietary}}" />
                                               </div>
                                           </div>
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">Street</label>
                                                   <input type="text" placeholder="775 N Green Mount Rd" class="form-control" name="street" value="{{$user->street}}" />
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">City</label>
                                                   <input type="text" placeholder="Esperance" class="form-control" name="city" value="{{$user->city}}" />
                                               </div>
                                           </div>
                                           <div class="col-sm-6">
                                               <div class="row">
                                                   <div class="col-xs-6">
                                                       <div class="form-group">
                                                           <label class="control-label">State</label>
                                                           <input type="text" placeholder="WA" class="form-control" name="state" value="{{$user->state}}" />
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-6">
                                                       <div class="form-group">
                                                           <label class="control-label">Zip/Postal</label>
                                                           <input type="text" placeholder="62221" class="form-control" name="zip" value="{{$user->zip}}" />
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">Employer</label>
                                                   <input type="text" placeholder="Employer" class="form-control" name="employer" value="{{$user->employer}}" />
                                               </div>
                                           </div>
                                           <div class="col-sm-6">
                                               <div class="row">
                                                   <div class="col-xs-6">
                                                       <div class="form-group">
                                                           <label class="control-label">Cell Phone</label>
                                                           <input type="text" placeholder="Phone Number" class="form-control" id="phone_cell" name="phone_cell" value="{{$user->phone_cell}}" />
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-6">
                                                       <div class="form-group">
                                                           <label class="control-label">Home/Work Phone</label>
                                                           <input type="text" placeholder="Phone Number" class="form-control" id="phone_home" name="phone_home" value="{{$user->phone_home}}" />
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                    <label class="control-label">T-shirt size</label>
                                                    <select name="shirt_size" class="select2 form-control">
                                                        <option value="1" @if ($user->shirt_size == 1) selected @endif >Small</option>
                                                        <option value="2" @if ($user->shirt_size == 2) selected @endif >Medium</option>
                                                        <option value="3" @if ($user->shirt_size == 3) selected @endif >Large</option>
                                                        <option value="4" @if ($user->shirt_size == 4) selected @endif >X-Large</option>
                                                        <option value="5" @if ($user->shirt_size == 5) selected @endif >2X-Large</option>
                                                        <option value="6" @if ($user->shirt_size == 6) selected @endif >3X-Large</option>
                                                        <option value="7" @if ($user->shirt_size == 7) selected @endif >4X-Large</option>
                                                        <option value="8" @if ($user->shirt_size == 8) selected @endif >5X-Large</option>
                                                        <option value="9" @if ($user->shirt_size == 9) selected @endif >6X-Large</option>
                                                    </select>
                                               </div>
                                           </div>
                                           <div class="col-sm-6">
                                               <div class="form-group">
                                                   <label class="control-label">Volunteered Year</label>
                                                   <select name="volunteered_year" id="volunteered_year_list" class="select2 form-control">
                                                       <option value="1" @if($user->volunteered_year == 1) selected @endif >This is My First Year!</option>
                                                       @for ($i=2; $i < 41; $i++)
                                                           <option value="{{$i}}" @if($user->volunteered_year == $i) selected @endif >{{$i}} Years</option>
                                                       @endfor
                                                   </select>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="margiv-top-10">
                                           <button type="submit" class="btn green"> Save Changes </a>
                                       </div>
                                   </form>
                               </div>
                           </div>
                       </div>
                       <div class="tab-pane @if(session('tap') && session('tap') == 'avartar') active @endif" id="tab_1_2">
                           <form action="{{ route('singleUser.edit.avartar') }}" role="form" method="post" class="user-avartar-upload-form" enctype="multipart/form-data">
                               {{ csrf_field() }}
                               <input type="hidden" name="user_id" value="{{$user->id}}">
                               <div class="form-group">
                                   <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                           <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                       <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                       <div>
                                           <span class="btn default btn-file">
                                               <span class="fileinput-new"> Select image </span>
                                               <span class="fileinput-exists"> Change </span>
                                               <input type="file" name="user_image" required>
                                           </span>
                                           <a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                       </div>
                                   </div>
                               </div>
                               <div class="margin-top-10">
                                   <button class="btn green" type="submit"> Submit </button>
                               </div>
                           </form>
                       </div>
                       <div class="tab-pane @if(session('tap') && session('tap') == 'password') active @endif" id="tab_1_3">
                           <div class="row">
                               <div class="col-md-8 profile-info">
                                   <form action="{{ route('singleUser.edit.password') }}" class="changepassword-form" method="post" enctype="multipart/form-data" >
                                       <div class="alert alert-danger display-hide">
                                           <button class="close" data-close="alert"></button>
                                           <span>Enter correct data. </span>
                                       </div>
                                       {{ csrf_field() }}
                                       <input type="hidden" name="user_id" value="{{$user->id}}">
                                       <div class="form-group">
                                           <label class="control-label">New Password</label>
                                           <input type="password" autocomplete="off" class="form-control" id="change_password" name="password" required /> </div>
                                       <div class="form-group">
                                           <label class="control-label">Re-type New Password</label>
                                           <input type="password" autocomplete="off" class="form-control" name="password_confirmation" required /> </div>
                                       <div class="margin-top-10">
                                           <button type="submit" class="btn green"> Change Password </button>
                                       </div>
                                   </form>
                               </div>
                           </div>
                       </div>
                       <div class="tab-pane" id="tab_1_5">
                           <div class="row">
                               <div class="col-md-10 profile-info">
                                   <form role="form" action="{{route('singleUser.edit.email')}}" style="margin-bottom:20px;" method="post" enctype="multipart/form-data">
                                       {{ csrf_field() }}
                                       <input type="hidden" name="user_id" value="{{$user->id}}">
                                       <div class="form-group">
                                           <label class="control-label">Your Email</label>
                                           <input type="text" placeholder="example@mail.com" name="email" value="{{$user->email}}" class="form-control" /> </div>
                                       <div class="margiv-top-10">
                                           <button type="submit" href="javascript:;" class="btn green"> Save Changes </button>
                                       </div>
                                   </form>
                               </div>
                           </div>
                       </div>
                       <div class="tab-pane" id="tab_1_7">
                           <div class="row">
                               <div class="col-md-10 profile-info">
                                   <form role="form" action="{{route('singleUser.edit.task')}}" style="margin-bottom:20px;" method="post" enctype="multipart/form-data">
                                       {{ csrf_field() }}
                                       <input type="hidden" name="user_id" value="{{$user->id}}">
                                       <div class="form-group">
                                           <label class="control-label">Your Task</label>
                                           <br />
                                           <?php
                                               $user_tasks = explode(',', $user->best_serve);
                                               $tasks = \App\UserTasks::orderBy('id', 'ASC')->get();
                                           ?>
                                           @foreach ($tasks as $task)
                                               <label style="cursor:pointer;margin-left:5px;" class="bold">
                                                   <input type="checkbox" class="icheck" data-checkbox="icheckbox_minimal" @if (in_array($task->id, $user_tasks)) checked @endif name="user_task[]" value="{{$task->id}}">
                                                   {{$task->title}}
                                               </label>
                                               <p style="margin-top:0;">&nbsp;{{$task->description}}</p>
                                           @endforeach
                                           <div class="margiv-top-10">
                                               <button type="submit" href="javascript:;" class="btn green"> Save Changes </button>
                                           </div>
                                       </div>
                                   </form>
                               </div>
                           </div>
                       </div>
                   </div>
               </div>
           </div>
       </div>
    </div>
    <!-- start add new user -->
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/location.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $("#phone_cell").mask("(000) 000-0000");
        $("#phone_home").mask("(000) 000-0000");
    </script>
@endsection

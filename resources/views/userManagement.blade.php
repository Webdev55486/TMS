@extends('master')
@section('title')
User Lists
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ cdn('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            <button class="close" data-close="alert"></button>
            <span>{{ session('status') }}</span>
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger">
            <button class="close" data-close="alert"></button>
            {{ session('error') }}
        </div>
    @endif
    <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> User management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" data-toggle="modal" href="#responsive" class="btn green"> Add User
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="userlist_table">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#userlist_table .checkboxes" />
                              </th>
                              <th> Photo </th>
                              <th> Name </th>
                              <th> Email </th>
                              <th> Address </th>
                              <th> Level </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $user)
                            @if(Auth::user()->user_role >= $user->user_role && Auth::user()->id != $user->id)
                                <tr class="odd gradeX" id="user_{{$user->id}}">
                                    <td style="cursor:pointer;vertical-align:middle;">
                                        <input type="checkbox" class="checkboxes" value="1" />
                                    </td>
                                    <td style="cursor:pointer;text-align:center;">
                                        @if(file_exists('assets/images/avatar'.'/'.$user->photo.'_thumbnail.jpg'))
                                          <a href="{{ url('single-user/view/'.$user->id) }}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/avatar').'/'.$user->photo.'_thumbnail.jpg'}}" /></a>
                                        @else
                                          <a href="{{ url('single-user/view/'.$user->id) }}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/avatar/nophoto.jpg')}}" /></a>
                                        @endif
                                    </td>
                                    <td style="vertical-align:middle;"><a href="{{ url('single-user/view/'.$user->id) }}" style="cursor:pointer;">{{$user->first_name}}&nbsp;{{$user->last_name}}</a></td>
                                    <td style="vertical-align:middle;"> {{$user->email}} </td>
                                    <td style="vertical-align:middle;"> {{$user->street}}, {{$user->city}}, {{$user->state}} &nbsp;{{$user->zip}} </td>
                                    <td style="vertical-align:middle;"> @if($user->user_role == 3) Master Admin @elseif($user->user_role == 2) Staff Admin @elseif($user->user_role == 1) Site Manager @else Volunteers @endif</td>
                                    <td align='center' style="vertical-align:middle;">
                                        <a href="{{url('single-user/edit/'.$user->id)}}" class="btn btn-sm btn-default green user-managebutton-size" style="width:75px;margin-bottom:5px;">Edit</a>
                                        <a type="button" onclick="delete_user({{$user->id}})" class="btn btn-sm btn-default red-flamingo user-managebutton-size" style="width:75px;margin-top:5px;">Delete</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
    </div>
    <div id="responsive" class="modal fade" data-backdrop="static" tabindex="-1" data-width="560">
        <form action="{{ route('singleUser.add') }}" class="add-user-form" method="post">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h2 class="modal-title text-center">New User</h2>
                <br />
                <h4 class="modal-title text-center">Enter detail below to create new user</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">First Name</label>
                                <div class="input-icon">
                                    <i class="fa fa-font"></i>
                                    <input class="form-control placeholder-no-fix" type="text" placeholder="First Name" name="first_name" />
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Last Name</label>
                                <div class="input-icon">
                                    <i class="fa fa-font"></i>
                                    <input class="form-control placeholder-no-fix" type="text" placeholder="Last Name" name="last_name" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                                <label class="control-label visible-ie8 visible-ie9">Email</label>
                                <div class="input-icon">
                                    <i class="fa fa-envelope"></i>
                                    <input class="form-control placeholder-no-fix" type="text" id="register_email" placeholder="Email" name="email" /> </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Role</label>
                                <select name="user_role" class="select2 form-control">
                                    <option selected value="0">Volunteer</option>
                                    <option value="1">Site Manager</option>
                                    <option value="2">Staff Admin</option>
                                    @if (Auth::user()->user_role == 3)
                                        <option value="3">Master Admin</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Password</label>
                                <div class="input-icon">
                                    <i class="fa fa-lock"></i>
                                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" id="register_password" placeholder="Password" name="password" /> </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label visible-ie8 visible-ie9">Re-type Password</label>
                                <div class="input-icon">
                                    <i class="fa fa-check"></i>
                                    <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Re-type Password" name="password_confirmation" />
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer text-center" style="text-align:center;">
                <button type="button" data-dismiss="modal" class="btn dark">Cancel</button>
                <button type="submit" class="btn green">Proceed</button>
            </div>
        </form>
    </div>
@endsection
@section('pagelevel_script')
    <script src="{{ cdn('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js') }}" type="text/javascript"></script>
    <script src="{{ cdn('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js') }}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script type="text/javascript">
        var BASEURL = "{{ url('/single-user') }}";
        function delete_user(id) {
            var delete_url = BASEURL +'/delete/'+id;
            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete!",
              cancelButtonText: "No, cancel!",
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){
                            $("#user_"+id).remove();
                            swal("Deleted!", "User has been deleted.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "User is safe :)", "error");
                }
            });
          }
    </script>
@endsection

@extends('master')
@section('title')
DashBoard
@endsection
@section('pagelevel_cssplugin')
    <link href="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/dashboard.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="page-bar">
        <div class="page-toolbar">
        </div>
    </div>
    <div class="row widget-row" style="padding-top: 20px;">
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Active Volunteers</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-green icon-users"></i>
                    <div class="widget-thumb-body tms-dashboard-thumb-widget" >
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$overview_data['active_volunteer']}}">0</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Total Scheduled Hours</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-red icon-clock"></i>
                    <div class="widget-thumb-body tms-dashboard-thumb-widget">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$overview_data['total_scheduled_time']}}">0</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Total active locations</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-purple  icon-pointer"></i>
                    <div class="widget-thumb-body tms-dashboard-thumb-widget">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$overview_data['active_location']}}">0</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
        <div class="col-md-3">
            <!-- BEGIN WIDGET THUMB -->
            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">First Year Volunteers</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-blue icon-user-follow"></i>
                    <div class="widget-thumb-body tms-dashboard-thumb-widget">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{$overview_data['first_year_volunteer']}}">0</span>
                    </div>
                </div>
            </div>
            <!-- END WIDGET THUMB -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <!-- BEGIN REGIONAL STATS PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-docs font-red-sunglo"></i>
                        <span class="caption-subject font-red-sunglo bold uppercase">Report</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="row">
                                <div class="btn-group">
                                    <a href="{{route('report.dietary')}}" class="icon-btn tooltips" data-placement="bottom" data-original-title="Dietary Needs Report"><i class="fa fa-cutlery"></i><div> Reports </div></a>
                                </div>
                                <div class="btn-group">
                                    <a target="_blank" id="btnGroupVerticalDrop1" class="icon-btn dropdown-toggle tooltips" data-toggle="dropdown" data-placement="top" data-original-title="Tshirt Report"><i class="fa fa-qq"></i><div> Reports </div></a>
                                    <ul class="dropdown-menu" role="menu" aria-labelledby="btnGroupVerticalDrop1">
                                        <li>
                                            <a href="{{route('report.tshirt.first')}}" target="_blank"> FIRST YEAR ONLY </a>
                                        </li>
                                        <li>
                                            <a href="{{route('report.tshirt')}}" target="_blank"> ALL VOLUNTEERS </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="btn-group">
                                    <a data-toggle="modal" href="#location-schedule-user-report" class="icon-btn tooltips" data-placement="bottom" data-original-title="Schedule Report"><i class="fa fa-calendar"></i><div> Reports </div></a>
                                </div>
                                <div class="btn-group">
                                    <a href="{{route('report.traininguser')}}" target="_blank" class="icon-btn tooltips" data-placement="bottom" data-original-title="Training Completion Report"><i class="fa fa-graduation-cap"></i><div> Reports </div></a>
                                </div>
                                <div class="btn-group">
                                    <a href="{{route('report.volunteer')}}" target="_blank" class="icon-btn tooltips" data-placement="bottom" data-original-title="Volunteer Report"><i class="fa fa-users"></i><div> Reports </div></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6">
            <!-- BEGIN REGIONAL STATS PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-pointer font-red-sunglo"></i>
                        <span class="caption-subject font-red-sunglo bold uppercase">Locations</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="dashboard-location-table">
                        <thead>
                            <tr>
                                <th> Location Name </th>
                                <th> Number Of Volunteers </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($active_locations as $active_location)
                                <tr>
                                    <td style="text-align:left;padding: 8px 10px;">{{$active_location['location_name']}}</td>
                                    <td>{{$active_location['active_user']}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div id="location-schedule-user-report" class="modal fade" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" class="form-horizontal" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Select Location Schedule</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-group accordion" id="locations">
                        @foreach ($locations as $location)
                            <?php
                                $have_event_check1 = 0;
                                foreach ($events as $event) {
                                    if($event->location_id == $location->id) {
                                        $have_event_check1 += 1;
                                    }
                                }
                            ?>
                            @if( $have_event_check1 > 0 )
                                <div class="panel panel-default registerplase-panel">
                                    <div class="panel-heading registerplase-panelheading">
                                        <h4 class="panel-title">
                                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#locations" href="#location{{$location->id}}"> {{$location->title}} | {{$location->street}} | {{$location->city_town}}, {{$location->state_province}} {{$location->zip_postal}}, <span class="room-number location-room-number"> {{$location->room}} </span> </a>
                                        </h4>
                                    </div>
                                    <div id="location{{$location->id}}" class="panel-collapse collapse">
                                        <div class="panel-body" style="border-top: 0;">
                                            <div class="row">
                                                @foreach ($events as $event)
                                                    <?php
                                                        $resultdate = DateTime::createFromFormat('Y-m-d', $event->date);
                                                        $event_date = $resultdate->format('m-d-Y');
                                                        $event_day = str_replace('-', '/', $event_date);
                                                    ?>
                                                    @if ($event->location_id == $location->id)
                                                        <div class="col-sm-6" style="padding-top:10px;">
                                                            <a style="cursor:pointer;" target="_blank" href="{{ url('report/location-date-user/'.$event->id) }}" class="bold"> {{$event_date}} | {{date( "g:i a", strtotime($event->start_time))}} - {{date( "g:i a", strtotime($event->end_time))}} </a>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.waypoints.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/counterup/jquery.counterup.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/dashboard.js')}}" type="text/javascript"></script>
@endsection

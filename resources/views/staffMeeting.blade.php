@extends('master')
@section('title')
Training Session
@endsection
@section('content')
  <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> Staff Meeting management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a id="sample_editable_1_new" href="{{route('staffMeeting.add')}}" class="btn green"> Add Staff Meeting
                                        <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="staff_meeting_table">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#staff_meeting_table .checkboxes" />
                              </th>
                              <th> Image </th>
                              <th> Title </th>
                              <th> Address </th>
                              <th> Zip, Postal </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @foreach($staff_meetings as $staff_meeting)
                            <tr class="odd gradeX" id="location_{{$staff_meeting->id}}">
                                <td style="cursor:pointer;vertical-align:middle;">
                                    <input type="checkbox" class="checkboxes" value="1" />
                                </td>
                                <td style="cursor:pointer;text-align:center;">
                                    @if(file_exists('assets/images/staff_meeting'.'/'.$staff_meeting->img_name.'_thumbnail.jpg'))
                                      <a href="{{url('staffMeeting/view/'.$staff_meeting->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/staff_meeting').'/'.$staff_meeting->img_name.'_thumbnail.jpg'}}" /></a>
                                    @else
                                      <a href="{{url('staffMeeting/view/'.$staff_meeting->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/staff_meeting/not_found.jpg')}}" /></a>
                                    @endif
                                </td>
                                <td style="vertical-align:middle;"> {{$staff_meeting->title}} </td>
                                <td style="vertical-align:middle;"> {{$staff_meeting->street}}, {{$staff_meeting->city_town}}, {{$staff_meeting->state_province}} &nbsp;{{$staff_meeting->zip_postal}}, {{$staff_meeting->name}} , <label class="font-red-flamingo"> {{$staff_meeting->room}} </label></td>
                                <td style="vertical-align:middle;"> {{$staff_meeting->zip_postal}} </td>
                                <td align='center' style="vertical-align:middle;">
                                    <a href="{{url('staffMeeting/edit/'.$staff_meeting->id)}}" class="btn btn-sm btn-default green user-managebutton-size" style="width:75px;margin-bottom:5px;">Edit</a>
                                    <a type="button" onclick="delete_staffMeeting({{$staff_meeting->id}})" class="btn btn-sm btn-default red-flamingo user-managebutton-size" style="width:75px;margin-top:5px;">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
  </div>
@endsection
@section('custom_script')
<script>

  function delete_staffMeeting(id){
    var BASEURL = "{{ url('/') }}";
    var delete_url = BASEURL +'/staffMeeting/delete/'+id;
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, Delete !",
      cancelButtonText: "No, Cancel !",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: delete_url,
                type: 'get',
                success: function(result){
                    $("#location_"+ id).remove();
                    swal("Deleted!", "Staff Meeting has been deleted.", "success");
                },
                error: function(result){
                    console.log(error);
                }
            });
        } else {
            swal("Cancelled", "Staff Meeting is safe :)", "error");
        }
    });
  };
</script>
@endsection

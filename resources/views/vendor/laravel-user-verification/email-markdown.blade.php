<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div style="width: 100%;height: 100%; background-color: #fff;padding: 20px;">
            <?php
                $setting = \App\SiteSetting::get()->first();
                echo html_entity_decode($setting->register_email_contents);
            ?>
        </div>
    </body>
</html>

@extends('master')
@section('title')
Location
@endsection
@section('content')
  <div class="row">
      <div class="col-md-12">
          <!-- BEGIN EXAMPLE TABLE PORTLET-->
          <div class="portlet light bordered">
              <div class="portlet-title">
                  <div class="caption font-dark">
                      <i class="icon-settings font-dark"></i>
                      <span class="caption-subject bold uppercase"> Location management</span>
                  </div>
              </div>
              <div class="portlet-body">
                  @if (Auth::user()->user_role > 1)
                      <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a id="sample_editable_1_new" href="{{route('location.add')}}" class="btn green"> Add Location
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                  <table class="table table-striped table-bordered table-hover table-checkable order-column" id="location_table">
                      <thead>
                          <tr>
                              <th>
                                  <input type="checkbox" class="group-checkable" data-set="#location_table .checkboxes" />
                              </th>
                              <th> Image </th>
                              <th> Title </th>
                              <th> Address </th>
                              <th> Zip, Postal </th>
                              <th> Managers </th>
                              <th> Action </th>
                          </tr>
                      </thead>
                      <tbody>
                        @if (Auth::user()->user_role > 1)
                            @foreach($locations as $location)
                                <tr class="odd gradeX" id="location_{{$location->id}}">
                                    <td style="cursor:pointer;vertical-align:middle;">
                                        <input type="checkbox" class="checkboxes" value="1" />
                                    </td>
                                    <td style="cursor:pointer;text-align:center;vertical-align:middle;">
                                        @if(file_exists('assets/images/location'.'/'.$location->img_name.'_thumbnail.jpg'))
                                          <a href="{{url('location/view/'.$location->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/location').'/'.$location->img_name.'_thumbnail.jpg'}}" /></a>
                                        @else
                                          <a href="{{url('location/view/'.$location->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/location/not_found.jpg')}}" /></a>
                                        @endif
                                    </td>
                                    <td style="vertical-align:middle;"> {{$location->title}} </td>
                                    <td style="vertical-align:middle;"> {{$location->street}}, {{$location->city_town}}, {{$location->state_province}} &nbsp;{{$location->zip_postal}}, {{$location->name}},  <label class="font-red-flamingo"> {{$location->room}} </label></td>
                                    <td style="vertical-align:middle;"> {{$location->zip_postal}} </td>
                                    <td style="vertical-align:middle;">
                                        @if ($location->manager != "")
                                            <?php
                                                $managers = explode(',', $location->manager);
                                            ?>
                                            @foreach ($users as $user)
                                                @if (in_array($user->id, $managers))
                                                    <p>{{$user->first_name}} {{$user->last_name}}</p>
                                                @endif
                                            @endforeach
                                        @else
                                            <p class="bold">no manager</p>
                                        @endif
                                    </td>
                                    <td align='center' style="vertical-align:middle;">
                                        <a href="{{url('location/edit/'.$location->id)}}" class="btn btn-sm btn-default green user-managebutton-size" style="width:75px;margin-bottom:5px;">Edit</a>
                                        <a type="button" onclick="delete_location({{$location->id}})" class="btn btn-sm btn-default red-flamingo user-managebutton-size" style="width:75px;margin-top:5px;">Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        @elseif (Auth::user()->user_role == 1)
                            @foreach($locations as $location)
                                @if ($location->manager == Auth::user()->id)
                                    <tr class="odd gradeX" id="location_{{$location->id}}">
                                        <td style="cursor:pointer;vertical-align:middle;">
                                            <input type="checkbox" class="checkboxes" value="1" />
                                        </td>
                                        <td style="cursor:pointer;text-align:center;vertical-align:middle;">
                                            @if(file_exists('assets/images/location'.'/'.$location->img_name.'_thumbnail.jpg'))
                                              <a href="{{url('location/view/'.$location->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/location').'/'.$location->img_name.'_thumbnail.jpg'}}" /></a>
                                            @else
                                              <a href="{{url('location/view/'.$location->id)}}"><img style="width:100%;max-width:150px;min-width:150px;" src="{{cdn('assets/images/location/not_found.jpg')}}" /></a>
                                            @endif
                                        </td>
                                        <td style="vertical-align:middle;"> {{$location->title}} </td>
                                        <td style="vertical-align:middle;"> {{$location->street}}, {{$location->city_town}}, {{$location->state_province}} &nbsp;{{$location->zip_postal}}, {{$location->name}},  {{$location->room}}</td>
                                        <td style="vertical-align:middle;"> {{$location->zip_postal}} </td>
                                        <td align='center' style="vertical-align:middle; pointer-events: none;opacity: 0.6;">
                                            <a class="btn btn-sm btn-default green user-managebutton-size" style="width:75px;margin-bottom:5px;">Edit</a>
                                            <a type="button" class="btn btn-sm btn-default red-flamingo user-managebutton-size" style="width:75px;margin-top:5px;">Delete</a>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        @endif
                      </tbody>
                  </table>
              </div>
          </div>
          <!-- END EXAMPLE TABLE PORTLET-->
      </div>
  </div>
@endsection
@section('custom_script')
<script>

  function delete_location(id){
    var BASEURL = "{{ url('/') }}";
    var delete_url = BASEURL +'/location/delete/'+id;
    swal({
      title: "Are you sure?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete location!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            $.ajax({
                url: delete_url,
                type: 'get',
                success: function(result){
                    $("#location_"+ id).remove();
                    swal("Deleted!", "Location has been deleted.", "success");
                },
                error: function(result){
                    console.log(error);
                }
            });
        } else {
            swal("Cancelled", "Location is safe :)", "error");
        }
    });
  };
</script>
@endsection

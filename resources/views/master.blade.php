<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <title>@yield('title')</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  <meta name="_token" content="{{ csrf_token() }}" />
  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/simple-line-icons/simple-line-icons.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/uniform/css/uniform.default.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css" />
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
  <link href="{{ cdn('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ cdn('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <!-- END GLOBAL MANDATORY STYLES -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <link href="{{cdn('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
  @yield('pagelevel_cssplugin')
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL STYLES -->
  <link href="{{cdn('assets/global/css/components-md.css')}}" rel="stylesheet" id="style_components" type="text/css" />
  <link href="{{cdn('assets/global/css/plugins-md.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/global/plugins/icheck/skins/all.css')}}" rel="stylesheet" type="text/css" />
  <!-- END THEME GLOBAL STYLES -->
  <!-- BEGIN PAGE LEVEL STYLES -->
  @yield('pagelevel_style')
  <!-- END PAGE LEVEL STYLES -->
  <!-- BEGIN THEME LAYOUT STYLES -->
  <link href="{{cdn('assets/layouts/layout/css/layout.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('assets/layouts/layout/css/themes/darkblue.min.css')}}" rel="stylesheet" type="text/css" id="style_color" />
  <link href="{{cdn('assets/layouts/layout/css/custom.min.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{cdn('css/custom.css')}}" rel="stylesheet" type="text/css" />
  @yield('custom_style')
  <!-- END THEME LAYOUT STYLES -->
  <link rel="icon" type="image/png" sizes="32x32" href="{{cdn('assets/images/components/favicon/fav_ico.png')}}">
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" onload="initialize()">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
  <!-- BEGIN HEADER -->
  <div class="page-header navbar navbar-fixed-top" id="homepage-header-div">
      <!-- BEGIN HEADER INNER -->
      <div class="page-header-inner ">
          <!-- BEGIN LOGO -->
          <?php
              $setting = \App\SiteSetting::first();
          ?>
          <div class="page-logo">
              <a href="{{url('/')}}" style="margin-left: 15px;">
                  @if(file_exists('assets/images/company_setting'.'/'.$setting->company_logo))
                    <img src="{{cdn('assets/images/company_setting').'/'.$setting->company_logo}}" style="width:125px;max-height:33px;" alt="logo" class="logo-default" /> </a>
                  @else
                    <img src="{{ cdn('assets/images/company_setting/default_logo.png') }}" style="width:125px; max-height:32px;" alt="logo" class="logo-default" /> </a>
                  @endif
              <div class="menu-toggler sidebar-toggler"> </div>
          </div>
          <!-- END LOGO -->
          <!-- BEGIN RESPONSIVE MENU TOGGLER -->
          <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
          <!-- END RESPONSIVE MENU TOGGLER -->
          <!-- BEGIN TOP NAVIGATION MENU -->
          <div class="top-menu">
              <ul class="nav navbar-nav pull-right">
                  <li class="dropdown dropdown-user">
                      <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="padding-left: 5px;padding-bottom: 10px;background-color: #364150;">
                          @if(file_exists('assets/images/avatar/'.'/'.Auth::user()->photo.'_thumbnail.jpg'))
                            <img alt="" class="img-circle" src="{{ cdn('assets/images/avatar/').'/'.Auth::user()->photo.'_thumbnail.jpg'}}" />
                          @else
                            <img alt="" class="img-circle" src="{{ cdn('assets/images/avatar/nophoto.jpg') }}" />
                          @endif
                          <span class="username username-hide-on-mobile" style="display: inline-block;"> {{Auth::user()->first_name}}&nbsp;{{Auth::user()->last_name}}</span>
                          <i class="fa fa-angle-down"></i>
                      </a>
                      <ul class="dropdown-menu dropdown-menu-default">
                          <li>
                              <a href="{{ route('profile.view') }}">
                                  <i class="icon-user"></i> Profile </a>
                          </li>
                          <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="icon-key"></i> Log Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                          </li>
                      </ul>
                  </li>
              </ul>
          </div>
          <!-- END TOP NAVIGATION MENU -->
      </div>
      <!-- END HEADER INNER -->
  </div>
  <!-- END HEADER -->
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      <div class="page-sidebar-wrapper">
          <!-- BEGIN SIDEBAR -->
          <div class="page-sidebar navbar-collapse collapse" id="page-sidebar-height">
              <!-- BEGIN SIDEBAR MENU -->
              <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                  <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                <li class="sidebar-toggler-wrapper hide">
                  <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                  <div class="sidebar-toggler"> </div>
                  <!-- END SIDEBAR TOGGLER BUTTON -->
                </li>
                <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                @if(Auth::user()->user_role > 1)
                    <li class="nav-item start @if( Route::currentRouteName()=='dashboard')open active @endif">
                      <a href="{{ route('dashboard') }}" class="nav-link nav-toggle">
                          <i class="icon-home"></i>
                          <span class="title">DashBoard</span>
                          @if(Route::currentRouteName()=='dashboard')
                          <span class="selected"></span>
                          @endif
                      </a>
                    </li>
                @endif
                <li class="nav-item  @if( Route::currentRouteName()=='profile.view' || Route::currentRouteName()=='profile.edit') open active @endif">
                    <a href="{{route('profile.view')}}" class="nav-link nav-toggle">
                        <i class="icon-user"></i>
                        <span class="title">Profile</span>
                        @if(Route::currentRouteName()=='profile.view' || Route::currentRouteName()=='profile.edit')
                            <span class="selected"></span>
                        @endif
                    </a>
                </li>
                @if(Auth::user()->user_role > 1)
                    <li class="nav-item start @if( Route::currentRouteName()=='user.lists' || Route::currentRouteName()=='singleUser.view' || Route::currentRouteName()=='singleUser.edit' )open active @endif">
                        <a href="{{ route('user.lists') }}" class="nav-link nav-toggle">
                            <i class="icon-users"></i>
                            <span class="title">Users & Volunteers</span>
                            @if(Route::currentRouteName()=='user.lists' || Route::currentRouteName()=='singleUser.view' || Route::currentRouteName()=='singleUser.edit')
                            <span class="selected"></span>
                            @endif
                        </a>
                    </li>
                    <li class="nav-item start @if( Route::currentRouteName()=='user_task')open active @endif">
                        <a href="{{ route('user_task') }}" class="nav-link nav-toggle">
                            <i class="icon-badge"></i>
                            <span class="title">Volunteer Tasks</span>
                            @if(Route::currentRouteName()=='user_task')
                            <span class="selected"></span>
                            @endif
                        </a>
                    </li>
                @endif
                @if(Auth::user()->user_role > 0)
                    <li class="nav-item start @if( Route::currentRouteName()=='location' || Route::currentRouteName()=='location.add' || Route::currentRouteName()=='location.edit' || Route::currentRouteName()=='location.view')open active @endif">
                        <a href="{{ route('location') }}" class="nav-link nav-toggle">
                            <i class="icon-pointer"></i>
                            <span class="title">Site Locations</span>
                            @if(Route::currentRouteName()=='location' || Route::currentRouteName()=='location.add' || Route::currentRouteName()=='location.view')
                            <span class="selected"></span>
                            @endif
                        </a>
                    </li>
                @endif
                @if(Auth::user()->user_role > 1)
                    <li class="nav-item start @if( Route::currentRouteName()=='trainingSession' || Route::currentRouteName()=='trainingSession.add' || Route::currentRouteName()=='trainingSession.edit' || Route::currentRouteName()=='trainingSession.view')open active @endif">
                        <a href="{{ route('trainingSession') }}" class="nav-link nav-toggle">
                          <i class="icon-graduation"></i>
                          <span class="title">Training Sessions</span>
                          @if(Route::currentRouteName()=='trainingSession' || Route::currentRouteName()=='trainingSession.add' || Route::currentRouteName()=='trainingSession.view')
                          <span class="selected"></span>
                          @endif
                        </a>
                    </li>
                    <li class="nav-item start @if( Route::currentRouteName()=='staffMeeting' || Route::currentRouteName()=='staffMeeting.add' || Route::currentRouteName()=='staffMeeting.edit' || Route::currentRouteName()=='staffMeeting.view')open active @endif">
                        <a href="{{ route('staffMeeting') }}" class="nav-link nav-toggle">
                            <i class="icon-bubbles"></i>
                            <span class="title">Staff Meetings</span>
                            @if(Route::currentRouteName()=='staffMeeting' || Route::currentRouteName()=='staffMeeting.add' || Route::currentRouteName()=='staffMeeting.view')
                            <span class="selected"></span>
                            @endif
                        </a>
                    </li>
                @endif
                @if(Auth::user()->user_role == 3)
                    <li class="nav-item start @if( Route::currentRouteName()=='site.setting') open active @endif">
                        <a href="{{ route('site.setting') }}" class="nav-link nav-toggle">
                            <i class="icon-settings"></i>
                            <span class="title">Settings</span>
                            @if(Route::currentRouteName()=='site.setting')
                            <span class="selected"></span>
                            @endif
                        </a>
                    </li>
                @endif
            </ul>
          </div>
      </div>
      <!-- END SIDEBAR -->
      <!-- BEGIN CONTENT -->
      <div class="page-content-wrapper">
          <!-- BEGIN CONTENT BODY -->
          <div class="page-content">
              @yield('content')
          </div>
          <!-- END CONTENT BODY -->
      </div>
      <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
  <!-- BEGIN CORE PLUGINS -->
  <script src="{{cdn('assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/bootstrap/js/bootstrap.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/js.cookie.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/jquery.blockui.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/uniform/jquery.uniform.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/sweetalert/sweetalert.min.js')}}" type="text/javascript"></script>
  <script src="{{ cdn('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/jquery.maskedinput.min.js')}}" type="text/javascript"></script>
  <script src="{{ cdn('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
  <!-- END CORE PLUGINS -->
  <!-- BEGIN PAGE LEVEL PLUGINS -->
  <script src="{{cdn('assets/global/plugins/icheck/icheck.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
  @yield('pagelevel_jsplugin')
  <!-- END PAGE LEVEL PLUGINS -->
  <!-- BEGIN THEME GLOBAL SCRIPTS -->
  <script src="{{cdn('assets/global/scripts/app.min.js')}}" type="text/javascript"></script>
  <!-- END THEME GLOBAL SCRIPTS -->
  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  @yield('pagelevel_script')
  <!-- END PAGE LEVEL SCRIPTS -->
  <!-- BEGIN THEME LAYOUT SCRIPTS -->
  <script src="{{cdn('assets/layouts/layout/scripts/layout.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/layouts/layout/scripts/demo.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/layouts/global/scripts/quick-sidebar.min.js')}}" type="text/javascript"></script>
  <script src="{{ cdn('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
  <script src="{{cdn('assets/pages/scripts/components-bootstrap-switch.js')}}" type="text/javascript"></script>
  <script src="{{cdn('assets/pages/scripts/form-icheck.min.js')}}" type="text/javascript"></script>
  <script src="{{cdn('js/custom.js')}}" type="text/javascript"></script>
  @yield('custom_script')
  <script>
        window.onload = function () { setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50); }
  </script>
  <!-- END THEME LAYOUT SCRIPTS -->
</body>

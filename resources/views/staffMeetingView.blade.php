@extends('master')
@section('title')
View Staff Metting
@endsection
@section('pagelevel_cssplugin')
    <link href="{{ cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/fullcalendar/fullcalendar.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{cdn('assets/global/plugins/clockface/css/clockface.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('custom_style')
    <link href="{{cdn('css/locationPage.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a href="{{route('staffMeeting')}}" class="btn green btn-sm"> Back to Staff Meeting List </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 ">
            <div class="portlet location-image-form light bordered">
                <div class="fileinput fileinput-new" data-provides="fileinput" style="width:100%;position:relative;text-align:center;">
                    <div class="fileinput-new thumbnail" style="height:383px;width:100%;">
                        @if(file_exists('assets/images/staff_meeting'.'/'.$staff_meeting->img_name.'_thumbnail.jpg'))
                          <img style="width:100%;height:100%;" src="{{cdn('assets/images/staff_meeting').'/'.$staff_meeting->img_name.'.jpg'}}" />
                        @else
                          <img style="width:100%;height:100%;" src="{{cdn('assets/images/staff_meeting/not_found.jpg')}}" />
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 ">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title" style="padding-bottom: 0;margin-bottom:0;">
                    <div class="caption font-red-sunglo">
                        <i class="fa fa-map-marker font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> {{$staff_meeting->title}}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="address-title" style="padding-bottom:10px;">
                        <div class="font-dark">
                            <span class="address-caption-subject bold"> {{$staff_meeting->street}} , {{$staff_meeting->city_town}} , {{$staff_meeting->state_province}} {{$staff_meeting->zip_postal}}, US , <label class="font-red-flamingo bold"> {{$staff_meeting->room}} </label> </span>
                        </div>
                    </div>
                    <div id="gmap" class="gmaps"> </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 ">
            <div class="btn-group pull-right" style="margin-bottom:20px;">
                <a href="{{route('staffMeeting')}}" class="btn green btn-sm"> Back to Staff Meeting List </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet light bordered">
                <p class="bold uppercase text-center hidden-when-select">Please selected a scheduled date to manage users, if you need to add a date click <a href="{{url('staffMeeting/edit/'.$staff_meeting->id.'/#calendar')}}" style="text-decoration:underline;">here</a></p>
                <div class="single-schedule-user-mange-div" style="display:none;">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-12 text-right">
                                <div class="btn-group">
                                    <a id="schedule-email-sende-btn" class="btn green" Disabled><i class="fa fa-envelope-o"></i>Email Schedule</a>
                                </div>
                                <div class="btn-group">
                                    <a class="btn red-flamingo" id="event-date-change-btn"><i class="fa fa fa-pencil"></i>Edit</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <i class="icon-settings font-dark"></i>
                            <span class="caption-subject bold uppercase" id="schedule_date"> management</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="btn-group">
                                        <a id="sample_editable_1_new" onclick="show_addform()" class="btn green"> Add User
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="schedule_user">
                            <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" class="group-checkable" data-set="#location_table .checkboxes" />
                                    </th>
                                    <th> Name </th>
                                    <th> Time </th>
                                    <th> Hours </th>
                                    <th> NS </th>
                                    <th> EX </th>
                                    <th> Delete </th>
                                </tr>
                            </thead>
                            <tbody id="user-body-table">
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="portlet light bordered">
                <div style="display:inline-block;padding:6px 0;">
                    <div class="btn-group pull-left">
                        <a href="{{url('staffMeeting/edit/'.$staff_meeting->id.'/#calendar')}}" class="btn green btn-sm"> Add new dates </a>
                    </div>
                </div>
                <div id='calendar'></div>
            </div>
        </div>
    </div>
    <div id="schedule_add_new_user_form" class="modal fade" data-backdrop="static" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add new user</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <form action="#" class="form-horizontal scheduleUserAdd-form">
                            <input type="hidden" name="schedule_event_id" id="schedule_event_id" value="">
                            <div class="form-group">
                                {{ Form::label('date_select', 'Date') }}
                                {{ Form::text('date_select', old('date_select'), ['class' => 'form-control', 'readonly' => 'true']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('date_select', 'User') }}
                                <select class="select2_tms form-control adduser_select" id="add_user_schedule" name="add_user_schedule">
                                    <option value=""></option>
                                    @foreach ($users as $user)
                                        @if ($user->user_role > 0 && $user->id != Auth::user()->id)
                                            <option value="{{$user->id}}">{{$user->first_name}} {{$user->last_name}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true">Close</button>
                    <button class="btn green" onclick="add_newUser()">Add</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('pagelevel_jsplugin')
    <script src="{{cdn('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/moment.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/fullcalendar/fullcalendar.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/clockface/js/clockface.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/jquery.validate.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/jquery-validation/js/additional-methods.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
@endsection
@section('pagelevel_script')
    <script src="{{cdn('assets/pages/scripts/components-date-time-pickers.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/pages/scripts/ui-modals.min.js')}}" type="text/javascript"></script>
    <script src="{{cdn('assets/pages/scripts/components-select2.js')}}" type="text/javascript"></script>
@endsection
@section('custom_script')
    <script src="{{cdn('js/location.js')}}" type="text/javascript"></script>
    <script>
        var staffMeetingId = <?php echo $staff_meeting->id ;?>;

        var BASEURL = "{{ url('/staffMeetingCalendar') }}";

        var addUser_date = null;
        var addUser_event_id = null;

        var d = new Date();
        var n = d.getTime();
        var fortimestamp;

        $(document).ready(function() {

            $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,basicWeek,basicDay'
                },
                navLinks: true, // can click day/week names to navigate views
                editable: true,
                selectable: false,
                selectHelper: true,

                events: BASEURL + '/events/' + staffMeetingId,

                eventRender: function(event, element, view) {
                    /* Only muck about if it is a background rendered event */
                    if (event.rendering == 'background') {
                        var bgEventTitle = document.createElement('div');
                        bgEventTitle.style.position = 'absolute';
                        bgEventTitle.style.bottom = '0';
                        bgEventTitle.classList.add('fc-event');
                        bgEventTitle.classList.add('custom-title-div');
                        bgEventTitle.innerHTML = '<h3 class="fc-title custom-h3">' + event.title + '</h3>' ;
                        element.css('position', 'relative').html(bgEventTitle);
                    }
                },

                dayClick: function(date, jsEvent, view) {
                    var eventDate =  date.format();
                    var getData = BASEURL +'/event/'+staffMeetingId+'/'+eventDate;
                    fortimestamp = date.format('DD-MM-YYYY');
                    $.ajax({
                        url: getData,
                        type: 'get',
                        success: function(result){
                            if (result != "nodata" || jsEvent.target.classList.contains('fc-bgevent')) {

                                $('#schedule_date').html(date.format('MM-DD-YYYY')+' ( '+result.default_start_time+' - '+result.default_end_time+' )');
                                addUser_date = date.format('MM-DD-YYYY');
                                addUser_event_id = result.id;
                                $('#user-body-table').html('');
                                result.schedule_datas.forEach(function(user) {

                                    $('#schedule-email-sende-btn').attr('onclick', 'send_schedule_email()');
                                    $('#schedule-email-sende-btn').attr('Disabled', false);

                                    var dateString = fortimestamp+" "+user.end_timestamp,
                                        dateTimeParts = dateString.split(' '),
                                        timeParts = dateTimeParts[1].split(':'),
                                        dateParts = dateTimeParts[0].split('-'),
                                        endDateTime;

                                    endDateTime = new Date(dateParts[2], parseInt(dateParts[1], 10)-1, dateParts[0], timeParts[0], timeParts[1], timeParts[2]);
                                    var endTimeStamp = endDateTime.getTime();

                                    var prepend_html = '<tr class="odd gradeX" id="schedule_'+user.schedule_id+'">'+
                                        '<td style="cursor:pointer;vertical-align:middle;">'+
                                            '<input type="checkbox" class="checkboxes" value="'+user.schedule_id+'" />'+
                                            '<input type="hidden" class="schedule_id" value="'+user.schedule_id+'" />'+
                                        '</td>'+
                                        '<td style="cursor:pointer;text-align:center;vertical-align:middle;">'+user.name+'</td>'+
                                        '<td style="vertical-align:middle;" class="scedule-time-td">'+
                                            '<div class="user_time" style="width:80%;float:left;white-space:nowrap;">'+
                                                '<span class="start_time">'+user.start+'</span>'+
                                                '<span class="time_separator">-</span>'+
                                                '<span class="end_time">'+user.end+'</span>'+
                                            '</div>';

                                    if (endTimeStamp < n) {
                                        prepend_html += '<div class="user_action" style="width:20%;float:left;padding-left:10px;padding-right:10px;white-space:normal;">'+
                                                            '<a class="edit" href="javascript:;"> Edit </a></div>';
                                    }

                                    if (endTimeStamp > n) {
                                        prepend_html += '</td><td style="vertical-align:middle;">'+
                                                        '<div class="total_time" style="width:80%;float:left;white-space:nowrap;text-align:center;">'+user.total_time+'</div>';
                                    }

                                    if (endTimeStamp < n) {
                                        prepend_html += '</td><td style="vertical-align:middle;width:100px;">'+
                                                            '<div class="total_time" style="width:40%;float:left;white-space:nowrap;text-align:center;">'+user.total_time+'</div>'+
                                                            '<div class="total_time_action" style="width:60%;float:left;padding-left:10px;padding-right:10px;white-space:normal;">'+
                                                            '<a class="edit_total_time" href="javascript:;"> Edit </a></div>';
                                    }

                                    prepend_html += '</td><td style="cursor:pointer;vertical-align:middle;text-align:center;">';

                                    if (endTimeStamp < n) {
                                        var ns = "";
                                        if (user.ns == 1) {
                                            ns = "checked";
                                        }
                                        prepend_html += '<input type="checkbox" onclick="changeNS(this)" style="cursor:pointer;" '+ns+' class="checkboxes" value="1" />';
                                    }

                                    prepend_html += '</td><td style="cursor:pointer;vertical-align:middle;text-align:center;">';

                                    if (endTimeStamp < n) {
                                        var ex = "";

                                        if (user.ex == 1) {
                                            ex = "checked";
                                        }
                                        prepend_html += '<input type="checkbox" onclick="changeEX(this)" style="cursor:pointer;" '+ex+' class="checkboxes" value="1" />';
                                    }

                                    prepend_html += '</td>'+
                                                    '<td align="center" style="vertical-align:middle;">'+
                                                        '<a type="button" class="btn btn-sm btn-default red-flamingo delete">Delete</a>'+
                                                    '</td></tr>';

                                    $('#user-body-table').prepend(prepend_html);

                                    App.init();
                                });
                                $('.hidden-when-select').css({'display':'none'});
                                $('.single-schedule-user-mange-div').css({'display':'block'});
                            }
                        },
                        error: function(result){
                            console.log(result);
                        }
                    });
                },
            });

        });

        var start_time = null;
        var end_time = null;
        var total_time = null;
        var schedule_base_url = "{{ url('/staffMeetingSchedule') }}";

        function restoreRow(nRow) {
            $('.clockface_schedule').clockface('hide');
            var user_time_div = $(nRow).find('.user_time');
            var action_div = $(nRow).find('.user_action');
            user_time_div.css({'width':'80%','float':'left','white-space':'nowrap','margin-top':'0'});
            user_time_div.html('<span class="start_time">'+start_time+'</span>'+
                                '<span class="time_separator">-</span>'+
                                '<span class="end_time">'+end_time+'</span>');
            action_div.html('<a class="edit" href="javascript:;"> Edit </a>');
            start_time = null;
            end_time = null;
        }

        function restoreTotalTimeRow(nRow) {
            var total_time_div = $(nRow).find('.total_time');
            var action_div = $(nRow).find('.total_time_action');
            total_time_div.css({'margin-top':'0'});
            total_time_div.html(total_time);
            action_div.html('<a class="edit_total_time" href="javascript:;"> Edit </a>');
            total_time = null;
        }

        function editRow(nRow) {
            var user_time_div = $(nRow).find('.user_time');
            var action_div = $(nRow).find('.user_action');

            start_time = user_time_div.find('.start_time').html().trim();
            end_time = user_time_div.find('.end_time').html().trim();

            user_time_div.css({'width':'80%','float':'left','white-space':'nowrap','margin-top':'8px'});
            user_time_div.html('<input type="text" class="clockface_schedule start_time_input" data-format="h:mm a" style="width:47%;float:left;height:25px;padding:6px;outline:0;" name="start_time" value="'+start_time+'">'+
                                '<span style="float:left;padding-top:4px;padding-bottom:4px;padding-left:2px;padding-right:2px;">-</span>'+
                                '<input type="text" class="clockface_schedule end_time_input" data-format="h:mm a" style="width:47%;float:left;height:25px;padding:6px;outline:0;" name="end_time" value="'+end_time+'">');
            action_div.html('<a class="save" href="javascript:;"> save </a><a class="cancel" href="javascript:;"> cancel </a>');
            time_clock_init();
        }

        function editTotalTime(nRow) {
            var total_time_div = $(nRow).find('.total_time');
            var action_div = $(nRow).find('.total_time_action');

            total_time = total_time_div.html().trim();

            total_time_div.css({'margin-top':'8px'});
            total_time_div.html('<input type="text" class="total_time_input" style="width:100%;float:left;height:25px;padding:6px;outline:0;" name="total_time" value="'+total_time+'">');
            action_div.html('<a class="save_total_time" href="javascript:;"> save </a><a class="cancel_total_time" href="javascript:;"> cancel </a>');
        }

        function time_clock_init(){
            $('.clockface_schedule').clockface();
        }

        function saveRow(nRow) {
            var user_time_div = $(nRow).find('.user_time');

            start_time = user_time_div.find('.start_time_input').val();
            end_time = user_time_div.find('.end_time_input').val();
            var schedule_id = $(nRow).find('.schedule_id').val();
            var edit_final_url = schedule_base_url + '/edit/' + schedule_id;

            axios.post(edit_final_url, {startTime:start_time, endTime:end_time}).then(function (response) {
                if(response.data == "success") {
                    restoreRow(nRow);
                }
            }).catch(function (error) {
                console.log(error);
            });
        }

        function saveTotalTimeRow(nRow) {
            var total_time_div = $(nRow).find('.total_time');

            total_time = total_time_div.find('.total_time_input').val();
            var schedule_id = $(nRow).find('.schedule_id').val();
            var edit_totaltime_final_url = schedule_base_url + '/edit_total_time/' + schedule_id;

            axios.post(edit_totaltime_final_url, {totalTime:total_time}).then(function (response) {
                if(response.data == "success") {
                    restoreTotalTimeRow(nRow);
                }
            }).catch(function (error) {
                console.log(error);
            });
        }

        var table = $('#schedule_user');

        var nEditing = null;
        var nEditing1 = null;
        var nRowDelete = null;

        table.on('click', '.cancel', function (e) {
                restoreRow(nEditing);
                nEditing = null;
        });

        table.on('click', '.cancel_total_time', function (e) {
                restoreTotalTimeRow(nEditing1);
                nEditing1 = null;
        });

        table.on('click', '.edit', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            // console.log(nRow);

            if (nEditing !== null && nEditing != nRow) {

                restoreRow( nEditing);
                editRow(nRow);
                nEditing = nRow;

            } else {
                /* No edit in progress - let's start one */
                editRow(nRow);
                nEditing = nRow;
            }
        });

        table.on('click', '.edit_total_time', function (e) {
            e.preventDefault();

            /* Get the row as a parent of the link that was clicked on */
            var nRow = $(this).parents('tr')[0];

            // console.log(nRow);

            if (nEditing1 !== null && nEditing1 != nRow) {

                // $('.clockface_schedule').clockface('hide');
                restoreTotalTimeRow( nEditing1);
                editTotalTime(nRow);
                nEditing1 = nRow;

            } else {
                /* No edit in progress - let's start one */
                editTotalTime(nRow);
                nEditing1 = nRow;
            }
        });

        table.on('click', '.save', function (e) {
            e.preventDefault();
            saveRow(nEditing);
            nEditing = null;
        });

        table.on('click', '.save_total_time', function (e) {
            e.preventDefault();
            saveTotalTimeRow(nEditing1);
            nEditing1 = null;
        });

        table.on('click', '.delete', function (e) {
            e.preventDefault();

            var nRow = $(this).parents('tr')[0];

            nRowDelete = nRow;

            var schedule_id = $(nRow).find('.schedule_id').val();

            var delete_url = schedule_base_url +'/delete/'+schedule_id;

            swal({
              title: "Are you sure?",
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Yes, delete Schedule!",
              cancelButtonText: "No, cancel plx!",
              closeOnConfirm: false,
              closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: delete_url,
                        type: 'get',
                        success: function(result){

                            var count_nrow = 0 ;
                            $(nRowDelete).parent().find('tr').each(function() {
                               count_nrow += 1;
                            })
                            if (count_nrow < 2) {
                               $('#schedule-email-sende-btn').removeAttr('onclick');
                               $('#schedule-email-sende-btn').attr('Disabled', true);
                            }

                            nRow.remove();
                            swal("Deleted!", "Schedule has been deleted.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    swal("Cancelled", "Schedule is safe :)", "error");
                }
            });
        });

        function show_addform() {
            var addform = $('#schedule_add_new_user_form');
            addform.find('#date_select').val(addUser_date);
            addform.find('#schedule_event_id').val(addUser_event_id);
            $('#schedule_add_new_user_form').modal('show');
        }

        function add_newUser() {
            var addform = $('#schedule_add_new_user_form');
            var event_id = addform.find('#schedule_event_id').val();
            var user_id = null;

            if(addform.find('#add_user_schedule').val() != "") {
                user_id = addform.find('#add_user_schedule').val();
            }
            else {
                alert('please select User');
                return false;
            }

            if (user_id != null) {
                var add_user_final_url = schedule_base_url + "/add/" + event_id +"/"+ user_id;

                $.ajax({
                    url: add_user_final_url,
                    type: 'get',
                    success: function(result){
                        if(result == "exist") {
                            alert("user already added to this schedule");
                        }
                        else {

                            $('#schedule-email-sende-btn').attr('onclick', 'send_schedule_email()');
                            $('#schedule-email-sende-btn').attr('Disabled', false);

                            var dateString = fortimestamp+" "+result['end_timestamp'],
                                dateTimeParts = dateString.split(' '),
                                timeParts = dateTimeParts[1].split(':'),
                                dateParts = dateTimeParts[0].split('-'),
                                endDateTime;

                            endDateTime = new Date(dateParts[2], parseInt(dateParts[1], 10)-1, dateParts[0], timeParts[0], timeParts[1], timeParts[2]);
                            var endTimeStamp = endDateTime.getTime();

                            console.log(n);
                            console.log(endTimeStamp);

                            var prepend_html = '<tr class="odd gradeX" id="schedule_'+result['schedule_id']+'">'+
                                '<td style="cursor:pointer;vertical-align:middle;">'+
                                    '<input type="checkbox" class="checkboxes" value="'+result['schedule_id']+'" />'+
                                    '<input type="hidden" class="schedule_id" value="'+result['schedule_id']+'" />'+
                                '</td>'+
                                '<td style="cursor:pointer;text-align:center;vertical-align:middle;">'+result['name']+'</td>'+
                                '<td style="vertical-align:middle;" class="scedule-time-td">'+
                                    '<div class="user_time" style="width:80%;float:left;white-space:nowrap;">'+
                                        '<span class="start_time">'+result['start']+'</span>'+
                                        '<span class="time_separator">-</span>'+
                                        '<span class="end_time">'+result['end']+'</span>'+
                                    '</div>';

                            if (endTimeStamp < n) {
                                prepend_html += '<div class="user_action" style="width:20%;float:left;padding-left:10px;padding-right:10px;white-space:normal;">'+
                                                    '<a class="edit" href="javascript:;"> Edit </a></div>';
                            }

                            if (endTimeStamp > n) {
                                prepend_html += '</td><td style="vertical-align:middle;">'+
                                                '<div class="total_time" style="width:80%;float:left;white-space:nowrap;text-align:center;">'+result['total_time']+'</div>';
                            }

                            if (endTimeStamp < n) {
                                prepend_html += '</td><td style="vertical-align:middle;width:100px;">'+
                                                    '<div class="total_time" style="width:40%;float:left;white-space:nowrap;text-align:center;">'+result['total_time']+'</div>'+
                                                    '<div class="total_time_action" style="width:60%;float:left;padding-left:10px;padding-right:10px;white-space:normal;">'+
                                                    '<a class="edit_total_time" href="javascript:;"> Edit </a></div>';
                            }

                            prepend_html += '</td><td style="cursor:pointer;vertical-align:middle;text-align:center;">';

                            if (endTimeStamp < n) {
                                var ns = "";
                                if (result['ns'] == 1) {
                                    ns = "checked";
                                }
                                prepend_html += '<input type="checkbox" onclick="changeNS(this)" style="cursor:pointer;" '+ns+' class="checkboxes" value="1" />';
                            }

                            prepend_html += '</td><td style="cursor:pointer;vertical-align:middle;text-align:center;">';

                            if (endTimeStamp < n) {
                                var ex = "";

                                if (result['ex'] == 1) {
                                    ex = "checked";
                                }
                                prepend_html += '<input type="checkbox" onclick="changeEX(this)" style="cursor:pointer;" '+ex+' class="checkboxes" value="1" />';
                            }

                            prepend_html += '</td>'+
                                            '<td align="center" style="vertical-align:middle;">'+
                                                '<a type="button" class="btn btn-sm btn-default red-flamingo delete">Delete</a>'+
                                            '</td></tr>';

                            $('#user-body-table').prepend(prepend_html);

                            addform.find('#schedule_event_id').val("");
                            $('#schedule_add_new_user_form').modal('hide');
                            App.init();
                        }
                    },
                    error: function(result){
                        console.log(error);
                    }
                });
            }
        }

        function changeNS(ns) {
            var ns_status = null;

            var nRow = $(ns).parents('tr')[0];

            if($(ns).prop('checked') == true) {
                ns_status = 1;
                // console.log(ns_status);
            }
            else if($(ns).prop('checked') == false) {
                ns_status = 0;
                // console.log(ns_status);
            }

            var schedule_id = $(nRow).find('.schedule_id').val();

            var ns_final_url = schedule_base_url + "/ns/" + schedule_id +"/"+ ns_status;

            $.ajax({
                url: ns_final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(error);
                }
            });
        }

        function changeEX(ns) {
            var ns_status = null;

            var nRow = $(ns).parents('tr')[0];

            if($(ns).prop('checked') == true) {
                ns_status = 1;
                // console.log(ns_status);
            }
            else if($(ns).prop('checked') == false) {
                ns_status = 0;
                // console.log(ns_status);
            }

            var schedule_id = $(nRow).find('.schedule_id').val();

            var ex_final_url = schedule_base_url + "/ex/" + schedule_id +"/"+ ns_status;

            $.ajax({
                url: ex_final_url,
                type: 'get',
                success: function(result){
                    console.log(result);
                },
                error: function(result){
                    console.log(error);
                }
            });
        }

        function send_schedule_email() {
            swal({
              title: "You are about to Email everyone on schedule below a reminder of the schedule",
              type: "info",
              showCancelButton: true,
              confirmButtonColor: "#32c5d2",
              confirmButtonText: "Yes, Send Email!",
              cancelButtonText: "No, cancel plx!",
              showLoaderOnConfirm: true,
              closeOnConfirm: false,
              closeOnCancel: true
            }, function (isConfirm) {
                if (isConfirm) {
                    var schedule_email_base_url = "{{url('send_staff_schedule_email')}}";
                    var schedule_email_url = schedule_email_base_url+"/"+addUser_event_id;
                    $.ajax({
                        url: schedule_email_url,
                        type: 'get',
                        success: function(result){
                            console.log(result);
                            swal("Sent!", "Email Sent.", "success");
                        },
                        error: function(result){
                            console.log(error);
                        }
                    });
                } else {
                    console.log("hello");
                }
            });
        }

    </script>

    <script>
        var str_coordinatelat = <?php echo $staff_meeting->coor_lat; ?>;
        var str_coordinatelng = <?php echo $staff_meeting->coor_lng; ?>;
        var coordinatelat = parseFloat(str_coordinatelat);
        var coordinatelng = parseFloat(str_coordinatelng);

        var map;
        var marker;
        function initMap() {
            var mapOptions = {
        		center: new google.maps.LatLng(coordinatelat,coordinatelng),
        		zoom: 17,
        		mapTypeId: google.maps.MapTypeId.ROADMAP
        	};
            map = new google.maps.Map(document.getElementById('gmap'),mapOptions);

            var myLatLng = {lat: coordinatelat, lng: coordinatelng};

            marker = new google.maps.Marker({
              map: map,
              position: myLatLng
            });
        }
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtCsu8LKseS4s2ahkBvYcL_yKKmKWX82g&callback=initMap"></script>
@endsection

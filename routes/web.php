<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('dashboard');

Route::get('/mail', function(){
    Mail::send(['text' => 'email.html'], ['name' => 'Assassin'], function ($message){
      $message->to('chol.r@hotmail.com', 'Assassiner')->subject('Welcome to Expertphp.in!');
      $message->from('tms@support.com', 'from me');
    });
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('thankyou', 'Auth\RegisterController@thankyou')->name('thankyou');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('registerInfo', 'Auth\RegisterController@registerInfo');
Route::post('registerschedule', 'Auth\RegisterController@registerSchedule');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password-reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password-email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password-reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password-reset', 'Auth\ResetPasswordController@reset');

Route::get('email-verification/error', 'Auth\RegisterController@getVerificationError')->name('email-verification.error');
Route::get('email-verification/check/{token}', 'Auth\RegisterController@getVerification')->name('email-verification.check');

Route::prefix('profile')->group(function(){
    Route::get('/view', 'UserController@profile')->name('profile.view');
    Route::get('/edit', 'UserController@profile_edit')->name('profile.edit');
    Route::post('/edit/bio', 'UserController@profile_edit_bio')->name('profile.edit.bio');
    Route::post('/edit/task', 'UserController@profile_edit_task')->name('profile.edit.task');
    Route::post('/edit/email', 'UserController@profile_edit_email')->name('profile.edit.email');
    Route::post('/edit/avartar', 'UserController@profile_edit_avartar')->name('profile.edit.avartar');
    Route::post('/edit/password', 'UserController@profile_edit_password')->name('profile.edit.password');
});

Route::prefix('report')->group(function(){
    Route::get('/dietary', 'ReportController@dietary')->name('report.dietary');
    Route::get('/tshirt', 'ReportController@tshirt')->name('report.tshirt');
    Route::get('/tshirt-first', 'ReportController@tshirt_first')->name('report.tshirt.first');
    Route::get('/location-date-user/{id}', 'ReportController@location_date_user');
    Route::get('/traininguser', 'ReportController@training_user')->name('report.traininguser');
    Route::get('/volunteer', 'ReportController@volunteer')->name('report.volunteer');
});

Route::get('users', 'UserController@user_lists')->name('user.lists');
Route::get('siteSetting', 'UserController@site_setting')->name('site.setting');
Route::get('change_tshirt/{status}', 'UserController@change_tshirt');
Route::get('change_newuser/{status}', 'UserController@change_newuser');
Route::get('reset_logo/{status}', 'UserController@reset_logo');
Route::get('reset_backstretch/{status}', 'UserController@reset_backstretch');
Route::post('change_setting', 'UserController@change_setting')->name('change.setting');
Route::post('change_logo', 'UserController@change_logo')->name('change.logo');
Route::post('change_register_form', 'UserController@change_register_form')->name('change.register_form');
Route::post('change_thankyou_text', 'UserController@change_thankyou_text')->name('change.thankyou_text');
Route::post('change_register_email_text', 'UserController@change_register_email_text')->name('change.register_email_text');
Route::post('change_env_maildetail', 'UserController@change_env_maildetail')->name('change.env.maildetail');
Route::get('preview-register', 'UserController@preview_register_form')->name('preview.register');
Route::post('change_location_schedule_email_text', 'UserController@change_location_schedule_email_text')->name('change.location_schedule_email_text');
Route::post('change_location_schedule_delete_user_text', 'UserController@change_location_schedule_delete_user_text')->name('change.location_schedule_delete_user_text');
Route::post('change_train_schedule_email_text', 'UserController@change_train_schedule_email_text')->name('change.change_train_schedule_email_text');
Route::post('change_train_schedule_delete_user_text', 'UserController@change_train_schedule_delete_user_text')->name('change.train_schedule_delete_user_text');
Route::post('change_staff_schedule_email_text', 'UserController@change_staff_schedule_email_text')->name('change.change_staff_schedule_email_text');
Route::post('change_staff_schedule_delete_user_text', 'UserController@change_staff_schedule_delete_user_text')->name('change.staff_schedule_delete_user_text');
Route::get('send_schedule_email/{event_id}', 'HomeController@send_schedule_email');
Route::get('send_train_schedule_email/{event_id}', 'HomeController@send_train_schedule_email');
Route::get('send_staff_schedule_email/{event_id}', 'HomeController@send_staff_schedule_email');

Route::prefix('single-user')->group(function(){
    Route::post('/add', 'UserController@user_add')->name('singleUser.add');
    Route::get('/view/{id}', 'UserController@view_single_user')->name('singleUser.view');
    Route::get('/edit/{id}', 'UserController@edit_single_user')->name('singleUser.edit');
    Route::post('/edit/bio', 'UserController@edit_bio_single_user')->name('singleUser.edit.bio');
    Route::post('/edit/task', 'UserController@edit_task_single_user')->name('singleUser.edit.task');
    Route::post('/edit/email', 'UserController@edit_email_single_user')->name('singleUser.edit.email');
    Route::post('/edit/avartar', 'UserController@edit_avartar_single_user')->name('singleUser.edit.avartar');
    Route::post('/edit/password', 'UserController@edit_password_single_user')->name('singleUser.edit.password');
    Route::post('/edit/role', 'UserController@user_role_change')->name('singleUser.role.change');
    Route::get('/delete/{id}', 'UserController@delete_single_user');
    Route::get('/delete_schedule/{id}', 'LocationScheduleController@single_schedule_destroy');
    Route::get('/delete_schedule_training_session/{id}', 'TrainingSessionScheduleController@single_schedule_destroy');
    Route::get('/delete_schedule_staff_meeting/{id}', 'StaffMeetingScheduleController@destroy');
});

Route::prefix('location')->group(function(){
    Route::get('/', 'LocationController@index')->name('location');
    Route::get('/add', 'LocationController@showLocationAddForm')->name('location.add');
    Route::post('/add', 'LocationController@store')->name('location.store');
    Route::get('/view/{id}', 'LocationController@view_location')->name('location.view');
    Route::get('/edit/{id}', 'LocationController@edit_location')->name('location.edit');
    Route::post('/update', 'LocationController@update')->name('location.update');
    Route::get('/changeSat/{id}/{statue}', 'LocationController@reset_saturday');
    Route::get('/delete/{id}', 'LocationController@destroy');
    Route::post('/assign_manager', 'LocationController@assign_manager')->name('location.assign.manager');
    Route::get('/delete_manager/{location_id}/{user_id}', 'LocationController@destroy_manager');
});

Route::prefix('locationCalendar')->group(function(){
    Route::get('/events/{locationId}', 'LocationCalendarController@index');
    Route::get('/event/{locationId}/{eventDate}', 'LocationCalendarController@event');
    Route::get('/event_edit/{locationId}/{eventDate}', 'LocationCalendarController@edit_event');
    Route::get('/delete_schedule/{id}', 'LocationCalendarController@delete_schedule');
    Route::get('/delete/{id}', 'LocationCalendarController@destroy');
    Route::post('/store', 'LocationCalendarController@store')->name('location.event.store');
    Route::post('/update', 'LocationCalendarController@update_event')->name('location.event.update');
    Route::post('/change-date', 'LocationCalendarController@change_event_date')->name('location.event.date.change');
});

Route::prefix('locationSchedule')->group(function(){
    Route::post('/edit/{id}', 'LocationScheduleController@update');
    Route::post('/edit_total_time/{id}', 'LocationScheduleController@update_total_time');
    Route::post('/delete/{id}', 'LocationScheduleController@destroy');
    Route::get('/add/{eventId}/{userId}', 'LocationScheduleController@store');
    Route::get('/ns/{id}/{status}', 'LocationScheduleController@ns_change');
    Route::get('/ex/{id}/{status}', 'LocationScheduleController@ex_change');
});

Route::prefix('trainingSession')->group(function(){
    Route::get('/', 'TrainingSessionController@index')->name('trainingSession');
    Route::get('/add', 'TrainingSessionController@showTrainingSessionAddForm')->name('trainingSession.add');
    Route::post('/add', 'TrainingSessionController@store')->name('trainingSession.store');
    Route::get('/view/{id}', 'TrainingSessionController@view_trainingSession')->name('trainingSession.view');
    Route::get('/edit/{id}', 'TrainingSessionController@edit_trainingSession')->name('trainingSession.edit');
    Route::post('/update', 'TrainingSessionController@update')->name('trainingSession.update');
    Route::get('/changeTBD/{id}/{statue}', 'TrainingSessionController@reset_tbd');
    Route::get('/delete/{id}', 'TrainingSessionController@destroy');
});

Route::prefix('trainingSessionCalendar')->group(function(){
    Route::get('/events/{trainingSessionId}', 'TrainingSessionCalendarController@index');
    Route::get('/event/{trainingSessionId}/{eventDate}', 'TrainingSessionCalendarController@event');
    Route::get('/event_edit/{trainingSessionId}/{eventDate}', 'TrainingSessionCalendarController@event_edit');
    Route::get('/delete/{id}', 'TrainingSessionCalendarController@destroy');
    Route::post('/store', 'TrainingSessionCalendarController@store')->name('strainSession.event.store');
    Route::post('/update', 'TrainingSessionCalendarController@update_event')->name('strainSession.event.update');
    Route::get('/delete_schedule/{id}', 'TrainingSessionCalendarController@delete_schedule');
    Route::post('/change-date', 'TrainingSessionCalendarController@change_event_date')->name('strainSession.event.date.change');
});

Route::prefix('trainingSessionSchedule')->group(function(){
    Route::post('/edit/{id}', 'TrainingSessionScheduleController@update');
    Route::post('/delete/{id}', 'TrainingSessionScheduleController@destroy');
    Route::get('/add/{eventId}/{userId}', 'TrainingSessionScheduleController@store');
    Route::get('/ns/{id}/{status}', 'TrainingSessionScheduleController@ns_change');
    Route::get('/ex/{id}/{status}', 'TrainingSessionScheduleController@ex_change');
    Route::get('/cm/{id}/{status}', 'TrainingSessionScheduleController@cm_change');
});

Route::prefix('staffMeeting')->group(function(){
    Route::get('/', 'StaffMeetingController@index')->name('staffMeeting');
    Route::get('/add', 'StaffMeetingController@showStaffMeetingAddForm')->name('staffMeeting.add');
    Route::post('/add', 'StaffMeetingController@store')->name('staffMeeting.store');
    Route::get('/view/{id}', 'StaffMeetingController@view_staffMeeting')->name('staffMeeting.view');
    Route::get('/edit/{id}', 'StaffMeetingController@edit_staffMeeting')->name('staffMeeting.edit');
    Route::post('/update', 'StaffMeetingController@update')->name('staffMeeting.update');
    Route::get('/delete/{id}', 'StaffMeetingController@destroy');
});

Route::prefix('staffMeetingCalendar')->group(function(){
    Route::get('/events/{staffMeetingId}', 'StaffMeetingCalendarController@index');
    Route::get('/event/{staffMeetingId}/{eventDate}', 'StaffMeetingCalendarController@event');
    Route::get('/event_edit/{staffMeetingId}/{eventDate}', 'StaffMeetingCalendarController@event_edit');
    Route::get('/delete/{id}', 'StaffMeetingCalendarController@destroy');
    Route::post('/store', 'StaffMeetingCalendarController@store')->name('staffMeeting.event.store');
    Route::post('/update', 'StaffMeetingCalendarController@update_event')->name('staffMeeting.event.update');
});

Route::prefix('staffMeetingSchedule')->group(function(){
    Route::post('/edit/{id}', 'StaffMeetingScheduleController@update');
    Route::post('/edit_total_time/{id}', 'StaffMeetingScheduleController@update_total_time');
    Route::get('/delete/{id}', 'StaffMeetingScheduleController@destroy');
    Route::get('/add/{eventId}/{userId}', 'StaffMeetingScheduleController@store');
    Route::get('/ns/{id}/{status}', 'StaffMeetingScheduleController@ns_change');
    Route::get('/ex/{id}/{status}', 'StaffMeetingScheduleController@ex_change');
});

Route::prefix('task')->group(function(){
    Route::get('/', 'UserTaskController@index')->name('user_task');
    Route::post('/add', 'UserTaskController@store')->name('user_task.store');
    Route::get('/delete/{id}', 'UserTaskController@destroy');
    Route::get('/get_single/{id}', 'UserTaskController@get_single');
    Route::post('/edit', 'UserTaskController@update')->name('user_task.update');
});
